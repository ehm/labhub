<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

trait LabhubServiceTrait {

  /**
   * @var string
   */
  private $service_name;

  /**
   * @var string
   */
  private $host;

  /**
   * @var string
   */
  private $service_type;

  /**
   * @return mixed
   */
  public function getServiceType() {
    return $this->service_type;
  }

  /**
   * @param mixed $service_type
   */
  public function setServiceType($service_type) {
    $this->service_type = $service_type;
  }

  /**
   * @return bool
   */
  public function testConnection() {
    // check of host contains port information
    $host = $this->host;
    if ($pos = strpos($host, ':')) {
      if ($next_slash = strpos($host, '/', $pos)) {
        $port = substr($host, $pos, strlen($host) - $next_slash);
      }
      else {
        $port = substr($host, $pos, strlen($host) - $pos);
      }

      $port = str_replace(':', '', $port);
      $host = substr($host, 0, $pos);
      drupal_set_message("Hostname: $host, Port: $port");

      $fp = fsockopen($host, (int) $port);

      return !$fp ? FALSE : TRUE;

    }
    // If no port information is given:
    $response = NULL;
    $cmd = 'ping -c 1 ' . $this->host . ' > dev0';
    system($cmd, $response);
    if ($response == 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }

  }

  public function save() {
    if ($id = LabhubServiceRepository::save($this)) {
      $this->id = $id;
    }
  }

  /**
   * @return array
   */
  public function dbInsertFields() {
    return [
      'service_name' => $this->getServiceName(),
      'service_type' => $this->getServiceType(),
      'host' => $this->getHost(),
    ];
  }

  /**
   * @return mixed
   */
  public function getServiceName() {
    return $this->service_name;
  }

  /**
   * @param string $service_name
   */
  public function setServiceName($service_name) {
    $this->service_name = $service_name;
  }

  /**
   * @return mixed
   */
  public function getHost() {
    if ($pos = strpos($host = $this->host, ':')) {
      if ($next_slash = strpos($host, '/', $pos)) {
        $port = substr($host, $pos, strlen($host) - $next_slash);
      }
      else {
        $port = substr($host, $pos, strlen($host) - $pos);
      }

      $port = str_replace(':', '', $port);
      $host = substr($host, 0, $pos);

      switch ($port) {
        case 443:
          $host = str_replace(":443", "", $this->host);
          return 'https://' . $host;
          break;
        case 80:
        default:
          return $this->host;
          break;
      }

    }
    else {
      return $this->host;
    }
  }

  /**
   * @param mixed $host
   */
  public function setHost($host) {

    $host = self::trim_url($host);

    if (valid_url($host)) {
      $this->host = $host;
    }
    else {
      drupal_set_message('Not a valid URL.', 'error');
    }
  }

  /**
   * Removes leading http/s and trailing slashes from URL.
   *
   * @param string $url
   *
   * @return string $route
   */
  private static function trim_url($url) {
    $url = str_replace('http://', '', $url);
    $url = str_replace('https://', '', $url);
    $last_char = substr($url, -1);
    if ($last_char == '/') {
      $url = substr_replace($url, '', -1);
    }
    return $url;
  }

}