<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

interface LabhubServiceInterface extends LabhubObjectInterface {

  public function getServiceName();

  public function setServiceName($service_name);

  public function getHost();

  public function setHost($host);

  public function getServiceType();

}