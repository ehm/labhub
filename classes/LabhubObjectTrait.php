<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


trait LabhubObjectTrait {

  /**
   * @var int
   */
  private $id = LabhubObjectInterface::EMPTY_ID;

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }


  public static function dbResultsToObjects($results) {
    $data = [];
    if (count($results)) {
      foreach ($results as $result) {
        $data[] = self::dbResultToObject($result);
      }
      return $data;
    }
    else {
      return $data;
    }
  }

  public static function dbResultToObject($result) {
    $service = new self();
    if (is_object($result)) {
      $service->parseObject($result);
    }
    return $service;
  }

  public function parseObject(stdClass $object) {
    $vars = get_object_vars($object);
    foreach ($vars as $key => $value) {
      $this->$key = $value;
    }
  }

  /**
   * @return string
   */
  public function __toString() {
    return json_encode(get_object_vars($this));
  }

  public function isEmpty() {
    return $this->id == self::EMPTY_ID ? TRUE : FALSE;
  }
}