<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


class LabhubCredentialsRepository implements LabhubObjectRepositoryInterface {

  use LabhubObjectRepositoryTrait;

  private static $table_name = 'labhub_credentials';

  private static $database_fields = [
    'id',
    'context',
    'type',
    'secret',
    'algorithm',
    'issuer',
    'subject',
  ];

  private static $class_name = 'LabhubCredentials';

  public static function getAllByContext($context) {

    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t', self::$database_fields)
        ->condition('t.context', $context, '=')
        ->execute()
        ->fetchAll();

    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return [];
    }

    return LabhubCredentials::dbResultsToObjects($result);
  }

  /**
   * Validate an instance of LabhubCredentials against the set of
   * defined instances for a given context.
   *
   * @param \LabhubCredentials $credentials
   * @param string $context
   *
   * @return bool TRUE if tested credentials are known
   */
  public static function validate(LabhubCredentials $credentials, $context) {
    $whitelist = self::getAllByContext($context);
    foreach ($whitelist as $control) {
      /**
       * @var \LabhubCredentials $control
       */
      if ($control->equals($credentials)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public static function getAllTypeByContext($type, $context) {

    //ToDo: test if $type is valid CredentialsType string

    try {

      $db_and = db_and();
      $db_and->condition('t.type', $type);
      $db_and->condition('t.context', $context);

      $result = db_select(self::$table_name, 't')
        ->fields('t', self::$database_fields)
        ->condition($db_and)
        ->execute()
        ->fetchAll();

    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return [];
    }

    return LabhubCredentials::dbResultsToObjects($result);
  }

}
