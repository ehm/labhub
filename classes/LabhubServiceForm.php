<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


abstract class LabhubServiceForm {

  /**
   * @var \LabhubServiceInterface
   */
  private static $service;

  public static function create(LabhubServiceInterface $service) {
    self::$service = $service;

    $form = [];

    $fields = [
      'service_name',
      'host',
    ];

    foreach ($fields as $field) {
      $form[$field] = self::getFormField($field);
    }

    return $form;
  }

  public static function update(LabhubServiceInterface $service) {
    // ToDo: implement
    $form = [];
    return $form;
  }

  private static function getFormField($field) {
    switch ($field) {
      case 'service_name':
        return [
          '#type' => 'textfield',
          '#title' => t('Service Name'),
          '#description' => t('Enter a human readable name for the service.'),
          '#default_value' => self::$service->getServiceName(),
          '#required' => TRUE,
        ];
        break;
      case 'host':
        return [
          '#type' => 'textfield',
          '#title' => t('Host'),
          '#default_value' => self::$service->getHost(),
          '#required' => TRUE,
        ];
        break;
      default:
        return [];
    }
  }

  public static function addSubmitButton($form) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'save',
    ];
    return $form;
  }
}