<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubAuthentication implements LabhubObjectInterface {

  use LabhubObjectTrait;

  const AUTH_TYPE_BASIC = 100;

  const AUTH_TYPE_JWT = 200;

  /**
   * @var int
   */
  private $type;

  /**
   * @var string
   */
  private $label;

  /**
   * @var string
   */
  private $username;

  /**
   * @var string
   */
  private $resource;

  /**
   * @var string
   */
  private $password;

  public function save() {
    LabhubAuthenticationRepository::save($this);
  }

  /**
   * @return int
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param int $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * @param string $label
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * @return string
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * @param string $username
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * @return string
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * @param string $password
   */
  public function setPassword($password) {
    $this->password = labhub_ssl_encrypt($password);
  }

  public function getResource() {
    return $this->resource;
  }

  /**
   * @param string $resource
   */
  public function setResource($resource) {
    $this->resource = $resource;
  }

  /**
   * @return array
   */
  public function dbInsertFields() {
    return [
      'type' => $this->getType(),
      'username' => $this->getUsername(),
      'password' => $this->getPassword(),
      'resource' => $this->getResource(),
    ];
  }
}