<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


/**
 * Store all Labhub Database Services (that implement
 *
 * Class LabhubDatabaseRepository
 */
class LabhubDatabaseRepository {

  private static $table_name = 'labhub_database';

  private static $database_fields = [
    'id',
    'service_id',
    'auth_id',
  ];

  /**
   * @param \LabhubDatabaseInterface $database
   *
   * @return bool|int ID on success, FALSE otherwise
   */
  public static function save(LabhubDatabaseInterface $database) {

    $transaction = db_transaction();

    try {
      if ($database->isEmpty()) {
        $id = NULL;
      }
      else {
        $id = $database->getId();
      }

      $service_id = LabhubServiceRepository::save($database);
      $auth_id = LabhubAuthenticationRepository::save($database->getAuthentication());

      db_merge(self::$table_name)
        ->key(['id' => $id])
        ->fields([
          'service_id' => $service_id,
          'auth_id' => $auth_id,
        ])
        ->execute();

      return Database::getConnection()->lastInsertId();

    } catch (Exception $e) {

      $transaction->rollback();

      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');
    }
    return FALSE;

  }

  /**
   * @param int $id
   *
   * @return mixed
   */
  public static function getById($id) {

    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t', self::$database_fields)
        ->condition('t.id', $id, '=')
        ->range(0, 1)
        ->execute()
        ->fetch();

    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return NULL;
    }

    $service = LabhubServiceRepository::getById($result->service_id);
    $authentication = LabhubAuthenticationRepository::getById($result->auth_id);

    $types = labhub_types();
    $classname = $types[$service->getServiceType()];

    return $classname::cast($service, $authentication, $result->id);

  }

  public static function getAll($service_type = NULL) {

    $types = labhub_types();
    if (!array_key_exists($service_type, $types)) {
      $service_type = NULL;
    }

    try {
      $query = db_select(self::$table_name, 'db');

      if (!is_null($service_type)) {
        $query->join(LabhubServiceRepository::$table_name, 's',
          'db.service_id = s.id');
        $query->condition('s.service_type', $service_type);
      }
      $query->fields('db', self::$database_fields);
      $results = $query->execute()->fetchAll();

    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return [];
    }

    $data = [];
    foreach ($results as $result) {
      $service = LabhubServiceRepository::getById($result->service_id);
      $authentication = LabhubAuthenticationRepository::getById($result->auth_id);

      $types = labhub_types();
      $classname = $types[$service->getServiceType()];

      $data[] = $classname::cast($service, $authentication, $result->id);
    }
    return $data;
  }

}