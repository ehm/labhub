<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


use Firebase\JWT\JWT;

/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 02.05.18
 * Time: 19:11
 */
class LabhubCredentials implements LabhubObjectInterface {

  use LabhubObjectTrait;

  const TYPE_USERPASS = 100;

  const TYPE_JWT = 200;

  const TYPE_API_KEY = 300;

  private
    $context,
    $type = self::TYPE_USERPASS,
    $secret,
    $algorithm,
    $issuer,
    $subject;

  /**
   * @return mixed
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * @param mixed $context
   */
  public function setContext($context) {
    $this->context = $context;
  }

  /**
   * @return mixed
   */
  public function getSecret() {
    return $this->secret;
  }

  /**
   * @param mixed $secret
   */
  public function setSecret($secret) {
    $this->secret = labhub_ssl_encrypt($secret);
  }

  /**
   * @return mixed
   */
  public function getAlgorithm() {
    return $this->algorithm;
  }

  /**
   * @param mixed $algorithm
   */
  public function setAlgorithm($algorithm) {
    $this->algorithm = $algorithm;
  }

  /**
   * @return mixed
   */
  public function getIssuer() {
    return $this->issuer;
  }

  /**
   * @param mixed $issuer
   */
  public function setIssuer($issuer) {
    $this->issuer = $issuer;
  }

  /**
   * @return mixed
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * @param mixed $subject
   */
  public function setSubject($subject) {
    $this->subject = $subject;
  }

  /**
   * @return int
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param int $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @param \LabhubCredentials $credentials
   *
   * @return bool
   */
  public function equals(LabhubCredentials $credentials) {
    if ($this->type != $credentials->getType()) {
      return FALSE;
    }
    if ($this->secret != $credentials->getSecret()) {
      return FALSE;
    }

    switch ($this->type) {
      case self::TYPE_JWT:
        if ($this->issuer != $credentials->getIssuer()) {
          return FALSE;
        }
        if ($this->subject != $credentials->getSubject()) {
          return FALSE;
        }
        break;
      case self::TYPE_USERPASS:
        // Is default case
      default:
        // nothing to do
        break;
    }

    // If no test is negative, instances can be considered equal
    return TRUE;
  }

  public function getForm() {

    $form = [];

    if ($this->type == self::TYPE_USERPASS) {

      $form['fieldset_userpass'] = [
        '#type' => 'fieldset',
        '#title' => 'HTTP Basic Authorization',
      ];

      if (!empty($this->secret)) {
        $userpass = explode(':', labhub_ssl_decrypt($this->secret), 2);
        $username = $userpass[0];
        $password = $userpass[1];
      }
      else {
        $username = NULL;
        $password = NULL;
      }

      $form['fieldset_userpass']['username'] = [
        '#type' => 'textfield',
        '#title' => t('Username'),
        '#default_value' => $username,
        '#required' => TRUE,
      ];

      $form['fieldset_userpass']['password'] = [
        '#type' => 'password',
        '#title' => t('Password'),
        '#required' => empty($password) ? TRUE : FALSE,
      ];

    }
    elseif ($this->type == self::TYPE_JWT) {

      $form['fieldset_jwt'] = [
        '#type' => 'fieldset',
        '#title' => 'JSON Web Token Authorization',
      ];

      $options = [];
      $options['none'] = 'Please choose a signing algorithm.';
      labhub_include_lib_jwt();
      $algorithms = JWT::$supported_algs;
      foreach ($algorithms as $key => $value) {
        $options[$key] = $key;
      }

      $form['fieldset_jwt']['algorithm'] = [
        '#type' => 'select',
        '#title' => 'Token signing algorithm',
        '#options' => $options,
        '#default_value' => $this->algorithm,
        '#required' => TRUE,
      ];

      $form['fieldset_jwt']['key'] = [
        '#type' => 'password',
        '#title' => 'Shared secret (key)',
        '#required' => empty($this->secret) ? TRUE : FALSE,
      ];

      $form['fieldset_jwt']['iss'] = [
        '#type' => 'textfield',
        '#title' => t('Issuer'),
        '#description' => t('Issuer string for JWT Authentication process.'),
        '#default_value' => $this->issuer,
        '#required' => TRUE,
      ];

      $form['fieldset_jwt']['sub'] = [
        '#type' => 'textfield',
        '#title' => t('Subject'),
        '#description' => t('Subject string for JWT Authentication process.'),
        '#default_value' => $this->subject,
        '#required' => TRUE,
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];

    return $form;
  }

  public function save() {
    if ($id = LabhubCredentialsRepository::save($this)) {
      $this->id = $id;
    }
  }

  /**
   * @return array
   */
  public function dbInsertFields() {
    return [
      'context' => $this->getContext(),
      'type' => $this->getType(),
      'secret' => $this->getSecret(),
      'algorithm' => $this->getAlgorithm(),
      'issuer' => $this->getIssuer(),
      'subject' => $this->getSubject(),
    ];
  }
}