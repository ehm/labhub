<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


abstract class LabhubHttpResources {

  /**
   * Methods analog to definition in PECL HTTP module as HTTP_METH_*
   */
  const METHOD_HTTP_GET = 1;

  const METHOD_HTTP_POST = 3;

  const METHOD_HTTP_PUT = 4;

  const METHOD_HTTP_DELETE = 5;

  public static $methods = [
    // @link http://php.net/manual/en/http.constants.php
    'GET' => self::METHOD_HTTP_GET,
    'POST' => self::METHOD_HTTP_POST,
    'PUT' => self::METHOD_HTTP_PUT,
    'DELETE' => self::METHOD_HTTP_DELETE,
  ];

  /**
   * HTTP status codes as of 2018/04/28 according to
   * https://www.iana.org/assignments/http-status-codes
   *
   * @var array
   */
  public static $http_status_codes = [
    100 => "Continue",
    101 => "Switching Protocols",
    102 => "Processing",
    103 => "Early Hints",
    200 => "OK",
    201 => "Created",
    202 => "Accepted",
    203 => "Non-Authoritative Information",
    204 => "No Content",
    205 => "Reset Content",
    206 => "Partial Content",
    207 => "Multi-Status",
    208 => "Already Reported",
    300 => "Multiple Choices",
    301 => "Moved Permanently",
    302 => "Found",
    303 => "See Other",
    304 => "Not Modified",
    305 => "Use Proxy",
    306 => "(Unused)",
    307 => "Temporary Redirect",
    308 => "Permanent Redirect",
    400 => "Bad Request",
    401 => "Unauthorized",
    402 => "Payment Required",
    403 => "Forbidden",
    404 => "Not Found",
    405 => "Method Not Allowed",
    406 => "Not Acceptable",
    407 => "Proxy Authentication Required",
    408 => "Request Timeout",
    409 => "Conflict",
    410 => "Gone",
    411 => "Length Required",
    412 => "Precondition Failed",
    413 => "Payload Too Large",
    414 => "URI Too Long",
    415 => "Unsupported Media Type",
    416 => "Range Not Satisfiable",
    417 => "Expectation Failed",
    421 => "Misdirected Request",
    422 => "Unprocessable Entity",
    423 => "Locked",
    424 => "Failed Dependency",
    426 => "Upgrade Required",
    428 => "Precondition Required",
    429 => "Too Many Requests",
    431 => "Request Header Fields Too Large",
    451 => "Unavailable For Legal Reasons",
    500 => "Internal Server Error",
    501 => "Not Implemented",
    502 => "Bad Gateway",
    503 => "Service Unavailable",
    504 => "Gateway Timeout",
    505 => "HTTP Version Not Supported",
    506 => "Variant Also Negotiates",
    507 => "Insufficient Storage",
    508 => "Loop Detected",
    510 => "Not Extended",
    511 => "Network Authentication Required",
  ];

  /**
   * @param int $code HTTP response code
   *
   * @return string The human readable explanation for the HTTP code.
   */
  public static function translateHttpResponseCode($code) {
    return array_key_exists($code,
      self::$http_status_codes) ? self::$http_status_codes[$code] : 'Not a valid HTTP response';
  }
}