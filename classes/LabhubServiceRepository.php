<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class LabhubServiceRepository implements LabhubObjectRepositoryInterface {

  use LabhubObjectRepositoryTrait;

  public static $table_name = 'labhub_service';

  private static $database_fields = [
    'id',
    'service_name',
    'service_type',
    'host',
  ];

  private static $class_name = 'LabhubService';

  /**
   * @param $service_type
   *
   * @return \LabhubService[]
   */
  public static function getAllByType($service_type) {

    // Do nothing, if not a valid service type
    $types = labhub_types();
    if (!array_key_exists($service_type, $types)) {
      return [];
    }

    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t', self::$database_fields)
        ->condition('t.service_type', $service_type, '=')
        ->execute()
        ->fetchAll();

    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return [];
    }

    return LabhubService::dbResultsToObjects($result);
  }

}