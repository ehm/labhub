<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


trait LabhubApiClientTrait {

  private $route = '',
    $headers = [],
    $method = LabhubHttpResources::METHOD_HTTP_GET,
    $response_code = NULL,
    $data = NULL;

  /**
   * @var \LabhubCredentials
   */
  private $credentials = NULL;

  /**
   * @var \LabhubServiceInterface
   */
  private $service;

  /**
   * @var resource $handle cURL handle
   */
  private $handle;

  public function __construct(LabhubServiceInterface $service) {
    $this->service = $service;
  }

  /**
   * @param string $header
   */
  private function addHeader($header) {
    $this->headers[] = $header;
  }

  /**
   * @param int $code HTTP response code
   *
   * @return string The human readable explanation for the HTTP code.
   */
  public static function translateHttpResponseCode($code) {
    return LabhubHttpResources::translateHttpResponseCode($code);
  }

  /**
   * @return \LabhubCredentials
   */
  public function getCredentials() {
    return $this->credentials;
  }

  /**
   * @param \LabhubCredentials $credentials
   */
  public function setCredentials($credentials) {

    // Set the
    $this->credentials = $credentials;


    $type = $this->credentials->getType();
    if ($type == LabhubCredentials::TYPE_USERPASS) {
      $userpass = base64_encode(labhub_ssl_decrypt($this->credentials->getSecret()));
      $this->addHeader('Authorization: Basic ' . $userpass);
    }
    elseif ($type == LabhubCredentials::TYPE_JWT) {
      labhub_include_lib_jwt();

      $payload = [];
      $payload['iss'] = $this->credentials->getIssuer();
      $payload['sub'] = $this->credentials->getSubject();
      $key = labhub_ssl_decrypt($this->credentials->getSecret());
      $token = \Firebase\JWT\JWT::encode($payload, $key);
      $this->addHeader('Authorization: Bearer ' . $token);
    }
    elseif ($type == LabhubCredentials::TYPE_API_KEY) {
      $key = labhub_ssl_decrypt($this->credentials->getSecret());
      $this->addHeader('apiKey: ' . $key);
    }
  }

  /**
   * @return mixed
   */
  public function getRoute() {
    return $this->route;
  }

  /**
   * @param mixed $route
   */
  public function setRoute($route) {
    $this->route = $route;
  }

  /**
   * @return int
   */
  public function getMethod() {
    return array_keys(LabhubHttpResources::$methods, $this->method)[0];
  }

  /**
   * @param int $method
   */
  public function setMethod($method) {
    if (array_key_exists($method, LabhubHttpResources::$methods)) {
      $this->method = LabhubHttpResources::$methods[$method];
    }
    elseif (in_array($method, LabhubHttpResources::$methods)) {
      $this->method = $method;
    }
  }

  /**
   * @return null
   */
  public function getResponseCode() {
    return $this->response_code;
  }

  /**
   * @param null $response_code
   */
  public function setResponseCode($response_code) {
    if (in_array($response_code, LabhubHttpResources::$http_status_codes)) {
      $this->response_code;
    };
  }

  /**
   * @return null
   */
  public function getData() {
    return $this->data;
  }

  /**
   * @param null $data
   */
  public function setData($data) {
    $this->data = $data;
  }

  private function init() {
    if (isset($this->route)) {
      $route = $this->service->getHost() . '/' . $this->getRoute();
    }
    else {
      $route = $this->service->getHost();
    }
    $this->handle = curl_init($route);
  }

  private function execute() {

    /**
     * Default options. Override if needed.
     */
    // Verbose for debugging
    curl_setopt($this->handle, CURLOPT_VERBOSE, TRUE);
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, TRUE);
    // Should return headers be passed with response data?
    curl_setopt($this->handle, CURLOPT_HEADER, FALSE);

    /**
     * Handling of request execution
     */
    // Set HTTP method
    curl_setopt($this->handle, CURLOPT_CUSTOMREQUEST, $this->getMethod());
    // Add collected headers to handle
    curl_setopt($this->handle, CURLOPT_HTTPHEADER, $this->headers);

    // execute request
    $result = curl_exec($this->handle);

    $this->response_code = curl_getinfo($this->handle, CURLINFO_HTTP_CODE);

    return $result;
  }

}