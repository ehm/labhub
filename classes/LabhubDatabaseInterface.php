<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


/**
 * Interface LabhubDatabaseInterface
 */
interface LabhubDatabaseInterface extends LabhubServiceInterface {

  public function getDatabaseName();

  public function setDatabaseName($database_name);

  public function setUsername($username);

  public function getUsername();

  public function setPassword($password);

  public function getPassword();

  public function activate();

  public function deactivate();

  /**
   * @return \LabhubAuthentication
   */
  public function getAuthentication();

  /**
   * @param \LabhubServiceInterface $service
   * @param \LabhubAuthentication $authentication
   * @param int $id
   *
   * @return \LabhubDatabaseRepository
   */
  public static function cast(
    LabhubServiceInterface $service,
    LabhubAuthentication $authentication,
    $id = self::EMPTY_ID
  );

  /**
   * @return bool
   */
  public function testConnection();

  /**
   * @return array
   */
  public function getFormCreate();

  /**
   * @return array
   */
  public function getFormUpdate();


  /**
   * @return void
   */
  public function save();

  /**
   * @return bool
   */
  public function isEmpty();
}