<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubAuthenticationRepository implements LabhubObjectRepositoryInterface {

  use LabhubObjectRepositoryTrait;

  private static $table_name = 'labhub_authentication';

  private static $database_fields = [
    'id',
    'type',
    'username',
    'password',
    'resource',
  ];

  private static $class_name = 'LabhubAuthentication';

}