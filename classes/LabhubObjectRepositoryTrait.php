<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


trait LabhubObjectRepositoryTrait {

  public static function save(LabhubObjectInterface $object) {
    try {
      if ($object->isEmpty()) {
        $id = NULL;
      }
      else {
        $id = $object->getId();
      }
      db_merge(self::$table_name)
        ->key(['id' => $id])
        ->fields($object->dbInsertFields())
        ->execute();

      return Database::getConnection()->lastInsertId();

    } catch (Exception $e) {

      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');
    }
    return FALSE;

  }

  /**
   * @param $id
   *
   * @return \LabhubObjectInterface
   */
  public static function getById($id) {

    $classname = self::$class_name;

    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t', self::$database_fields)
        ->condition('t.id', $id, '=')
        ->range(0, 1)
        ->execute()
        ->fetch();

    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return new $classname();
    }

    return $classname::dbResultToObject($result);
  }


  public static function getAll() {

    $classname = self::$class_name;

    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t', self::$database_fields)
        ->execute()
        ->fetchAll();

    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return [];
    }

    return $classname::dbResultsToObjects($result);
  }

}