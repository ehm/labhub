<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


interface LabhubObjectInterface {

  const EMPTY_ID = -1;

  /**
   * @param \stdClass[] $results
   *
   * @return \LabhubObjectInterface[]
   */
  public static function dbResultsToObjects($results);

  /**
   * @param \stdClass $result
   *
   * @return \LabhubObjectInterface
   */
  public static function dbResultToObject($result);

  /**
   * @return int
   */
  public function getId();

  /**
   * @param int $id
   *
   * @return void
   */
  public function setId($id);

  /**
   * @param \stdClass $object
   *
   * @return void
   */
  public function parseObject(stdClass $object);

  /**
   * @return void
   */
  public function save();

  /**
   * @return array
   */
  public function dbInsertFields();

  /**
   * @return bool
   */
  public function isEmpty();

}