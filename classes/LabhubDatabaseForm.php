<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


abstract class LabhubDatabaseForm {

  /**
   * @var \LabhubDatabaseInterface
   */
  private static $database;

  public static function create(LabhubDatabaseInterface $database) {
    self::$database = $database;

    $form = LabhubServiceForm::create($database);

    $fields = [
      'database_name',
      'username',
      'password',
    ];

    foreach ($fields as $field) {
      $form[$field] = self::getFormField($field);
    }

    return $form;
  }

  public static function update(LabhubDatabaseInterface $database) {
    $form = self::create($database);

    // modify create form

    return $form;
  }

  private static function getFormField($field) {
    switch ($field) {
      case 'database_name':
        return [
          '#type' => 'textfield',
          '#title' => t('Database Name'),
          '#default_value' => self::$database->getDatabaseName(),
          '#required' => TRUE,
        ];
        break;
      case 'username':
        return [
          '#type' => 'textfield',
          '#title' => t('Username'),
          '#default_value' => self::$database->getUsername(),
          '#required' => TRUE,
        ];
        break;
      case 'password':
        return [
          '#type' => 'password',
          '#title' => t('Password'),
          '#required' => TRUE,
        ];
        break;
      default:
        return [];
    }
  }
}