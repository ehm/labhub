<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class PeakRepository implements EhmObjectRepositoryInterface {

  use EhmObjectRepositoryTrait;
  use EhmObjectCacheTrait;

  private static $table_name = 'peak';

  private static $class_name = 'Peak';

}