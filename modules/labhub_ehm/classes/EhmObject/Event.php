<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class Event
 */
class Event implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idEvent';

  private $idEvent = 0,
    $idParent = 1,
    $idEventType,
    $idLabware,
    $num,
    $name,
    $bGlobal,
    $target_date,
    $schedule_date,
    $date,
    $duration,
    $modified;

  /**
   * @param \Event[] $events
   *
   * @return string Rendered table of input array Event instances
   */
  public static function basicTable($events) {
    $header = [
      'ID',
      'Name',
      'Type',
      'Labware',
      'Target Date',
      'Scheduled Date',
      'Date',
    ];

    $rows = [];

    foreach ($events as $event) {
      $rows[] = [
        l($event->getId(),
          str_replace('%', $event->getId(), LABHUB_EHM_URL_EXPERIMENT_VIEW)),
        $event->getName(),
        $event->getIdEventType(),
        $event->getIdLabware(),
        $event->getTargetDate(),
        $event->getScheduleDate(),
        $event->getDate(),
      ];
    }

    try {
      return theme('table', ['header' => $header, 'rows' => $rows]);
    } catch (Exception $e) {
      watchdog_exception('LabHub EHM', $e);
      return '';
    }
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return mixed
   */
  public function getIdEventType() {
    return $this->idEventType;
  }

  /**
   * @return mixed
   */
  public function getIdLabware() {
    return $this->idLabware;
  }

  /**
   * @return mixed
   */
  public function getTargetDate() {
    return $this->target_date;
  }

  /**
   * @return mixed
   */
  public function getScheduleDate() {
    return $this->schedule_date;
  }

  /**
   * @return mixed
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @return int
   */
  public function getIdParent() {
    return $this->idParent;
  }

  /**
   * @param int $idParent
   */
  public function setIdParent($idParent) {
    $this->idParent = $idParent;
  }

  /**
   * @param mixed $idEventType
   */
  public function setIdEventType($idEventType) {
    $this->idEventType = $idEventType;
  }

  /**
   * @param mixed $idLabware
   */
  public function setIdLabware($idLabware) {
    $this->idLabware = $idLabware;
  }

  /**
   * @return mixed
   */
  public function getNum() {
    return $this->num;
  }

  /**
   * @param mixed $num
   */
  public function setNum($num) {
    $this->num = $num;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getBGlobal() {
    return $this->bGlobal;
  }

  /**
   * @param mixed $bGlobal
   */
  public function setBGlobal($bGlobal) {
    $this->bGlobal = $bGlobal;
  }

  /**
   * @param mixed $target_date
   */
  public function setTargetDate($target_date) {
    $this->target_date = $target_date;
  }

  /**
   * @param mixed $schedule_date
   */
  public function setScheduleDate($schedule_date) {
    $this->schedule_date = $schedule_date;
  }

  /**
   * @param mixed $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  /**
   * @return mixed
   */
  public function getDuration() {
    return $this->duration;
  }

  /**
   * @param mixed $duration
   */
  public function setDuration($duration) {
    $this->duration = $duration;
  }

  /**
   * @return mixed
   */
  public function getModified() {
    return $this->modified;
  }

  /**
   * @param mixed $modified
   */
  public function setModified($modified) {
    $this->modified = $modified;
  }

  /**
   * ToDo: doc
   *
   * @return string
   */
  public function displayPage() {
    drupal_set_title("Event $this->name");

    $display = "";

    $child_events = new LabhubEhmGenericInventoryList("event",
      ['idParent' => $this->getId()]);
    $child_events->addHeader("Child Events");

    $display .= $child_events->basicTable();

    return $display;
  }

  public function save() {
    $this->setId(EventRepository::save($this));
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'event',
        'id' => $this->getId(),
        'attributes' => [
          'num' => $this->num,
          'name' => $this->name,
          'bGlobal' => $this->bGlobal,
          'target_date' => $this->target_date,
          'schedule_date' => $this->schedule_date,
          'date' => $this->date,
          'duration' => $this->duration,
          'modified' => $this->modified,
        ],
        'relationships' => [
          'idParent' => [
            'data' => [
              'type' => 'event',
              'id' => $this->idParent,
            ],
          ],
          'idEventType' => [
            'data' => [
              'type' => 'eventtype',
              'id' => $this->idEventType,
            ],
          ],
          'idLabware' => [
            'data' => [
              'type' => 'labware',
              'id' => $this->idLabware,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }
}