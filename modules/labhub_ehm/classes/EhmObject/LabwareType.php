<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabwareType implements EhmObjectInterface {

  use EhmObjectTrait;

  const TYPE_PLATE = 'plate';

  const TYPE_WELL = 'well';

  const TYPE_LIBRARY = 'library';

  private static $id_fieldname = 'idLabwareType';

  private $idLabwareType = 0,
    $idParent = 1,
    $name = '',
    $description = '',
    $type = self::TYPE_PLATE;

  /**
   * @param $idParent
   */
  public function setIdParent(
    $idParent
  ) {
    $this->idParent = $idParent;
  }

  /**
   * @param string $name
   */
  public function setName(
    $name
  ) {
    $this->name = $name;
  }

  /**
   * @param string $description
   */
  public function setDescription(
    $description
  ) {
    $this->description = $description;
  }

  /**
   * @param string $type
   */
  public function setType(
    $type
  ) {
    $this->type = $type;
  }

  public function getForm() {
    $form = [];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => 'Label',
      '#required' => TRUE,
      '#default_value' => $this->getName(),
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => 'Description',
      '#required' => TRUE,
      '#default_value' => $this->getDescription(),
    ];

    $options = LabwareType::types();
    $form['type'] = [
      '#type' => 'select',
      '#title' => 'Type',
      '#options' => $options,
      '#default_value' => $this->getType(),
      '#ajax' => [
        'callback' => 'labhub_ehm_labware_type_ajax_callback',
        'wrapper' => 'form-labwaretype-wrapper',
      ],
    ];

    $types = LabwareTypeRepository::getAll();

    $options = [];
    foreach ($types as $type) {
      $options[$type->getId()] = $type->getName();
    }
    $form['parent'] = [
      '#type' => 'select',
      '#title' => 'Parent Type',
      '#options' => $options,
      '#default_value' => $this->getIdParent(),
      '#ajax' => [
        'callback' => 'labhub_ehm_labware_parent_ajax_callback',
        'wrapper' => 'form-labwaretype-wrapper',
      ],
    ];

    return $form;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  public static function types() {
    return [
      self::TYPE_WELL => 'Well',
      self::TYPE_PLATE => 'Microwell plate',
      self::TYPE_LIBRARY => 'Library',
    ];
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @return null
   */
  public function getIdParent() {
    return $this->idParent;
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'labwaretype',
        'id' => $this->getId(),
        'attributes' => [
          'name' => $this->name,
          'description' => $this->description,
          'type' => $this->type,
        ],
        'relationships' => [
          'idParent' => [
            'data' => [
              'type' => 'labwaretype',
              'id' => $this->idParent,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }

  public function save() {
    $this->setId(LabwareTypeRepository::save($this));
  }

}
