<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class EventType implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idEventType';

  private static $units = [
    'nl' => 'Nanoliter nl',
    'µl' => 'Microliter µl',
    'ml' => 'Milliliter ml',
    'nM' => 'Nanomol nM',
    'µM' => 'Micromol µM',
    'mM' => 'Millimol mM',
    '%' => 'percent %',
    'Hz' => 'Herz Hz',
  ];

  private $idEventType = 0,
    $name,
    $description,
    $idSolution,
    $volume,
    $unit,
    $frequency,
    $voltage,
    $duration;

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return mixed
   */
  public function getIdSolution() {
    return $this->idSolution;
  }

  /**
   * @param mixed $idSolution
   */
  public function setIdSolution($idSolution) {
    $this->idSolution = $idSolution;
  }

  /**
   * @return mixed
   */
  public function getVolume() {
    return $this->volume;
  }

  /**
   * @param mixed $volume
   */
  public function setVolume($volume) {
    $this->volume = $volume;
  }

  /**
   * @return mixed
   */
  public function getUnit() {
    return $this->unit;
  }

  /**
   * @param mixed $unit
   */
  public function setUnit($unit) {
    $this->unit = $unit;
  }

  /**
   * @return mixed
   */
  public function getFrequency() {
    return $this->frequency;
  }

  /**
   * @param mixed $frequency
   */
  public function setFrequency($frequency) {
    $this->frequency = $frequency;
  }

  /**
   * @return mixed
   */
  public function getVoltage() {
    return $this->voltage;
  }

  /**
   * @param mixed $voltage
   */
  public function setVoltage($voltage) {
    $this->voltage = $voltage;
  }

  /**
   * @return mixed
   */
  public function getDuration() {
    return $this->duration;
  }

  /**
   * @param mixed $duration
   */
  public function setDuration($duration) {
    $this->duration = $duration;
  }

  public function save() {
    $this->setId(EventTypeRepository::save($this));
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'eventtype',
        'id' => $this->getId(),
        'attributes' => [
          'name' => $this->name,
          'description' => $this->description,
          'volume' => $this->volume,
          'unit' => $this->unit,
          'frequency' => $this->frequency,
          'voltage' => $this->voltage,
          'duration' => $this->duration,
        ],
        'relationships' => [
          'idSolution' => [
            'data' => [
              'type' => 'solution',
              'id' => $this->idSolution,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }
}