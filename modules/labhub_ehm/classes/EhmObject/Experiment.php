<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class Experiment implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idEvent';

  private $idEvent = 0,
    $idPerson,
    $name,
    $comment;

  /**
   * @var \Event
   */
  private $event = NULL;

  /**
   * @var \Labware
   */
  private $labware = NULL;

  /**
   * @var \LabwareType
   */
  private $labwaretype = NULL;

  /**
   * @var \Labware[][]
   */
  private $wells = [];

  /**
   * @var \Solution
   */
  private $solution = NULL;

  /**
   * @var \Event[] $events
   */
  private $events = [];

  private $celltypes = [];

  /**
   * @param int $rowNumber
   *
   * @return mixed
   */
  private static function rowNumToChar($rowNumber) {
    $alphabet = range('A', 'Z');
    return $alphabet[$rowNumber - 1];
  }

  /**
   * @return \LabwareType
   */
  public function getLabwaretype() {
    if (is_null($this->labwaretype)) {
      $this->labwaretype = new LabwareType();
    }
    return $this->labwaretype;
  }

  /**
   * @param \LabwareType $labwaretype
   */
  public function setLabwaretype($labwaretype) {
    $this->labwaretype = $labwaretype;
  }

  /**
   * @param \Event[] $events
   */
  public function setEvents($events) {
    $this->events = $events;
  }

  /**
   * @param \Labware[][] $wells
   */
  public function setWells($wells) {
    $this->wells = $wells;
  }

  /**
   * @param array $celltypes
   */
  public function setCelltypes($celltypes) {
    $this->celltypes = $celltypes;
  }

  /**
   * @param mixed $idPerson
   */
  public function setIdPerson($idPerson) {
    $this->idPerson = $idPerson;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @param mixed $comment
   */
  public function setComment($comment) {
    $this->comment = $comment;
  }

  public function displayPage() {

    drupal_set_title("Experiment: " . $this->name);

    $display = "";

    $display .= $this->displayPageHeader();

    $display .= $this->displayCellTypes();

    $casting_event_ids = variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_CASTING);
    $casting_event_ids = implode(",", $casting_event_ids);
    $conditions = [
      'idParent' => $this->getId(),
      'idEventType' => $casting_event_ids,
    ];
    $casting_events = EventRepository::getAllByConditions($conditions);
    /** @var \Event[] $casting_events */

    /** @var \Event[] $well_casting_events */
    $well_casting_events = [];
    foreach ($casting_events as $event) {
      $well_casting_events[$event->getIdLabware()] = $event;
    }

    $display .= "<h2>Labware:</h2>";
    $plate_type = PlateTypeRepository::getById($this->getLabwaretype()
      ->getId());
    /** @var \PlateType $plate_type */
    $num_wells = $plate_type->getWells();
    $num_rows = $plate_type->getPlateRows();
    $num_cols = $plate_type->getPlateCols();
    if (count($wells = $this->wells) == $num_wells) {
      $data = [];
      foreach ($wells as $well) {
        /**
         * @var Labware $well
         */
        $event = $well_casting_events[$well->getId()];
        $data[$well->getRow()][$well->getCol()]['data'] = l($well->getName(),
          self::url_sample($this->getId(), $event->getId()));
        $data[$well->getRow()][$well->getCol()]['data-toggle'] = "tooltip";
        $data[$well->getRow()][$well->getCol()]['title'] = 'id: ' . $event->getId();
        //$data[$well->getRow()][$well->getCol()]['class'] = 'nureintest';
      }
      try {
        $display .= theme('table', [
          'header' => [],
          'rows' => $data,
          'attributes' => ['class' => 'labhub_ehm_wells'],
        ]);
      } catch (Exception $e) {
        watchdog_exception(__CLASS__, $e);
      }
    }

    $display .= "<h2>Events:</h2>";

    if ($count_wells = count($casting_events)) {
      $display .= '<h3>EHM Sample Casting</h3>';
      $header = [
        'Date',
        'Person',
        'Mastermix',
        'Volume',
        'Concentration',
        'Nr of samples',
      ];

      /**
       * Only use first entry, b/c every labware-well has an event
       */
      $casting_event = $casting_events[0];
      /** @var Event $casting_event */

      $eventtype = EventTypeRepository::getById($casting_event->getIdEventType());
      /** @var \EventType $eventtype */

      $solution = $this->getSolution();
      /** @var  \Solution $solution */

      $ehm = EHMRepository::getById($casting_event->getId());
      /** @var \EHM $ehm */

      $person = PersonRepository::getById($ehm->getIdPerson());
      /** @var \Person $person */

      $rows = [];
      $rows[] = [
        $casting_event->getDate(),
        $person->displayName(TRUE),
        $solution->getName(),
        $eventtype->getVolume() . ' ' . $eventtype->getUnit(),
        $solution->getConcentration() . ' ' . $solution->getUnit(),
        $count_wells,
      ];

      try {
        $display .= theme('table', ['header' => $header, 'rows' => $rows]);
      } catch (Exception $e) {
        watchdog_exception(__CLASS__, $e);
      }
    }

    $events = $this->events;
    foreach ($events as $key => $value) {
      /** @var Event $value */
      if (!($value->getIdParent() == $this->getId()
        && $value->getIdLabware() == $this->labware->getId())) {
        unset($events[$key]);
      }
    }

    $measurements = [];

    if (count($events)) {
      $header = ['Event ID', 'Event Type', 'Name', 'Target Date'];
      $rows = [];
      foreach ($events as $event) {
        /** @var Event $event */
        $type = EventTypeRepository::getById($event->getIdEventType());
        /** @var \EventType $type */
        $type = $type->getName();
        if ($type == 'measurement') {
          $measurements[] = $event;
        }
        else {

          $rows[] = [
            $event->getId(),
            $type,
            $event->getName(),
            $event->getTargetDate(),
          ];
        }
      }
      try {
        $display_events = '<h3>Other events</h3>' . theme('table',
            ['header' => $header, 'rows' => $rows]);
      } catch (Exception $e) {
        watchdog_exception('LabHub EHM', $e);
        $display_events = '';
      }
    }

    /** @var \Event[] $measurements */
    $rows = [];
    foreach ($measurements as $measurement_event) {
      $measurement = MeasurementRepository::getById($measurement_event->getId());
      /** @var \Measurement $measurement */
      $rows[] = $measurement->tableRow();
    }
    try {
      $display_measurements = '<h3>Measurements</h3>' . theme('table',
          ['header' => Measurement::tableHeader(), 'rows' => $rows]);
    } catch (Exception $e) {
      watchdog_exception('LabHub EHM', $e);
      $display_measurements = '';
    }

    if (isset($display_measurements)) {
      $display .= $display_measurements;
    }

    if (isset($display_events)) {
      $display .= $display_events;
    }

    _labhub_ehm_include_css();
    return $display;
  }

  public function displayPageHeader() {
    $person = PersonRepository::getById($this->idPerson);
    /** @var Person $person */

    $name = $person->displayName(TRUE);
    $display = "<div><strong>Process Owner</strong>: $name</div>";

    $plate_name = $this->getLabware()->getName();
    $display .= "<div><strong>Labware</strong>: $plate_name</div>";

    $plate_type = $this->getLabwaretype()->getName();
    $display .= "<div><strong>Labware Type</strong>: $plate_type</div>";

    return $display;
  }

  /**
   * @return \Labware
   */
  public function getLabware() {
    if (is_null($this->labware)) {
      $this->labware = new Labware();
    }
    return $this->labware;
  }

  /**
   * @param \Labware $labware
   */
  public function setLabware($labware) {
    $this->labware = $labware;
  }

  private function displayCellTypes() {
    if (count($this->celltypes)) {
      $header = ['idCelltype', 'Name', 'Species', 'Description'];
      $rows = [];
      foreach ($this->celltypes as $celltype) {
        $rows[] = [
          $celltype->id,
          $celltype->attributes->name,
          $celltype->attributes->species,
          $celltype->attributes->description,
        ];
      }
      try {
        return '<h2>Cell Lines</h2>' . theme('table',
            ['header' => $header, 'rows' => $rows]);
      } catch (Exception $exception) {
        watchdog_exception(__FUNCTION__, $exception);
      }
    }
    return "";
  }

  public static function url_sample($experiment_id, $sample_id) {
    return self::url_from_id($experiment_id) . '/' . $sample_id;
  }

  public static function url_from_id($id) {
    return base_path() . LABHUB_EHM_URL_EXPERIMENT . '/' . $id;
  }

  /**
   * @return \Solution
   */
  public function getSolution() {
    if (is_null($this->solution)) {
      $this->solution = $this->fetchSolution();
    }
    return $this->solution;
  }

  /**
   * @param \Solution $solution
   */
  public function setSolution($solution) {
    $this->solution = $solution;
  }

  public function getMeasurementEventIDs() {
    $measurements = [];
    foreach ($this->events as $event) {
      /** @var Event $event */
      $type = EventTypeRepository::getById($event->getIdEventType());
      /** @var \EventType $type */
      $type = $type->getName();
      if ($type == 'measurement') {
        $measurements[] = $event->getId();
      }
    }
    return $measurements;
  }

  public function getForm() {
    $form = [];

    // Experiment Name
    $name = $this->getName();
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => 'Experiment name',
      '#required' => TRUE,
      '#default_value' => $name,
    ];

    $form['fieldset_staff'] = [
      '#type' => 'fieldset',
      '#title' => 'Staff information',
    ];

    // Process owner
    $persons = PersonRepository::getAll();
    $options_person = [];
    foreach ($persons as $person) {
      /**
       * @var Person $person
       */
      $options_person[$person->getId()] = $person->displayName();
    }
    $form['fieldset_staff']['idPerson'] = [
      '#type' => 'select',
      '#title' => 'Process Owner',
      '#options' => $options_person,
      '#required' => TRUE,
      '#default_value' => $this->idPerson,
    ];

    // Executing person
    $form['fieldset_staff']['person_execution'] = [
      '#type' => 'select',
      '#title' => 'Process Execution',
      '#options' => $options_person,
      '#required' => TRUE,
    ];


    // EHM Cast Date
    $form['fieldset_date'] = [
      '#type' => 'fieldset',
      '#title' => 'EHM casting date (begin of experiment)',
    ];

    $event = $this->getEvent();
    $form['fieldset_date']['date'] = [
      //'#title' => t('Date'),
      '#date_format' => 'Y-m-d H:i',
      '#type' => 'date_popup',
      '#default_value' => !empty($date = $event->getDate()) ? $date : date("Y-m-d H:i"),
      '#required' => TRUE,
    ];

    $form['fieldset_labware'] = [
      '#type' => 'fieldset',
      '#title' => 'Specify labware',
    ];
    // Plate Type
    $labwaretypes = LabwareTypeRepository::getAllByConditions(['type' => LabwareType::TYPE_PLATE]);
    $options_labwaretypes = [];
    foreach ($labwaretypes as $labwaretype) {
      /** @var \LabwareType $labwaretype */
      $options_labwaretypes[$labwaretype->getId()] = $labwaretype->getName() . ': ' . $labwaretype->getDescription();
    }
    $form['fieldset_labware']['labwaretype'] = [
      '#type' => 'select',
      '#title' => 'Choose Labware Type',
      '#options' => $options_labwaretypes,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => 'labhub_ehm_experiment_select_labwaretype_ajax',
        'wrapper' => 'ehm-experiment-form-wrapper',
      ],
    ];

    // Plate ID
    $options_labware = [];
    $form['fieldset_labware']['labware'] = [
      '#type' => 'select',
      '#title' => 'Choose Labware',
      '#prefix' => '<div id="ehm-experiment-form-wrapper-labware">',
      '#suffix' => '</div>',
      '#options' => $options_labware,
      '#required' => TRUE,
      '#validated' => TRUE,
    ];

    $form['fieldset_wells'] = [
      '#type' => 'fieldset',
      '#title' => 'Select Wells',
    ];

    $options = self::well_checkboxes_options($rows = 8, $columns = 6);
    $form['fieldset_wells']['wells'] = [
      '#type' => 'checkboxes',
      '#multicolumn' => ['width' => $columns],
      '#options' => $options,
      '#prefix' => '<div id="ehm-experiment-form-wrapper-wells">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => 'labhub_ehm_experiment_select_wells_ajax',
        'wrapper' => 'ehm-experiment-form-wrapper-wells',
        'speed' => 'fast',
        'progress' => ['type' => 'none'],
      ],
    ];


    // Mastermix
    // Get mastermix information from child-event

    $default_solution_id = $this->getSolution()->getId();

    $solutions = SolutionRepository::getAllByConditions(['type' => 'mastermix']);
    $options_solution = [];
    foreach ($solutions as $solution) {
      /** @var \Solution $solution */
      $options_solution[$solution->getId()] = $solution->getName();
    }
    $form['mastermix'] = [
      '#type' => 'select',
      '#title' => 'Mastermix',
      '#options' => $options_solution,
      '#required' => TRUE,
      '#default_value' => $default_solution_id,
    ];

    $form['#prefix'] = '<div id="ehm-experiment-form-wrapper">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  public function getName() {
    return $this->name;
  }

  /**
   * @return \Event
   */
  public function getEvent() {
    if (is_null($this->event)) {
      $this->event = new Event();
    }
    return $this->event;
  }

  public static function well_checkboxes_options($rows, $columns) {
    $options = [];
    $alphabet = range('A', 'Z');
    for ($col = 1; $col <= $columns; $col++) {
      for ($row = 0; $row < $rows; $row++) {
        $count = $col + ($row * $columns);
        $options[$count] = (string) $alphabet[$row] . $col;
      }
    }
    return $options;
  }

  public function url() {
    return self::url_from_id($this->getId());
  }

  public function save() {
    $this->setId(ExperimentRepository::save($this));
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'experiment',
        'id' => $this->getId(),
        'attributes' => [
          'name' => $this->name,
          'comment' => $this->comment,
        ],
        'relationships' => [
          'idPerson' => [
            'data' => [
              'type' => 'person',
              'id' => $this->idPerson,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }

  /**
   * Returns the @see \Solution instance used in EHM casting event associated
   * with this experiment.
   *
   * @return \EhmObjectInterface|\Solution
   */
  private function fetchSolution() {
    if ($this->isEmpty()) {
      return new Solution();
    }
    else {
      $casting_event_ids = variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_CASTING);
      $casting_event_ids = implode(",", $casting_event_ids);
      $conditions = [
        'idParent' => $this->getId(),
        'idEventType' => $casting_event_ids,
      ];
      $event_casting = EventRepository::getAllByConditions($conditions)[0];
      /** @var \Event $event_casting */
      $eventtype_id = $event_casting->getIdEventType();
      $eventtype = EventTypeRepository::getById($eventtype_id);
      /** @var \EventType $eventtype */
      $solution_id = $eventtype->getIdSolution();
      return SolutionRepository::getById($solution_id);
    }
  }

  /**
   * @return \Labware[] Two-dimensional array of Labware wells indexed by Row
   *   and Column ID
   */
  private function wells() {

    $labware_id = $this->getLabware()->getId();
    $labwaretype_id = $this->getLabwaretype()->getId();
    $welltype = LabwareTypeRepository::getAllByConditions([
      'type' => 'well',
      'idParent' => $labwaretype_id,
      'limit' => 1,
    ]);
    $welltype_id = $welltype[0]->getId();

    $wells = LabwareRepository::getAllByConditions([
      'idParent' => $labware_id,
      'idLabwareType' => $welltype_id,
    ]);

    /**
     * @var Labware[] $wells
     */
    return $wells;
  }
}