<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class LabwareRepository implements EhmObjectRepositoryInterface {

  use EhmObjectRepositoryTrait;
  use EhmObjectCacheTrait;

  private static $table_name = 'labware';

  private static $class_name = 'Labware';

  public static function saveNewPlate(Labware $object) {

    /**
     * First: send POST to API Server storing Labware with
     * "fake" default idParent value to comply with database constraints
     */
    $jsonObj = $object->jsonObject();
    $client_api = new LabhubEhmApiClient();
    $result = $client_api->postGeneric(self::$table_name, $jsonObj);

    // decoded nested JSON array
    $result = json_decode($result);
    $data = json_decode($result->data);

    /**
     * Plates reference their own ID as parent in labware table
     */
    $object->setId($data->id);
    $object->setIdParent($data->id);

    /**
     * Send correct parent-ID to API
     */
    $jsonObj = $object->jsonObject();
    drupal_set_message("obj: $jsonObj");
    $result = $client_api->postGeneric(self::$table_name, $jsonObj);

    // decoded nested JSON array
    drupal_set_message("result: $result");
    $result = json_decode($result);
    $data = json_decode($result->data);

    self::cache($object);

    // return ID
    return $data->id;
  }

  /**
   * @param \Labware $labware
   */
  public static function createWellsForPlate(Labware $labware) {

    $platetype = PlateTypeRepository::getById($labware->getIdLabwareType());
    /** @var \PlateType $platetype */
    $rows = $platetype->getPlateRows();
    $cols = $platetype->getPlateCols();

    $conditions = [
      'limit' => 1,
      'idParent' => $labware->getIdLabwareType(),
      'type' => LabwareType::TYPE_WELL,
    ];
    $parent_well = LabwareTypeRepository::getAllByConditions($conditions)[0];
    /** @var \LabwareType $parent_well */
    $well_type_id = $parent_well->getId();

    /**
     * Test if wells exist?
     */
    $conditions = [
      'idParent' => $labware->getId(),
      'idLabwareType' => $well_type_id,
      'type' => LabwareType::TYPE_WELL,
    ];
    $wells = LabwareRepository::getAllByConditions($conditions);

    /**
     * If number of wells in repository/database does not match nr specified in
     * platetype, try to create wells.
     *
     * Should normally only result in 0 wells or exactly the number of wells specified
     * in platetype definition, different discrepancies are handled correctly anyways:
     * If by any chance some  previously defined wells were deleted from database,
     * they will be re-created. (With database contraint-violation messages for
     * the existing wells)
     */
    if (!(count($wells) == $platetype->getWells())) {

      /**
       * Generate well data and save the instance.
       * - Name is A1, A2, ..., B2, ... row-number as alphabetical character,
       * column number as a digit
       * - Position is a numeric counter 1, 2, ..., (#wells)
       */
      $alphabet = range('A', 'Z');
      for ($row = 0; $row < $rows; $row++) {
        for ($col = 1; $col <= $cols; $col++) {
          $name = (string) ($alphabet[$row] . $col);
          $position = (($row) * $cols) + $col;

          $well = new Labware();
          $well->setIdParent($labware->getId());
          $well->setIdLabwareType($well_type_id);
          $well->setName($name);
          $well->setPos($position);
          $well->setRow($row + 1);
          $well->setCol($col);

          $well->save();
        }
      }
    }
  }

}