<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 08.06.18
 * Time: 17:37
 */
abstract class TaskRepository {

  public static function fetch(
    $idPerson,
    $begin_date = NULL,
    $end_date = NULL
  ) {
    $api = new LabhubEhmApiClient();
    $result = $api->selectTasks($idPerson, $begin_date, $end_date);
    if ($api->getResponseCode() == 200) {
      $data = json_decode($result)->data;
      return self::resultsToObjects($data);
    }
    else {
      return [];
    }
  }

  /**
   * @param \stdClass[] $results
   *
   * @return \Task[]
   */
  static function resultsToObjects($results) {
    $data = [];
    foreach ($results as $result) {
      $object = new Task($result, TRUE);
      $data[] = $object;
    }
    return $data;
  }
}