<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class Labware implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idLabware';

  private $idLabware = 0,
    $idParent = 1, // default value to comply with database constraint on creation
    $idLabwareType,
    $name,
    $a0,
    $a1,
    $row,
    $col,
    $pos,
    $date;

  /**
   * @param mixed $pos
   */
  public function setPos($pos) {
    $this->pos = $pos;
  }

  /**
   * @var bool TRUE if parent LabwareType is "plate"
   */
  private $is_plate = NULL;

  /**
   * @return mixed
   */
  public function getRow() {
    return $this->row;
  }

  /**
   * @param mixed $row
   */
  public function setRow($row) {
    $this->row = $row;
  }

  /**
   * @return mixed
   */
  public function getCol() {
    return $this->col;
  }

  /**
   * @param mixed $col
   */
  public function setCol($col) {
    $this->col = $col;
  }

  /**
   * @return int
   */
  public function getIdParent() {
    return $this->idParent;
  }

  /**
   * @param int $idParent
   */
  public function setIdParent($idParent) {
    $this->idParent = $idParent;
  }

  /**
   * @return mixed
   */
  public function getIdLabwareType() {
    return $this->idLabwareType;
  }

  /**
   * @param mixed $idLabwareType
   */
  public function setIdLabwareType($idLabwareType) {
    $this->idLabwareType = $idLabwareType;
  }

  /**
   * @return mixed
   */
  public function getPos() {
    return $this->pos;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @param mixed $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  public function save() {
    /**
     * Special method for creating new Labware of type "plate"
     */
    if ($this->isEmpty() && $this->isPlate()) {
      $this->setId(LabwareRepository::saveNewPlate($this));
    }
    else {
      // Standard save method, i.e. for existing instances or wells
      $this->setId(LabwareRepository::save($this));
    }
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'labware',
        'id' => $this->getId(),
        'attributes' => [
          'name' => $this->name,
          'a0' => $this->a0,
          'a1' => $this->a1,
          'row' => $this->row,
          'col' => $this->col,
          'pos' => $this->pos,
          'date' => $this->date,
        ],
        'relationships' => [
          'idParent' => [
            'data' => [
              'type' => 'labware',
              'id' => $this->idParent,
            ],
          ],
          'idLabwareType' => [
            'data' => [
              'type' => 'labwaretype',
              'id' => $this->idLabwareType,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }

  public function getForm() {
    $form = [];

    // Labware PlateType
    $types = LabwareTypeRepository::getAllByConditions(['type' => 'plate']);
    $options = [];
    foreach ($types as $type) {
      /**
       * @var \LabwareType $type
       */
      $options[$type->getId()] = $type->getName() . '(' . $type->getDescription() . ')';
    }
    $form['plate_type'] = [
      '#type' => 'select',
      '#title' => 'Select plate type',
      '#options' => $options,
      '#defaul_value' => $this->getIdLabwareType(),
      '#required' => TRUE,
    ];

    // Label
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => 'Label',
      '#default_value' => $this->getName(),
      '#required' => TRUE,
    ];

    // Manufacturing Date
    $form['fieldset_date'] = [
      '#type' => 'fieldset',
      '#title' => 'Manufacturing date',
    ];

    $form['fieldset_date']['date'] = [
      //'#title' => t('Date'),
      '#date_format' => 'Y-m-d',
      '#type' => 'date_popup',
      '#default_value' => !empty($date = $this->getDate()) ? $date : date("Y-m-d"),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Check if this instance is a "plate" by inheritance from parent LabwareType.
   *
   * @return bool
   */
  public function isPlate() {
    /**
     * If private field is not set, check parent LabwareType and
     * store the information in field for future reference
     */
    if (is_null($this->is_plate)) {
      $platetype = LabwareTypeRepository::getById($this->getIdLabwareType());
      /** @var \LabwareType $platetype */
      if ($platetype->getType() == LabwareType::TYPE_PLATE) {
        return $this->is_plate = TRUE;
      }
      else {
        return $this->is_plate = FALSE;
      }
    }
    else {
      return $this->is_plate;
    }
  }
}