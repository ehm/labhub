<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class Person implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idPerson';

  private $idPerson = 0,
    $name,
    $lastname,
    $email,
    $phone,
    $username;

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getLastname() {
    return $this->lastname;
  }

  /**
   * @param mixed $lastname
   */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  /**
   * @return mixed
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param mixed $email
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * @return mixed
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @param mixed $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @return mixed
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * @param mixed $username
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  public function save() {
    $this->setId(PersonRepository::save($this));
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'person',
        'id' => $this->getId(),
        'attributes' => [
          'name' => $this->name,
          'lastname' => $this->lastname,
          'email' => $this->email,
          'phone' => $this->phone,
          'username' => $this->username,
        ],
      ],
    ];
    return json_encode($array);
  }

  /**
   * @param bool $as_link Should the name be redered as an internal link to the
   *   resource display page?
   *
   * @return string
   */
  public function displayName($as_link = FALSE) {
    $name = $this->getName() . ' ' . $this->getLastname();
    if ($as_link) {
      return l($name, $this->url());
    }
    else {
      return $name;
    }
  }

  public function displayPageHeader() {
    $display = '<div><strong>' . $this->displayName(TRUE) . '</strong></div>';
    $display .= '<div><strong>E-Mail:</strong> ' . $this->getEmail() . '</div>';
    return $display;
  }

  public function getFormTasks() {

    $tasks = TaskRepository::fetch($this->getId());

    if (!count($tasks)) {
      return [];
    }

    $date_options = [];

    foreach ($tasks as $task) {
      $date = date('Y-m-d', strtotime($task->getDate()));
      if (!in_array($date, $date_options)) {
        $date_options[$date] = $date;
      }
    }

    $form = [];

    $form['fieldset_controls'] = [
      '#type' => 'fieldset',
      '#title' => t('Select user tasks'),
    ];

    $form['fieldset_controls']['begin'] = [
      '#type' => 'select',
      '#title' => t('Begin date'),
      '#options' => $date_options,
      '#ajax' => [
        'callback' => 'labhub_ehm_person_tasks_ajax_callback',
        'wrapper' => 'labhub_ehm_person_tasks_ajax_wrapper',
      ],
    ];

    $form['fieldset_controls']['end'] = [
      '#type' => 'select',
      '#title' => t('End date'),
      '#options' => $date_options,
      '#default_value' => $date,
      '#ajax' => [
        'callback' => 'labhub_ehm_person_tasks_ajax_callback',
        'wrapper' => 'labhub_ehm_person_tasks_ajax_wrapper',
      ],
    ];

    $form['fieldset_display'] = [
      '#type' => 'fieldset',
      '#title' => t('Tasks'),
    ];

    $form['fieldset_display']['display'] = [
      '#markup' => Task::table($tasks),
    ];

    $form['#prefix'] = '<div id="labhub_ehm_person_tasks_ajax_wrapper">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  public function url() {
    return self::url_from_id($this->getId());
  }

  public static function url_from_id($id) {
    return base_path() . str_replace('%', $id, LABHUB_EHM_URL_PERSON);
  }
}