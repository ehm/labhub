<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class ExperimentRepository implements EhmObjectRepositoryInterface {

  use EhmObjectRepositoryTrait;
  use EhmObjectCacheTrait;

  private static $table_name = 'experiment';

  private static $class_name = 'Experiment';

  public static function getById($id) {

    if ($object = self::getCached($id)) {
      return $object;
    }
    else {
      $client_api = new LabhubEhmApiClient();
      $result = $client_api->selectExperiment($id);

      /**
       * Translate experiment object.
       */
      $data = json_decode($result)->data;
      $experiment = new Experiment($data, TRUE);

      /**
       * Translate included objects.
       */
      $result = json_decode($result, TRUE);
      if (array_key_exists('included', $result)) {
        $wells = [];
        $events = [];
        $celltypes = [];
        foreach ($result['included'] as $included) {
          if ($included['type'] == 'labware') {
            $labware_json = json_decode(json_encode($included));
            $experiment->setLabware(new Labware($labware_json, TRUE));
          }
          if ($included['type'] == 'labwaretype') {
            $labwaretype_json = json_decode(json_encode($included));
            $experiment->setLabwaretype(new LabwareType($labwaretype_json,
              TRUE));
          }
          if ($included['type'] == 'well') {
            $labware_json = json_decode(json_encode($included));
            $wells[] = new Labware($labware_json, TRUE);
          }
          if ($included['type'] == 'event') {
            $event_json = json_decode(json_encode($included));
            $events[] = new Event($event_json, TRUE);
          }
          if ($included['type'] == 'celltype') {
            $celltypes[] = json_decode(json_encode($included));
          }
        }
        $experiment->setWells($wells);
        $experiment->setEvents($events);
        $experiment->setCelltypes($celltypes);
      }
      else {
        $experiment->setLabware(new Labware());
        $experiment->setLabwaretype(new LabwareType());
      }

      return $experiment;
    }
  }
}