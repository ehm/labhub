<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 08.06.18
 * Time: 17:36
 */
class Task implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idEvent';

  private $idEvent = 0,
    $type,
    $name,
    $idPerson = 0,
    $date;

  /**
   * @param Task[] $tasks
   *
   * @return string Rendered table or empty string on exception
   */
  public static function table($tasks) {
    $rows = [];
    foreach ($tasks as $task) {
      $rows[] = $task->tableRow();
    }
    try {
      return theme('table', ['header' => self::tableHeader(), 'rows' => $rows]);
    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
      return '';
    }
  }

  public function tableRow() {
    if (class_exists($classname = $this->type)) {
      $link = l($id = $this->getId(), $classname::url_from_id($id));
    }
    else {
      $link = $this->getId();
    }
    return [
      $link,
      $this->type,
      $this->date,
      $this->name,
    ];
  }

  public static function tableHeader() {
    return [
      'idEvent',
      'type',
      'date',
      'name',
    ];
  }

  /**
   * @return void
   */
  public function save() {
    // must not be implemented
  }

  /**
   * @return string a JSON encoded string on success or <b>FALSE</b> on failure
   */
  public function jsonObject() {
    return json_encode([
      'type' => 'task',
      'id' => $this->getId(),
      'attributes' => [
        'type' => $this->type,
        'name' => $this->name,
        'date' => $this->date,
      ],
      'relationships' => [
        'idPerson' => [
          'data' => [
            'type' => 'person',
            'id' => $this->idPerson,
          ],
        ],
      ],
    ]);
  }

  public function getDate() {
    return $this->date;
  }
}