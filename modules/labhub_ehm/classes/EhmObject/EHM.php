<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class EHM implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idEvent';

  /**
   * @var int $idEvent
   * @var int $idPerson
   * @var string $comment
   * @var int $idQualityControl
   * @var bool $active
   */
  private $idEvent = 0,
    $idPerson = 0,
    $comment,
    $idQualityControl = 0,
    $active = 0;

  private $idExperiment = 0;

  private $idLabware = 0;

  /**
   * @param mixed $id An EHM sample id.
   *
   * @return string URI path for LabHub display page of the EHM sample.
   */
  public static function url_from_id($id) {
    $ehm = EHMRepository::getById($id);
    /** @var self $ehm */
    return $ehm->url();
  }

  /**
   * Generate LabHub display page for EHM sample using
   *
   * @see \Experiment::url_sample()
   *
   * @return string URI path for LabHub display page of this EHM sample.
   */
  public function url() {
    return Experiment::url_sample($this->getIdExperiment(),
      $this->getIdLabware());
  }

  /**
   * @return mixed
   */
  public function getIdExperiment() {
    if ($this->idExperiment == 0) {
      $event = EventRepository::getById($this->getId());
      /** @var \Event $event */
      $experiment = EventRepository::getById($event->getIdParent());
      /** @var \Event $experiment */
      if ($experiment->getIdEventType() == variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_EXPERIMENT)) {
        $this->setIdExperiment($experiment->getId());
      }
    }
    return $this->idExperiment;
  }

  /**
   * @param mixed $idExperiment
   */
  public function setIdExperiment($idExperiment) {
    $this->idExperiment = $idExperiment;
  }

  /**
   * Get ID of labware (well) holding EHM.
   *
   * @return mixed
   */
  public function getIdLabware() {
    if ($this->idLabware == 0) {
      $event = EventRepository::getById($this->getId());
      $this->idLabware = $event->getIdLabware();
    }
    return $this->idLabware;
  }

  /**
   * @return int
   */
  public function getIdPerson() {
    return $this->idPerson;
  }

  /**
   * @param int $idPerson
   */
  public function setIdPerson($idPerson) {
    $this->idPerson = $idPerson;
  }

  /**
   * @return string
   */
  public function getComment() {
    return $this->comment;
  }

  /**
   * @param string $comment
   */
  public function setComment($comment) {
    $this->comment = $comment;
  }

  /**
   * @return int
   */
  public function getIdQualityControl() {
    return $this->idQualityControl;
  }

  /**
   * @param int $idQualityControl
   */
  public function setIdQualityControl($idQualityControl) {
    $this->idQualityControl = $idQualityControl;
  }

  /**
   * @return bool
   */
  public function getActive() {
    return $this->active;
  }

  /**
   * @param bool $active
   */
  public function setActive($active) {
    $this->active = $active;
  }

  /**
   * @return void
   */
  public function save() {
    $this->setId(EHMRepository::save($this));
  }

  /**
   * @return string a JSON encoded string on success or <b>FALSE</b> on failure
   */
  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'ehm',
        'id' => $this->getId(),
        'attributes' => [
          'comment' => $this->comment,
          'idQualityControl' => $this->idQualityControl,
          'active' => $this->active,
        ],
        'relationships' => [
          'idPerson' => [
            'data' => [
              'type' => 'person',
              'id' => $this->idPerson,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }

  /**
   * Get date of EHM casting.
   *
   * @return mixed
   */
  public function getCastingDate() {
    /**
     * Get casting event IDs.
     */
    $casting_event_ids = array_values(array_filter(variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_CASTING)));

    /**
     * Conditions to identify casting event
     */
    $conditions = [
      'idEventType' => implode(",", $casting_event_ids),
      'idParent' => $this->getIdExperiment(),
      'idLabware' => $this->getIdLabware(),
    ];

    $casting_event = EventRepository::getAllByConditions($conditions)[0];
    return $casting_event->getDate();
  }
}