<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class Solution implements EhmObjectInterface {

  use EhmObjectTrait;

  public static $types = [
    'medium',
    'mastermix',
    'supplement',
    'cells',
    'reagent',
    'solvent',
    'growth factor',
    'antibiotic',
    'compound',
  ];

  private static $id_fieldname = 'idSolution';

  private static $units = [
    'nM' => 'Nanomol nM',
    'µM' => 'Micromol µM',
    'mM' => 'Millimol mM',
    'ng/ml' => 'ng per ml',
    'mg/ml' => 'mg per ml',
    '%' => 'percent',
    'x' => 'x-fold',
    'cells/ml' => 'cells per ml',
    'N' => 'N',
  ];

  private $idSolution = 0,
    $name,
    $description,
    $concentration,
    $unit,
    $type,
    $vendor,
    $ordernumber;

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return mixed
   */
  public function getConcentration() {
    return $this->concentration;
  }

  /**
   * @param mixed $concentration
   */
  public function setConcentration($concentration) {
    $this->concentration = $concentration;
  }

  /**
   * @return mixed
   */
  public function getUnit() {
    return $this->unit;
  }

  /**
   * @param mixed $unit
   */
  public function setUnit($unit) {
    $this->unit = $unit;
  }

  /**
   * @return mixed
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param mixed $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return mixed
   */
  public function getVendor() {
    return $this->vendor;
  }

  /**
   * @param mixed $vendor
   */
  public function setVendor($vendor) {
    $this->vendor = $vendor;
  }

  /**
   * @return mixed
   */
  public function getOrdernumber() {
    return $this->ordernumber;
  }

  /**
   * @param mixed $ordernumber
   */
  public function setOrdernumber($ordernumber) {
    $this->ordernumber = $ordernumber;
  }

  public function save() {
    $this->setId(SolutionRepository::save($this));
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'solution',
        'id' => $this->getId(),
        'attributes' => [
          'name' => $this->name,
          'description' => $this->description,
          'concentration' => $this->concentration,
          'unit' => $this->unit,
          'type' => $this->type,
          'vendor' => $this->vendor,
          'ordernumber' => $this->ordernumber,
        ],
      ],
    ];
    return json_encode($array);
  }
}