<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class Peak implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idPeak';

  private $idPeak = 0,
    $idPeakAnalysis = 0,
    $idEvent = 0,
    $frame,
    $start,
    $stop,
    $time,
    $force,
    $base,
    $height,
    $minL,
    $minR,
    $idPrevious,
    $RR,
    $width,
    $vContract,
    $vRelax,
    $t1,
    $t2,
    $t3,
    $t4,
    $height_fit;

  /**
   * @return int
   */
  public function getIdEvent() {
    return $this->idEvent;
  }

  public static function tableHeader() {
    return [
      "frame",
      "start",
      "stop",
      "time",
      "force",
      "base",
      "height",
      "minL",
      "minR",
      "idPrevious",
      "RR",
      "width",
      "vContract",
      "vRelax",
      "t1",
      "t2",
      "t3",
      "t4",
      "height_fit",
    ];
  }

  public function tableRow() {
    return [
      $this->frame,
      $this->start,
      $this->stop,
      $this->time,
      $this->force,
      $this->base,
      $this->height,
      $this->minL,
      $this->minR,
      $this->idPrevious,
      $this->RR,
      $this->width,
      $this->vContract,
      $this->vRelax,
      $this->t1,
      $this->t2,
      $this->t3,
      $this->t4,
      $this->height_fit,
    ];
  }

  /**
   * @return void
   */
  public function save() {
    $this->setId(PeakRepository::save($this));
  }

  /**
   * @return string a JSON encoded string on success or <b>FALSE</b> on failure
   */
  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'peak',
        'id' => $this->getId(),
        'attributes' => [
          'frame' => $this->frame,
          'start' => $this->start,
          'stop' => $this->stop,
          'time' => $this->time,
          'force' => $this->force,
          'base' => $this->base,
          'height' => $this->height,
          'minL' => $this->minL,
          'minR' => $this->minR,
          'idPrevious' => $this->idPrevious,
          'RR' => $this->RR,
          'width' => $this->width,
          'vContract' => $this->vContract,
          'vRelax' => $this->vRelax,
          't1' => $this->t1,
          't2' => $this->t2,
          't3' => $this->t3,
          't4' => $this->t4,
          'height_fit' => $this->height_fit,
        ],
        'relationships' => [
          'idPeakAnalysis' => [
            'data' => [
              'type' => 'peakanalysis',
              'id' => $this->idPeakAnalysis,
            ],
          ],
          'idEvent' => [
            'data' => [
              'type' => 'event',
              'id' => $this->idEvent,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }
}