<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class MeasurementRepository implements EhmObjectRepositoryInterface {

  use EhmObjectRepositoryTrait;
  use EhmObjectCacheTrait;

  private static $table_name = 'measurement';

  private static $class_name = 'Measurement';

  public static function getById($id) {

    if ($object = self::getCached($id)) {
      return $object;
    }
    else {
      $client_api = new LabhubEhmApiClient();
      $result = $client_api->selectMeasurement($id);
      $data = json_decode($result)->data;

      $measurement = new Measurement($data, TRUE);

      /**
       * Translate included objects.
       */
      $result = json_decode($result, TRUE);
      if (array_key_exists('included', $result)) {
        $peaks = [];
        foreach ($result['included'] as $included) {
          if ($included['type'] == 'event') {
            $obj = json_decode(json_encode($included));
            $event = new Event($obj, TRUE);
            $measurement->setEvent($event);
          }
          elseif ($included['type'] == 'person') {
            $obj = json_decode(json_encode($included));
            $person = new Person($obj, TRUE);
            $measurement->setPerson($person);
          }
          elseif ($included['type'] == 'peak') {
            $obj = json_decode(json_encode($included));
            $peak = new Peak($obj, TRUE);
            $peaks[] = $peak;
          }
        }
        $measurement->setPeaks($peaks);
      }

      return $measurement;
    }

  }
}