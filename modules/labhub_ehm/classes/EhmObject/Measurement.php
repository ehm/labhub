<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class Measurement implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idEvent';

  private $idEvent = 0,
    $name,
    $idPerson = 0,
    $comment,
    $resolutionTime,
    $resolutionSpace,
    $datapoints,
    $filename,
    $filepath,
    $Stimulation_frequency,
    $Stimulation_voltage;

  private $statistics;

  /**
   * @return \Event
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * @param \Event $event
   */
  public function setEvent($event) {
    $this->event = $event;
  }

  /**
   * @var array $wells
   */
  private $wells = [];

  /**
   * @var \Event $event
   */
  private $event;

  /**
   * @var \Person $person
   */
  private $person;

  /**
   * @return \Person
   */
  public function getPerson() {
    return $this->person;
  }

  /**
   * @param \Person $person
   */
  public function setPerson($person) {
    $this->person = $person;
  }

  private $wellsMeasured;

  /**
   * @var \Peak[]
   */
  private $peaks = [];

  public static function tableHeader() {
    return [
      'ID',
      'Date',
      'Name',
      'Person',
      'Wells measured',
    ];
  }

  public function tableRow() {
    return [
      l($this->getId(), $this->url()),
      $this->event->getDate(),
      $this->getName(),
      $this->person->displayName(TRUE),
      $this->getWellsMeasured(),
    ];
  }

  public function displayPage() {

    _labhub_ehm_include_css();

    $display = '';

    $experiment_link = l("Back to Experiment",
      Experiment::url_from_id($this->event->getIdParent()),
      ['attributes' => ['class' => 'btn btn-default btn-labhub-back-exp']]);

    $display .= $experiment_link;

    try {
      $display .= theme('table',
        ['header' => self::tableHeader(), 'rows' => [$this->tableRow()]]);
    } catch (Exception $e) {
      watchdog_exception(__CLASS__, $e);
    }

    $wells_table_vars = $this->getWellsTableVariables();
    try {
      //$display_wells = theme('table', $wells_table_vars);
      $display_wells = drupal_get_form('labhub_ehm_measurement_export_form',
        $this->getId(), $wells_table_vars);
      $display .= "<h2>Results:</h2>";
      $display .= drupal_render($display_wells);
    } catch (Exception $e) {
      watchdog_exception(__CLASS__, $e);
    }

    return $display;
  }

  public function url() {
    return self::url_from_id($this->getId());
  }

  public static function url_from_id($id) {
    return base_path() . str_replace('%', $id,
        LABHUB_EHM_URL_MEASUREMENT_VIEW);
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return int
   */
  public function getIdPerson() {
    return $this->idPerson;
  }

  /**
   * @param int $idPerson
   */
  public function setIdPerson($idPerson) {
    $this->idPerson = $idPerson;
  }

  /**
   * @return mixed
   */
  public function getComment() {
    return $this->comment;
  }

  /**
   * @param mixed $comment
   */
  public function setComment($comment) {
    $this->comment = $comment;
  }

  /**
   * @return mixed
   */
  public function getResolutionTime() {
    return $this->resolutionTime;
  }

  /**
   * @param mixed $resolutionTime
   */
  public function setResolutionTime($resolutionTime) {
    $this->resolutionTime = $resolutionTime;
  }

  /**
   * @return mixed
   */
  public function getResolutionSpace() {
    return $this->resolutionSpace;
  }

  /**
   * @param mixed $resolutionSpace
   */
  public function setResolutionSpace($resolutionSpace) {
    $this->resolutionSpace = $resolutionSpace;
  }

  /**
   * @return mixed
   */
  public function getDatapoints() {
    return $this->datapoints;
  }

  /**
   * @param mixed $datapoints
   */
  public function setDatapoints($datapoints) {
    $this->datapoints = $datapoints;
  }

  /**
   * @return mixed
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * @param mixed $filename
   */
  public function setFilename($filename) {
    $this->filename = $filename;
  }

  /**
   * @return mixed
   */
  public function getFilepath() {
    return $this->filepath;
  }

  /**
   * @param mixed $filepath
   */
  public function setFilepath($filepath) {
    $this->filepath = $filepath;
  }

  /**
   * @return mixed
   */
  public function getStimulationFrequency() {
    return $this->Stimulation_frequency;
  }

  /**
   * @param mixed $Stimulation_frequency
   */
  public function setStimulationFrequency($Stimulation_frequency) {
    $this->Stimulation_frequency = $Stimulation_frequency;
  }

  /**
   * @return mixed
   */
  public function getStimulationVoltage() {
    return $this->Stimulation_voltage;
  }

  /**
   * @param mixed $Stimulation_voltage
   */
  public function setStimulationVoltage($Stimulation_voltage) {
    $this->Stimulation_voltage = $Stimulation_voltage;
  }

  /**
   * @return array
   */
  public function getWells() {
    return $this->wells;
  }

  /**
   * @param array $wells
   */
  public function setWells($wells) {
    $this->wells = $wells;
  }

  /**
   * @return mixed
   */
  public function getWellsMeasured() {
    return $this->wellsMeasured;
  }

  /**
   * @param mixed $wellsMeasured
   */
  public function setWellsMeasured($wellsMeasured) {
    $this->wellsMeasured = $wellsMeasured;
  }

  /**
   * @return \Peak[]
   */
  public function getPeaks() {
    return $this->peaks;
  }

  /**
   * @param \Peak[] $peaks
   */
  public function setPeaks($peaks) {
    $this->peaks = $peaks;
  }

  /**
   * @return void
   */
  public function save() {
    $this->setId(MeasurementRepository::save($this));
  }

  /**
   * @return string a JSON encoded string on success or <b>FALSE</b> on failure
   */
  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'measurement',
        'id' => $this->getId(),
        'attributes' => [
          'name' => $this->name,
          'comment' => $this->comment,
          'resolutionTime' => $this->resolutionTime,
          'resolutionSpace' => $this->resolutionSpace,
          'datapoints' => $this->datapoints,
          'filename' => $this->filename,
          'filepath' => $this->filepath,
          'Stimulation_frequency' => $this->Stimulation_frequency,
          'Stimulation_voltage' => $this->Stimulation_voltage,
        ],
        'relationships' => [
          'idPerson' => [
            'data' => [
              'type' => 'person',
              'id' => $this->idPerson,
            ],
          ],
        ],
      ],
    ];
    return json_encode($array);
  }

  /**
   * @return mixed
   */
  public function getStatistics() {
    return $this->statistics;
  }

  /**
   * @param mixed $statistics
   */
  public function setStatistics($statistics) {
    $this->statistics = $statistics;
  }

  /**
   * @return array A Drupal table-renderable associative array containing
   *   "header" and "rows" keys
   */
  private function getWellsTableVariables() {
    if (isset($this->statistics)) {
      $header = ['Well', 'AVG FoC in %', 'STDDEV FoC'];
      $rows = [];
      foreach ($this->statistics as $idLabware => $stats) {
        $labware = LabwareRepository::getById($idLabware);
        $rows[] = [
          l($labware->getName(),
            Experiment::url_sample($this->event->getIdParent(), $idLabware)),
          number_format((float) $stats['foc_mean'], 4, '.', ''),
          number_format((float) $stats['foc_stddev'], 4, '.', ''),
        ];
      }
      return ['header' => $header, 'rows' => $rows];
    }
  }

  /**
   * Return an array of all wells with measured values in SEEK exportable
   * format.
   *
   * Table columns for export:
   * 'Title',
   * 'Measurement LabHub ID',
   * 'Measurement Date',
   * 'EHM LabHub ID',
   * 'EHM Casting Date',
   * 'EHM LabHub URL',
   * 'Mastermix LabHub ID',
   * 'Experiment LabHub ID',
   *
   *
   */
  public function getWellsExportFormat() {
    $rows = [];
    if (isset($this->statistics)) {

      /**
       * Collect export values independent of EHM sample ID.
       */
      $exp_id = $this->getEvent()->getIdParent();
      $experiment = ExperimentRepository::getById($exp_id);
      $solution = $experiment->getSolution();
      $mastermix_id = $solution->getId();
      $measurement_id = $this->getId();
      $measurement_date = $this->getEvent()->getDate();

      $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
      $url_base_path = $protocol . $_SERVER['HTTP_HOST'];

      foreach ($this->statistics as $idLabware => $stats) {

        /**
         * Collect sample values unique for each EHM sample.
         */
        $labware = LabwareRepository::getById($idLabware);
        $ehm_event = EventRepository::getAllByConditions([
          'idParent' => $exp_id,
          'idLabware' => $idLabware,
        ])[0];
        $ehm_id = $ehm_event->getId();
        $ehm = EHMRepository::getById($ehm_id);
        $ehm_casting_date = $ehm->getCastingDate();
        $ehm_labhub_url = $url_base_path . $ehm->url();

        $title = $experiment->getName()
          . '_' . $labware->getName()
          . '_' . $this->getName();

        /**
         * Assemble export values as array.
         */
        $rows[] = [
          $title,
          $measurement_id,
          $measurement_date,
          $ehm_id,
          $ehm_casting_date,
          $ehm_labhub_url,
          $mastermix_id,
          $exp_id,
        ];
      }
    }
    return $rows;
  }
}