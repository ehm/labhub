<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class PlateType implements EhmObjectInterface {

  use EhmObjectTrait;

  private static $id_fieldname = 'idLabwareType';

  private $idLabwareType = 0,
    $version,
    $description,
    $wells,
    $material,
    $stretcherDiameter,
    $bEqualDiameter = FALSE,
    $wellWidth,
    $wellHeight,
    $plate_rows,
    $plate_cols,
    $stretcherDistance,
    $force_mm;

  /**
   * @param mixed $version
   */
  public function setVersion($version) {
    $this->version = $version;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @param mixed $wells
   */
  public function setWells($wells) {
    $this->wells = $wells;
  }

  /**
   * @param mixed $material
   */
  public function setMaterial($material) {
    $this->material = $material;
  }

  /**
   * @param mixed $stretcherDiameter
   */
  public function setStretcherDiameter($stretcherDiameter) {
    $this->stretcherDiameter = $stretcherDiameter;
  }

  /**
   * @param mixed $bEqualDiameter
   */
  public function setBEqualDiameter($bEqualDiameter) {
    $this->bEqualDiameter = $bEqualDiameter;
  }

  /**
   * @param mixed $wellWidth
   */
  public function setWellWidth($wellWidth) {
    $this->wellWidth = $wellWidth;
  }

  /**
   * @param mixed $wellHeight
   */
  public function setWellHeight($wellHeight) {
    $this->wellHeight = $wellHeight;
  }

  /**
   * @param mixed $plate_rows
   */
  public function setPlateRows($plate_rows) {
    $this->plate_rows = $plate_rows;
  }

  /**
   * @param mixed $plate_cols
   */
  public function setPlateCols($plate_cols) {
    $this->plate_cols = $plate_cols;
  }

  /**
   * @param mixed $stretcherDistance
   */
  public function setStretcherDistance($stretcherDistance) {
    $this->stretcherDistance = $stretcherDistance;
  }

  /**
   * @param mixed $force_mm
   */
  public function setForceMm($force_mm) {
    $this->force_mm = $force_mm;
  }

  public function getForm() {
    $form = [];

    $form['fieldset_platetype'] = [
      '#type' => 'fieldset',
      '#title' => 'Microwell Plate details',
    ];

    $form['fieldset_platetype']['material'] = [
      '#type' => 'textfield',
      '#title' => t('Material'),
      '#default_value' => $this->getMaterial(),
      '#required' => TRUE,
    ];

    $form['fieldset_platetype']['plate_cols'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_integer_positive'],
      '#size' => 2,
      '#maxlength' => 2,
      '#title' => t('Number of Columns'),
      '#default_value' => $this->getPlateCols(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => 'labhub_ehm_labware_calculate_wells_ajax_callback',
        'wrapper' => 'labwaretype-calculate-wells-wrapper',
      ],
    ];

    $form['fieldset_platetype']['plate_rows'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_integer_positive'],
      '#size' => 2,
      '#maxlength' => 2,
      '#title' => t('Number of Rows'),
      '#default_value' => $this->getPlateRows(),
      '#ajax' => [
        'callback' => 'labhub_ehm_labware_calculate_wells_ajax_callback',
        'wrapper' => 'labwaretype-calculate-wells-wrapper',
      ],
      '#required' => TRUE,
    ];

    $form['fieldset_platetype']['wells'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_integer_positive'],
      '#size' => 3,
      '#maxlength' => 3,
      '#title' => t('Number of wells'),
      '#attributes' => ['readonly' => 'readonly'],
      '#default_value' => $this->getWells(),
      '#prefix' => '<div id="labwaretype-calculate-wells-wrapper">',
      '#suffix' => '</div>',
      '#required' => TRUE,
    ];

    $form['fieldset_platetype']['wellWidth'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_number'],
      '#size' => 5,
      '#maxlength' => 5,
      '#title' => t('Well Width'),
      '#default_value' => $this->getWellWidth(),
    ];

    $form['fieldset_platetype']['wellHeight'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_number'],
      '#size' => 5,
      '#maxlength' => 5,
      '#title' => t('Well Height'),
      '#default_value' => $this->getWellHeight(),
    ];

    $form['fieldset_platetype']['stretcherDiameter'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_number'],
      '#size' => 4,
      '#maxlength' => 4,
      '#title' => t('Stretcher Diameter'),
      '#default_value' => $this->getStretcherDiameter(),
    ];

    $form['fieldset_platetype']['stretcherDistance'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_number'],
      '#size' => 6,
      '#maxlength' => 6,
      '#title' => t('Stretcher Distance'),
      '#default_value' => $this->getStretcherDistance(),
    ];

    $form['fieldset_platetype']['bEqualDiameter'] = [
      '#type' => 'checkbox',
      '#title' => t('Equals Diameter?'),
      '#default_value' => $this->getBEqualDiameter(),
    ];

    $form['fieldset_platetype']['force_mm'] = [
      '#type' => 'textfield',
      '#element_validate' => ['element_validate_number'],
      '#size' => 6,
      '#maxlength' => 6,
      '#title' => t('Force mm'),
      '#default_value' => $this->getForceMm(),
    ];


    return $form;
  }

  /**
   * @return mixed
   */
  public function getMaterial() {
    return $this->material;
  }

  /**
   * @return mixed
   */
  public function getPlateCols() {
    return $this->plate_cols;
  }

  /**
   * @return mixed
   */
  public function getPlateRows() {
    return $this->plate_rows;
  }

  /**
   * @return mixed
   */
  public function getWells() {
    return $this->wells;
  }

  /**
   * @return mixed
   */
  public function getWellWidth() {
    return $this->wellWidth;
  }

  /**
   * @return mixed
   */
  public function getWellHeight() {
    return $this->wellHeight;
  }

  /**
   * @return mixed
   */
  public function getStretcherDiameter() {
    return $this->stretcherDiameter;
  }

  /**
   * @return mixed
   */
  public function getStretcherDistance() {
    return $this->stretcherDistance;
  }

  /**
   * @return mixed
   */
  public function getBEqualDiameter() {
    return $this->bEqualDiameter;
  }

  /**
   * @return mixed
   */
  public function getForceMm() {
    return $this->force_mm;
  }

  public function save() {
    $this->setId(PlateTypeRepository::save($this));
  }

  public function jsonObject() {
    $array = [
      'data' => [
        'type' => 'platetype',
        'id' => $this->getId(),
        'attributes' => [
          'version' => $this->getVersion(),
          'description' => $this->getDescription(),
          'wells' => $this->getWells(),
          'material' => $this->getMaterial(),
          'stretcherDiameter' => $this->getStretcherDiameter(),
          'bEqualDiameter' => $this->getBEqualDiameter(),
          'wellWidth' => $this->getWellWidth(),
          'wellHeight' => $this->getWellHeight(),
          'plate_rows' => $this->getPlateRows(),
          'plate_cols' => $this->getPlateCols(),
          'stretcherDistance' => $this->getStretcherDistance(),
          'force_mm' => $this->getForceMm(),
        ],
      ],
    ];
    return json_encode($array);
  }

  /**
   * @return mixed
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * @return mixed
   */
  public function getDescription() {
    return $this->description;
  }
}