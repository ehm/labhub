<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubEhmApiClient {

  use LabhubApiClientTrait;

  const ROUTE_GENERIC = 'contractiondb/api/v1/generic/';

  const ROUTE_EXPERIMENT = 'contractiondb/api/v1/experiment/';

  const ROUTE_MEASUREMENT = 'contractiondb/api/v1/measurement/';

  const ROUTE_TASKS = 'contractiondb/api/v1/tasks';

  public function __construct(\LabhubServiceInterface $service = NULL) {
    $service_id = variable_get("labhub_ehm_config_host");
    /**
     * @var \LabhubEhmService $service
     */
    $this->service = LabhubServiceRepository::getById($service_id);

    /**
     * @var \LabhubCredentials $credentials
     */
    $this->setCredentials(labhub_ehm_get_credentials());
  }

  public function test() {

    $this->setRoute(self::ROUTE_GENERIC);
    $this->init();

    curl_setopt($this->handle, CURLOPT_VERBOSE, TRUE);
    curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, TRUE);

    $test = json_decode($this->execute(), TRUE);
    if (is_array($test) && !array_key_exists('error', $test)
      && $test['meta']['service'] == 'ContractionDB API') {
      return TRUE;
    }
    else {
      watchdog('LabHub EHM API Client', json_encode($test['message']), [],
        WATCHDOG_ERROR);
      return FALSE;
    }

  }

  public function selectGeneric($table_name, $parameters = []) {

    $route = self::ROUTE_GENERIC . $table_name;
    if (count($parameters)) {
      $route .= '?';
      $params = [];
      foreach ($parameters as $key => $value) {
        $params[] = "$key=$value";
      }
      $route .= implode('&', $params);
    }
    $this->setRoute($route);

    $this->init();
    return $this->execute();
  }

  public function postGeneric($table_name, $object) {
    $route = 'contractiondb/api/v1/generic/' . $table_name;
    $this->setRoute($route);
    $this->init();

    curl_setopt($this->handle, CURLOPT_POSTFIELDS, $object);
    $this->setMethod(LabhubHttpResources::METHOD_HTTP_POST);

    return $this->execute();
  }

  /**
   * @param $table_name
   * @param $id
   *
   * @return bool TRUE if successfully deleted
   */
  public function delete($table_name, $id) {
    $route = 'contractiondb/api/v1/generic/' . $table_name . '/' . $id;
    $this->setRoute($route);
    $this->init();

    $this->setMethod(LabhubHttpResources::METHOD_HTTP_DELETE);
    $this->execute();

    if (in_array($this->response_code, [200, 202, 204])) {
      return TRUE;
    }
    else {
      return FALSE;
    }

  }

  public function selectExperiment($id) {

    $route = self::ROUTE_EXPERIMENT . $id;
    $this->setRoute($route);

    $this->init();
    return $this->execute();
  }

  public function selectMeasurement($id) {

    $route = self::ROUTE_MEASUREMENT . $id;
    $this->setRoute($route);

    $this->init();
    return $this->execute();

  }

  public function selectTasks($person_id, $begin = NULL, $end = NULL) {
    $route = self::ROUTE_TASKS . '?person=' . $person_id;
    if (!empty($begin)) {
      $route .= '&begin=' . $begin;
    }
    if (!empty($end)) {
      $route .= '&end=' . $end;
    }

    $this->setRoute($route);

    $this->init();
    return $this->execute();
  }

}