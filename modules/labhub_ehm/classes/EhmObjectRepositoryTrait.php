<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

trait EhmObjectRepositoryTrait {

  public static function save(EhmObjectInterface $object) {

    $jsonObj = $object->jsonObject();
    $client_api = new LabhubEhmApiClient();
    $result = $client_api->postGeneric(self::$table_name, $jsonObj);

    // decode nested JSON array
    $result = json_decode($result);

    /**
     * Only proceed with processing, if query actually contained valid data
     */
    $data = is_object($result) ? json_decode($result->data) : NULL;
    if ($data) {
      $object->setId($data->id);
      self::cache($object);
      // return ID
      return $data->id;
    }
    else {
      return 0;
    }
  }

  /**
   * Delete object identified by input parameter
   *
   * @param $id
   */
  public static function delete($id) {
    $client_api = new LabhubEhmApiClient();
    $result = $client_api->delete(self::$table_name, $id);
    /**
     * Result is TRUE if deletion successful
     */
    if ($result) {
      self::deleteCached($id);
    }
    else {
      watchdog("LabHub EHM",
        "Delete failed for object-id %id, repository class %class", [
          '%id' => $id,
          '%class' => __CLASS__,
        ],
        WATCHDOG_ERROR);
    }
  }

  /**
   * @param int $id
   *
   * @return \EhmObjectInterface
   */
  public static function getById($id) {
    if ($object = self::getCached($id)) {
      return $object;
    }
    else {
      $class_name = self::classname();

      $client_api = new LabhubEhmApiClient();
      $result = $client_api->selectGeneric(self::$table_name,
        [$class_name::getIdFieldname() => $id]);
      $result = json_decode($result);

      // "data" attribute contains an array of one JSON object
      return new $class_name($result->data[0]);
    }
  }

  public static function classname() {
    return self::$class_name;
  }

  /**
   * @return \EhmObjectInterface[]
   */
  public static function getAll() {

    if (count($cached_objects = self::getAllCached())) {
      return $cached_objects;
    }
    else {
      $client_api = new LabhubEhmApiClient();
      $result = $client_api->selectGeneric(self::$table_name, ['limit' => 0]);

      // decoded nested JSON array
      $result = json_decode($result);

      return self::resultsToObjects($result->data);
    }
  }

  /**
   * @param array $conditions
   *
   * @return \EhmObjectInterface[]
   */
  public static function getAllByConditions($conditions) {

    // ToDo: optionally implement extraction from cache

    // Add limit to conditions
    $conditions['limit'] = 0;

    /**
     * Query the API
     */
    $client_api = new LabhubEhmApiClient();
    $result = $client_api->selectGeneric(self::$table_name, $conditions);

    // decoded nested JSON array
    $result = json_decode($result);

    return self::resultsToObjects($result->data);
  }

  /**
   * @param \stdClass[]
   *
   * @return \EhmObjectInterface[]
   */
  public static function resultsToObjects(
    $results
  ) {
    $data = [];
    $class_name = self::classname();
    foreach ($results as $result) {
      /**
       * @var \EhmObjectInterface $object
       */
      $object = new $class_name($result);
      $data[] = $object;
      self::cache($object);
    }
    return $data;
  }

}