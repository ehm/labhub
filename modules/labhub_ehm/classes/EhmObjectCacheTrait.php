<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

trait EhmObjectCacheTrait {

  /**
   * @param \EhmObjectInterface $object
   *
   * @return void
   */
  public static function cache(EhmObjectInterface $object) {
    $cache_id = self::getCacheId();
    $key = $object->getId();

    if ($cache = cache_get($cache_id)) {
      $data = $cache->data;
    }
    else {
      $data = [];
    }

    $data[$key] = $object;
    cache_set($cache_id, $data);
  }

  public static function getCacheId() {
    return 'ehm_obj_' . self::$table_name;
  }

  /**
   * @param $object_id
   */
  public static function deleteCached($object_id) {
    if ($cache = cache_get(self::getCacheId())) {
      if (array_key_exists($object_id, $cache->data)) {
        unset($cache->data[$object_id]);
      }
    }
  }

  /**
   * @param int $object_id
   *
   * @return \EhmObjectInterface
   */
  public static function getCached($object_id) {
    if ($cache = cache_get(self::getCacheId())) {
      if (array_key_exists($object_id, $cache->data)) {
        return $cache->data[$object_id];
      }
      else {
        return NULL;
      }
    }
    else {
      return NULL;
    }
  }

  /**
   * @return \EhmObjectInterface[]
   */
  public static function getAllCached() {
    if ($cache = cache_get(self::getCacheId())) {
      $list = [];
      foreach ($cache->data as $object) {
        /**
         * @var \EhmObjectInterface $object
         */
        $list[$object->getId()] = $object;
      }
      return $list;
    }
    else {
      return [];
    }
  }

  /**
   * @return void
   */
  public static function flush() {
    if ($cache = cache_get(self::getCacheId())) {
      unset($cache->data);
    }
  }

}