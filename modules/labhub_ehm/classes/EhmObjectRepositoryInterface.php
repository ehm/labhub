<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

interface EhmObjectRepositoryInterface extends EhmObjectCacheInterface {

  /**
   * @return string The Class name of the EhmObjectInterface implementation
   * stored via this repository class
   */
  public static function classname();

  /**
   * @param \EhmObjectInterface $object
   *
   * @return int
   */
  public static function save(EhmObjectInterface $object);

  /**
   * @param $id
   *
   * @return void
   */
  public static function delete($id);

  /**
   * @param int $id
   *
   * @return \EhmObjectInterface
   */
  public static function getById($id);

  /**
   * @return \EhmObjectInterface[]
   */
  public static function getAll();

  /**
   * @param array $conditions
   *
   * @return \EhmObjectInterface[]
   */
  public static function getAllByConditions($conditions);

  /**
   * @param \stdClass[] $results
   *
   * @return \EhmObjectInterface[]
   */
  static function resultsToObjects($results);
}