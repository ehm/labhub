<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

trait EhmObjectTrait {

  /**
   * Constructor.
   *
   * @param \stdClass $object
   * @param bool $json_object TRUE if the input is a JSON-API object, see
   *   http://jsonapi.org/format/#document-resource-objects
   */
  public function __construct($object = NULL, $json_object = FALSE) {
    if (!is_null($object) && is_object($object)) {
      if ($json_object) {
        $data = json_decode(json_encode($object), TRUE);
        $this->setId($data['id']);
        if (array_key_exists('attributes', $data)) {
          foreach ($data['attributes'] as $key => $value) {
            $this->$key = $value;
          }
        }
        if (array_key_exists('relationships', $data)) {
          foreach ($data['relationships'] as $key => $value) {
            $this->$key = $value['data']['id'];
          }
        }
      }
      else {
        $vars = array_keys(get_object_vars($object));
        foreach ($vars as $var) {
          $this->$var = $object->$var;
        }
      }

    }
  }

  public static function getIdFieldname() {
    return self::$id_fieldname;
  }

  public function getId() {
    $fieldname = self::getIdFieldname();
    return $this->$fieldname;
  }

  public function setId($id) {
    $fieldname = self::getIdFieldname();
    $this->$fieldname = $id;
  }

  public function isEmpty() {
    return !$this->getId() ? TRUE : FALSE;
  }

  public function __toString() {
    return json_encode(get_object_vars($this));
  }

}