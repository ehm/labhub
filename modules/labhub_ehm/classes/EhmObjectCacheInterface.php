<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

interface EhmObjectCacheInterface {

  /**
   * @param \EhmObjectInterface $object
   *
   * @return void
   */
  public static function cache(EhmObjectInterface $object);

  /**
   * @param $object_id
   *
   * @return \EhmObjectInterface
   */
  public static function getCached($object_id);

  /**
   * @param $object_id
   *
   * @return void
   */
  public static function deleteCached($object_id);

  /**
   * @return \EhmObjectInterface[]
   */
  public static function getAllCached();

  /**
   * @return string
   */
  static function getCacheId();

  /**
   * @return void
   */
  public static function flush();
}