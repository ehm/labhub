<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubEhmService implements \LabhubServiceInterface {

  use \LabhubObjectTrait;
  use \LabhubServiceTrait;

  const TYPE_LABHUB_EHM_SERVICE = 'LabHub EHM Service Host';

  public function __construct() {
    $this->service_type = self::TYPE_LABHUB_EHM_SERVICE;
  }

  public function setServiceType($service_type) {
    // Do nothing
  }

}