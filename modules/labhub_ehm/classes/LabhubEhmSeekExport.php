<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class LabhubEhmSeekExport
 *
 * Provides methods to export EHM sample measurement data in SEEK-compatible
 * format.
 */
class LabhubEhmSeekExport {

  /**
   * @var array $samples Array of arrays containing export values for an
   *   EHM sample each.
   */
  private $samples = [];

  /**
   * LabhubEhmSeekExport constructor.
   *
   * @param array $samples Array of arrays containing export values for an
   *   EHM sample each.
   */
  public function __construct($samples) {
    if (is_array($samples)) {
      $this->samples = $samples;
    }
  }

  /**
   *  Construct export format table and trigger download.
   */
  public function download() {
    if ($this->samples) {
      try {
        $filename = 'LabHub_EHM_measurement_samples_SEEK_export.csv';
        $rows = $this->samples;
        $header = [
          'Title',
          'Measurement LabHub ID',
          'Measurement Date',
          'EHM LabHub ID',
          'EHM Casting Date',
          'EHM LabHub URL',
          'Mastermix LabHub ID',
          'Experiment LabHub ID',
        ];

        /**
         * Set HTTP headers triggering immediate download of a CSV file.
         */
        drupal_add_http_header('Content-Type', 'text/csv; utf-8');
        drupal_add_http_header('Content-Disposition',
          'attachment; filename=' . $filename);

        /**
         * Build output data in CSV format (tab separated).
         */
        $output = '';
        $keys = [];
        foreach ($header as $value) {
          $keys[] = $value;
        }
        if ($keys) {
          $output .= implode("\t", $keys) . "\n";
        }
        foreach ($rows as $value) {
          if (!is_array($value)) {
            throw new InvalidArgumentException();
          }
          $output .= implode("\t", $value) . "\n";
        }

        /**
         * Print collected output to CSV file that is immediately send to client browser.
         */
        print $output;
        exit();

      } catch (Exception $exception) {
        watchdog_exception(__CLASS__, $exception);
      }
    }
  }
}