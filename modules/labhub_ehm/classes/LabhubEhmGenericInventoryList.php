<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubEhmGenericInventoryList {

  private $table_name = '';

  private $conditions = [];

  private $header = '';

  /**
   * @var \LabhubEhmApiClient
   */
  private $client = NULL;

  public function __construct($table_name, $conditions = []) {
    $this->table_name = $table_name;
    $this->conditions = $conditions;

    $this->client = new LabhubEhmApiClient();
  }

  public function addHeader($header) {
    $this->header = $header;
  }

  public function basicTable() {
    $response = $this->client->selectGeneric($this->table_name,
      $this->conditions);

    $data = json_decode($response, TRUE)['data'];

    $display = $this->header ? "<h2>$this->header</h2>" : "";

    if (!count($data)) {
      return $display;
    }

    $header = array_keys($data[0]);
    $rows = [];

    if (class_exists($classname = $this->table_name)) {
      $interfaces = class_implements($classname);
      if (in_array('EhmObjectInterface', $interfaces)) {
        $id_field = $classname::getIdFieldname();
        if (!in_array($id_field, $header)) {
          unset($id_field);
        }
      }
    }

    foreach ($data as $key => $value) {
      $rows[$key] = $value;
      if (isset($id_field)) {
        try {
          $id = $value[$id_field];
          $rows[$key][$id_field] = method_exists($classname, 'url_from_id') ?
            l($id, $classname::url_from_id($id)) : $id;
        } catch (Exception $e) {
          watchdog_exception(__CLASS__ . $e);
        }
      }
    }

    try {
      $display .= theme('table',
        ['header' => $header, 'rows' => $rows]);
    } catch (Exception $e) {
      $display = '';
    }
    return $display;
  }
}