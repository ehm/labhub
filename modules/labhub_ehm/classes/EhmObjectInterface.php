<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

interface EhmObjectInterface {

  /**
   * EhmObjectInterface constructor.
   *
   * @param \stdClass $object
   * @param bool $json_object
   */
  public function __construct($object = NULL, $json_object = FALSE);

  /**
   * @return string
   */
  public static function getIdFieldname();

  /**
   * @return string
   */
  public function __toString();

  /**
   * @param int $id
   *
   * @return void
   */
  public function setId($id);

  /**
   * @return int
   */
  public function getId();

  /**
   * @return void
   */
  public function save();

  /**
   * @return bool
   */
  public function isEmpty();

  /**
   * @return string a JSON encoded string on success or <b>FALSE</b> on failure
   */
  public function jsonObject();

}