<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Enable other modules to add content to EHM person display page.
 *
 * @param int $idPerson EHM person ID.
 *
 * @return string Content to be attached to EHM person display page.
 */
function hook_ehm_person_display($idPerson) {
  return "<h2>Additional section on LabHub EHM person 
                display page for user $idPerson</h2>
            <div>Content of additional section.</div>";
}