<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * @param int $idPerson ContractionDB user id
 *
 * @return string Rendered page contents.
 */
function labhub_ehm_person_view($idPerson) {

  /**
   * ToDo: Permissions: a user may view his/her own page, admins may view anybodies' page
   */

  $person = PersonRepository::getById($idPerson);

  $display = '';

  /** @var \Person $person */
  $display .= $person->displayPageHeader();

  if (count($modules = module_implements($hook = 'ehm_person_display'))) {
    foreach ($modules as $module) {
      $display .= module_invoke($module, $hook, $idPerson);
    }
  }

  /**
   * ToDo: add submit button if RSpace key is set for this user
   * Submit shall trigger export to RSpace...
   */
  if (count(TaskRepository::fetch($idPerson))) {
    $tasks_form = drupal_get_form('labhub_ehm_person_tasks', $person->getId());
    $display .= '<h2>Tasks</h2>';
    $display .= drupal_render($tasks_form);
  }

  return $display;
}

function labhub_ehm_person_tasks($form = [], &$form_state, $idPerson) {

  $person = PersonRepository::getById($idPerson);
  /** @var \Person $person */
  $form = $person->getFormTasks();
  $form_state['idPerson'] = $idPerson;

  return $form;
}

function labhub_ehm_person_tasks_ajax_callback(&$form, &$form_state) {
  $begin = isset($form_state['values']['begin']) ? $form_state['values']['begin'] : NULL;
  $end = isset($form_state['values']['end']) ? $form_state['values']['end'] : NULL;

  if ($begin) {
    // Set end date to +1 day to make date query inclusive
    $end = isset($end) ? date('Y-m-d',
      strtotime("+1 days", strtotime($end))) : NULL;

    // Rebuild tasks display
    $tasks = TaskRepository::fetch($form_state['idPerson'], $begin, $end);
    $table = Task::table($tasks);

    $form['fieldset_display']['display']['#markup'] = $table;
  }

  return $form;

}