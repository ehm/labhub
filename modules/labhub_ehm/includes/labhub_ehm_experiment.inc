<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Default experiment overview page
 */
function labhub_ehm_experiment() {

  $experiment_eventtype_id = variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_EXPERIMENT);
  $events = EventRepository::getAllByConditions(['idEventType' => $experiment_eventtype_id]);

  /** @var \Event[] $events */
  return Event::basicTable($events);
}

/**
 * Display event page
 */
function labhub_ehm_experiment_view($id) {
  $experiment = ExperimentRepository::getById($id);
  /**
   * @var \Experiment $experiment
   */
  return $experiment->displayPage();

}

/**
 * Display form to add new experiment
 */
function labhub_ehm_experiment_create($form, &$form_state) {
  _labhub_ehm_include_css();

  $experiment = new Experiment();
  $form = $experiment->getForm();

  // ToDo: continue

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Save',
  ];

  return $form;
}

function labhub_ehm_experiment_select_labwaretype_ajax($form, $form_state) {

  if ($type_id = $form_state['input']['labwaretype']) {

    $labwares = LabwareRepository::getAllByConditions(['idLabwareType' => $type_id]);

    if (count($labwares)) {
      $options_labware = [];
      foreach ($labwares as $labware) {
        /** @var \Labware $labware */
        $options_labware[$labware->getId()] = $labware->getName();
      }
      $form['fieldset_labware']['labware']['#options'] = $options_labware;

    }
    else {
      $options_labware = [];
      $form['fieldset_labware']['labware']['#options'] = $options_labware;
    }

    $labwaretype = LabwareRepository::getById($type_id);
    /** @var \LabwareType $labwaretype */
    $platetype = PlateTypeRepository::getById($labwaretype->getId());
    /** @var \PlateType $platetype */

    $options = Experiment::well_checkboxes_options($platetype->getPlateRows(),
      $columns = $platetype->getPlateCols());
    $form['fieldset_wells']['wells']['#multicolumn']['width'] = $columns;
    $form['fieldset_wells']['wells']['#options'] = $options;

    foreach ($options as $key => $value) {
      $form['fieldset_wells']['wells'][$key]['#checked'] = TRUE;
      $form['fieldset_wells']['wells'][$key]['#prefix'] .= '<div class="ehm-experiment-form-well-wrapper-checked">';
      $form['fieldset_wells']['wells'][$key]['#suffix'] .= '</div>';

    }
  }
  return $form;
}

function labhub_ehm_experiment_select_wells_ajax($form, $form_state) {
  $options = $form['fieldset_wells']['wells']['#options'];
  foreach ($options as $key => $value) {
    $class = "ehm-experiment-form-well-wrapper";
    $class .= !is_null($form_state['input']['wells'][$key]) ? "-checked" : "";
    $form['fieldset_wells']['wells'][$key]['#prefix'] .= '<div class="' . $class . '">';
    $form['fieldset_wells']['wells'][$key]['#suffix'] .= '</div>';
  }

  return $form['fieldset_wells']['wells'];
}

/**
 * Handle experiment_create form submit action
 */
function labhub_ehm_experiment_create_submit($form, &$form_state) {
  $values = $form_state['values'];

  $event = new Event();
  /** @var \Event $event */
  $event->setIdEventType(variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_EXPERIMENT));
  $event->setName($values['name']);
  $event->setDate($values['date']);
  $event->setIdLabware($values['labware']);
  // Fake parent ID for creation (cheats database table constraint)
  $event->setIdParent(1);
  $event->save();
  // Set correct own ID as parent ID as is correct per database design...
  $event->setIdParent($event->getId());
  $event->save();

  $experiment = new Experiment();
  $experiment->setId($event->getId());
  $experiment->setName($values['name']);
  $experiment->setIdPerson($values['idPerson']);
  $experiment->save();

  $solution_id = $values['mastermix'];
  $casting_eventtypes = [6, 7];
  $cond = [
    'idEventType' => implode(",", $casting_eventtypes),
    'idSolution' => $solution_id,
  ];
  $ehm_casting_eventtype = EventTypeRepository::getAllByConditions($cond)[0];
  $ehm_casting_eventtype_id = $ehm_casting_eventtype->getId();

  $checked_wells = array_values($values['wells']);

  $well_type = LabwareTypeRepository::getAllByConditions([
    'idParent' => $values['labwaretype'],
    'type' => LabwareType::TYPE_WELL,
  ])[0];

  $wells = LabwareRepository::getAllByConditions([
    'idParent' => $event->getIdLabware(),
    'idLabwareType' => $well_type->getId(),
  ]);

  /** @var \Labware[] $wells */
  foreach ($wells as $well) {
    $cast_event = new Event();
    $cast_event->setIdEventType($ehm_casting_eventtype_id);
    $cast_event->setIdLabware($well->getId());
    $cast_event->setIdParent($event->getId());
    $cast_event->setDate($event->getDate());
    $cast_event->save();

    $ehm = new EHM();
    $ehm->setId($cast_event->getId());
    $ehm->setIdPerson($values['person_execution']);
    // Set unchecked wells' EHM inactive
    if (in_array($well->getPos(), $checked_wells)) {
      $ehm->setActive(1);
    }
    else {
      $ehm->setActive(0);
    }
    $ehm->save();
  }

  $form_state['redirect'] = LABHUB_EHM_URL_EXPERIMENT;
}

