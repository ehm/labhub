<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Display Labware default page
 */
function labhub_ehm_labware() {

  $types = LabwareTypeRepository::getAllByConditions(['type' => 'plate']);
  $plate_ids = [];
  foreach ($types as $type) {
    $plate_ids[] = $type->getId();
  }
  $plate_ids = implode(",", $plate_ids);

  $labware_list = new LabhubEhmGenericInventoryList('labware',
    ['idLabwareType' => $plate_ids]);
  $labware_list->addHeader("Microwell Plates");

  $display = $labware_list->basicTable();

  $list = new LabhubEhmGenericInventoryList('labwaretype', ['limit' => 0]);
  $list->addHeader('Labware Types');

  $display .= $list->basicTable();

  return $display;
}

/**
 *
 */
function labhub_ehm_labware_create($form, &$form_state) {

  $labware = new Labware();

  $form = $labware->getForm();

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Save',
  ];

  return $form;
}

function labhub_ehm_labware_create_submit($form, $form_state) {
  $values = $form_state['values'];

  $labware = new Labware();
  $labware->setIdLabwareType($values['plate_type']);
  $labware->setName($values['label']);
  $labware->setDate($values['date']);

  $labware->save();

  /**
   * If this is a microwell plate, create the corresponding
   * wells as child instances of Labware
   */
  if ($labware->getId() && $labware->isPlate()) {
    LabwareRepository::createWellsForPlate($labware);
  }

  $form_state['redirect'] = LABHUB_EHM_URL_LABWARE;
}
