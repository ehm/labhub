<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Display page for a Measurement.
 *
 * @param $measurement_id
 *
 * @return string Page content for HTML rendering.
 */
function labhub_ehm_measurement_view($measurement_id) {

  $measurement = MeasurementRepository::getById($measurement_id);

  /** @var \Measurement $measurement */
  return $measurement->displayPage();
}

/**
 * Drupal form containing a table of EHM sample measurements where each row is
 * selectable for export in SEEK compatible format.
 *
 * @param array $form
 * @param $form_state
 * @param $measurement_id
 * @param array $table_variables Associative array compatible with Drupal table
 *   theme, i.e. containing at least "header" and "rows" keys
 *
 * @return array Drupal form array.
 */
function labhub_ehm_measurement_export_form(
  $form = [],
  &$form_state,
  $measurement_id,
  $table_variables
) {
  /**
   * Rebuild header and options arrays in a tableselect-theme compatible format.
   *
   * https://api.drupal.org/api/drupal/includes!form.inc/function/theme_tableselect/7.x
   */
  $header_values = $table_variables['header'];
  $header = [];
  foreach ($header_values as $header_value) {
    $key = substr(md5($header_value), 0, 8);
    $header[$key] = t($header_value);
  }
  $rows = $table_variables['rows'];

  $options = [];
  /**
   * index 0 must be skipped in options array because it would mess up
   * handling of submitted values
   */
  $i = 1;
  foreach ($rows as $row_array) {
    $row = [];
    foreach ($header as $h_key => $h_value) {
      $row[$h_key] = array_shift($row_array);
    }
    $options[$i++] = $row;
  }

  /**
   * Build form using tableselect theme.
   */

  $form['table'] = [
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No content available.'),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Export for SEEK',
  ];

  $form_state['measurement_id'] = $measurement_id;

  return $form;
}

/**
 * Drupal form handler triggering export of select EHM sample values in SEEK
 * format.
 *
 * @param array $form
 * @param $form_state
 */
function labhub_ehm_measurement_export_form_submit($form = [], &$form_state) {
  /**
   * Get the select row numbers from form engine and remove NULL values.
   */
  $options = $form_state['values']['table'];
  $options = array_filter($options);

  /**
   * Get an array of EHM sample measurement values in export-ready format.
   */
  $measurement = MeasurementRepository::getById($form_state['measurement_id']);
  $rows = $measurement->getWellsExportFormat();

  /**
   * Filter only selected EHM samples.
   */
  $selected = [];
  foreach ($options as $option) {
    //
    /**
     * Correct for skipped 0-key in form options array, see form definition
     * in function labhub_ehm_measurement_export_form() above.
     */
    $selected[] = $rows[$option - 1];
  }

  /**
   * Trigger export.
   */
  $export = new LabhubEhmSeekExport($selected);
  $export->download();
}