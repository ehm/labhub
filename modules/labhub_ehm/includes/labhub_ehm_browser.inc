<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_ehm_browser($form, &$form_state) {

  /**
   * Parse URL parameters
   *
   * - Limit
   * - Table
   * - ...?
   */
  $params = array_keys($_GET);
  $parameters = [];
  foreach ($params as $key) {
    if ($param = filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRING)) {
      $parameters[$key] = $param;
    }
  }
  if (array_key_exists('limit_override', $_GET)) {
    $parameters['limit_override'] = filter_input(INPUT_GET, 'limit_override',
      FILTER_SANITIZE_NUMBER_INT);
  }

  if (array_key_exists('table', $parameters)) {
    $table_default = $parameters['table'];
  }
  else {
    $table_default = NULL;
  }

  if (array_key_exists('limit_override', $parameters)) {
    $limit_override_default = $parameters['limit_override'];
  }
  else {
    $limit_override_default = 0;
  }

  // FORM SELECT TABLE
  $options_tables = [
    NULL => '--Select--',
    'analysis' => 'analysis',
    'analysistype' => 'analysistype',
    'area' => 'area',
    'cells' => 'cells',
    'celltype' => 'celltype',
    'compound' => 'compound',
    'ehm' => 'ehm',
    'event' => 'event',
    'eventtype' => 'eventtype',
    'experiment' => 'experiment',
    'forces' => 'forces',
    'labware' => 'labware',
    'labwaretype' => 'labwaretype',
    'measurement' => 'measurement',
    'peak' => 'peak',
    'peakanalysis' => 'peakanalysis',
    'person' => 'person',
    'platetype' => 'platetype',
    'protocol' => 'protocol',
    'protocolstep' => 'protocolstep',
    'solution' => 'solution',
    'solution_cells' => 'solution_cells',
    'solution_compound' => 'solution_compound',
    'solution_solution' => 'solution_solution',
    'vehm' => 'vehm',
    'vexperiment' => 'vexperiment',
    'vmeasurement' => 'vmeasurement',
    'vpeak' => 'vpeak',
  ];

  // Re-use $param variable to construct array of URL parameters for link field default value
  $params = [];
  /**
   * If URL parameters were passed, build display content on page load.
   */
  if ($table_default && in_array($table_default, $options_tables)) {
    $params[] = 'table=' . $table_default;
    if ($limit_override_default == 1) {
      $conditions = ['limit' => 'null'];
      $params[] = 'limit=null';
    }
    else {
      $conditions = [];
    }
    $display_default = new LabhubEhmGenericInventoryList($table_default,
      $conditions);
    $display_default = $display_default->basicTable();
    $link_default = _construct_permanent_link($params);
  }
  else {
    $display_default = '';
    $link_default = '';
  }

  /**
   * Construction the actual form
   */

  $form = [];

  $form['fieldset_parameters'] = [
    '#type' => 'fieldset',
    '#title' => 'Parameters',
  ];

  $form['fieldset_parameters']['table'] = [
    '#type' => 'select',
    '#title' => 'Select table or view',
    '#options' => $options_tables,
    '#default_value' => $table_default,
    '#ajax' => [
      'callback' => 'labhub_ehm_browser_ajax_callback',
      'wrapper' => 'labhub_ehm_browser_wrapper',
    ],
  ];

  $form['fieldset_parameters']['limit_override'] = [
    '#type' => 'checkbox',
    '#title' => 'Override standard limit (10 rows)?',
    '#default_value' => $limit_override_default,
    '#ajax' => [
      'callback' => 'labhub_ehm_browser_ajax_callback',
      'wrapper' => 'labhub_ehm_browser_wrapper',
    ],
  ];

  $form['fieldset_parameters']['link'] = [
    '#type' => 'textfield',
    '#title' => 'Permanent link for this view',
    '#default_value' => $link_default,
    '#attributes' => ['readonly' => 'readonly'],
  ];

  $form['fieldset_display'] = [
    '#type' => 'fieldset',
    '#title' => 'View',
  ];

  // TABLE DISPLAY AREA
  $form['fieldset_display']['display'] = [
    '#markup' => $display_default,
    '#prefix' => '<div class="labhub_ehm_browser_display_wrapper">',
    '#suffix' => '</div>',
  ];

  $form['#prefix'] = '<div id="labhub_ehm_browser_wrapper">';
  $form['#suffix'] = '</div>';

  return $form;
}

function labhub_ehm_browser_ajax_callback($form, &$form_state) {
  $table = $form_state['input']['table'];
  $params = [];
  $params[] = 'table=' . $table;
  $conditions = [];
  if (isset($form_state['input']['limit_override']) && $form_state['input']['limit_override']) {
    $conditions['limit'] = 'null';
    $params[] = 'limit_override=1';
  }

  $form['fieldset_parameters']['link']['#value'] = _construct_permanent_link($params);

  $display = new LabhubEhmGenericInventoryList($table, $conditions);
  $form['fieldset_display']['display']['#markup'] = $display->basicTable();

  return $form;
}

function _construct_permanent_link($params) {
  global $base_url;
  return $base_url . base_path() . LABHUB_EHM_URL_TABLE_BROWSER . '?' . implode("&",
      $params);
}