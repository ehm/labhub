<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_ehm_config($form, &$form_state) {
  $form = [];

  $form['fieldset_host'] = [
    '#type' => 'fieldset',
    '#title' => 'LabHub EHM Module default host',
  ];

  $services = LabhubServiceRepository::getAllByType(LabhubEhmService::TYPE_LABHUB_EHM_SERVICE);

  $options = [];
  foreach ($services as $service) {
    $options[$service->getId()] = $service->getServiceName();
  }
  $form['fieldset_host']['labhub_ehm_config_host'] = [
    '#type' => 'select',
    '#title' => t("Choose default host"),
    '#options' => $options,
    '#default_value' => $default_host = variable_get("labhub_ehm_config_host"),
    '#required' => TRUE,
  ];

  $form['fieldset_jwt'] = [
    '#type' => 'fieldset',
    '#title' => 'JSON Web Token Authorization settings',
  ];

  $default_key = variable_get("labhub_ehm_config_jwt_key", NULL);
  $form['fieldset_jwt']['labhub_ehm_config_jwt_key'] = [
    '#type' => 'password',
    '#title' => 'Shared secret (key)',
    '#required' => is_null($default_key) ? TRUE : FALSE,
  ];

  $form['fieldset_jwt']['labhub_ehm_config_jwt_iss'] = [
    '#type' => 'textfield',
    '#title' => t('Issuer'),
    '#description' => t('Issuer string for JWT Authentication process.'),
    '#default_value' => $default_iss = variable_get("labhub_ehm_config_jwt_iss",
      NULL),
    '#required' => TRUE,
  ];

  $form['fieldset_jwt']['labhub_ehm_config_jwt_sub'] = [
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Subject string for JWT Authentication process.'),
    '#default_value' => $default_sub = variable_get("labhub_ehm_config_jwt_sub",
      NULL),
    '#required' => TRUE,
  ];


  /**
   * Only display EHM configuration if default service connection has
   * been configured (above) and works.
   */
  $client = new LabhubEhmApiClient();
  if ($client->test()) {
    drupal_get_messages("warning");

    $form['fieldset_ehm_settings'] = [
      '#type' => 'fieldset',
      '#title' => 'Experiment Configuration',
    ];

    $eventtypes = EventTypeRepository::getAll();
    $options = [];
    foreach ($eventtypes as $eventtype) {
      /** @var EventType $eventtype */
      $options[$eventtype->getId()] = $eventtype->getName();
    }

    $form['fieldset_ehm_settings'][LABHUB_EHM_VAR_EVENTTYPE_ID_EXPERIMENT] = [
      '#type' => 'select',
      '#title' => 'Choose event type that defines Experiment',
      '#options' => $options,
      '#default_value' => variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_EXPERIMENT,
        NULL),
    ];

    $form['fieldset_ehm_settings'][LABHUB_EHM_VAR_EVENTTYPE_ID_CASTING] = [
      '#type' => 'checkboxes',
      '#title' => 'Choose event types that define EHM casting',
      '#options' => $options,
      '#default_value' => variable_get(LABHUB_EHM_VAR_EVENTTYPE_ID_CASTING,
        []),
    ];

  }
  else {
    /**
     * If connection test failed, display message.
     */
    $message = t('Could not verify authentication with selected 
         Service. Please check if the entered credentials are correct.');
    drupal_set_message($message, 'warning', FALSE);
  }

  /**
   * Add custom submit handler for JWT key field
   */
  $form['#submit'] = ['labhub_ehm_config_submit'];

  return system_settings_form($form);
}

/**
 * Drupal form submit handler. Fill in JWT key default value.
 */
function labhub_ehm_config_submit($form, &$form_state) {

  if (empty($form_state['values']['labhub_ehm_config_jwt_key'])) {
    $default_key = variable_get("labhub_ehm_config_jwt_key");
    $form_state['values']['labhub_ehm_config_jwt_key'] = $default_key;
  }
}

function labhub_ehm_config_service_create($form, &$form_state) {
  $service = new LabhubEhmService();

  $form = LabhubServiceForm::create($service);

  $form = LabhubServiceForm::addSubmitButton($form);

  return $form;
}

function labhub_ehm_config_service_create_validate($form, &$form_state) {

  $values = $form_state['values'];
  $service = new LabhubEhmService();
  $service->setHost($values['host']);
  if (!$service->testConnection()) {
    form_set_error('host', 'Could not connect to the host.');
  }
}

function labhub_ehm_config_service_create_submit($form, &$form_state) {
  $values = $form_state['values'];

  $service = new LabhubEhmService();
  $service->setHost($values['host']);
  $service->setServiceName($values['service_name']);
  $service->save();

  $form_state['redirect'] = LABHUB_EHM_URL_CONFIG;
}
