<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Implements @see \hook_form()
 */
function labhub_ehm_labware_type_create($form, &$form_state) {
  $labwaretype = new LabwareType();
  $form = $labwaretype->getForm();

  /**
   * @var \PlateType $platetype
   */
  $platetype = PlateTypeRepository::getById($labwaretype->getIdParent());
  $form = array_merge($form, $platetype->getForm());

  $form['#prefix'] = '<div id="form-labwaretype-wrapper">';
  $form['#suffix'] = '</div>';

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Save',
  ];

  return $form;
}

function labhub_ehm_labware_type_ajax_callback($form, $form_state) {
  $type = $form_state['input']['type'];

  if ($type == LabwareType::TYPE_PLATE) {
    $form['fieldset_platetype']['#access'] = TRUE;
    $form['fieldset_platetype']['material']['#required'] = TRUE;
    $form['fieldset_platetype']['wells']['#required'] = TRUE;
    $form['fieldset_platetype']['plate_rows']['#required'] = TRUE;
    $form['fieldset_platetype']['plate_cols']['#required'] = TRUE;
  }
  else {
    $form['fieldset_platetype']['#access'] = FALSE;
    $form['fieldset_platetype']['material']['#required'] = FALSE;
    $form['fieldset_platetype']['wells']['#required'] = FALSE;
    $form['fieldset_platetype']['plate_rows']['#required'] = FALSE;
    $form['fieldset_platetype']['plate_cols']['#required'] = FALSE;
  }

  return $form;
}

function labhub_ehm_labware_parent_ajax_callback($form, $form_state) {
  $parent_id = $form_state['input']['parent'];
  /**
   * @var \PlateType $parent
   */
  $parent = PlateTypeRepository::getById($parent_id);

  $form['fieldset_platetype']['material']['#value'] = $parent->getMaterial();
  $form['fieldset_platetype']['plate_cols']['#value'] = $parent->getPlateCols();
  $form['fieldset_platetype']['plate_rows']['#value'] = $parent->getPlateRows();
  $form['fieldset_platetype']['wells']['#value'] = $parent->getWells();
  $form['fieldset_platetype']['wellWidth']['#value'] = $parent->getWellWidth();
  $form['fieldset_platetype']['wellHeight']['#value'] = $parent->getWellHeight();
  $form['fieldset_platetype']['stretcherDiameter']['#value'] = $parent->getStretcherDiameter();
  $form['fieldset_platetype']['stretcherDistance']['#value'] = $parent->getStretcherDistance();
  $form['fieldset_platetype']['bEqualDiameter']['#value'] = $parent->getBEqualDiameter();
  $form['fieldset_platetype']['force_mm']['#value'] = $parent->getForceMm();

  return $form;
}

function labhub_ehm_labware_calculate_wells_ajax_callback($form, $form_state) {
  $columns = array_key_exists('plate_cols',
    $form_state['values']) ? $form_state['values']['plate_cols'] : FALSE;
  $rows = array_key_exists('plate_rows',
    $form_state['values']) ? $form_state['values']['plate_rows'] : FALSE;

  if ($columns && $rows) {
    $form['fieldset_platetype']['wells']['#value'] = $columns * $rows;

    return $form['fieldset_platetype']['wells'];
  }
}

/**
 * ToDo: implement validation, name/unique, idParent/exists
 */
function labhub_ehm_labware_type_create_validate($form, &$form_state) {
}

function labhub_ehm_labware_type_create_submit($form, &$form_state) {
  $values = $form_state['values'];

  $labwaretype = new LabwareType();
  $labwaretype->setType($values['type']);
  $labwaretype->setIdParent($values['parent']);
  $labwaretype->setName($values['name']);
  $labwaretype->setDescription($values['description']);

  $labwaretype->save();

  /**
   * If saved successfully and type is "plate", store further information and
   * create a child-labwaretype "well".
   */
  if (!$labwaretype->isEmpty() && $labwaretype->getType() == LabwareType::TYPE_PLATE) {

    /**
     * Store "plate" information in "platetype" table
     */
    $platetype = new PlateType();
    $platetype->setId($labwaretype->getId());

    $platetype->setDescription($values['description']);
    $platetype->setWells($values['wells']);
    $platetype->setMaterial($values['material']);
    $platetype->setStretcherDiameter($values['stretcherDiameter']);
    $platetype->setBEqualDiameter($values['bEqualDiameter']);
    $platetype->setWellWidth($values['wellWidth']);
    $platetype->setWellHeight($values['wellHeight']);
    $platetype->setPlateRows($values['plate_rows']);
    $platetype->setPlateCols($values['plate_cols']);
    $platetype->setStretcherDistance($values['stretcherDistance']);
    $platetype->setForceMm($values['force_mm']);

    $platetype->save();

    /**
     * Create child-labwaretype of type "well"
     */

    $well = new LabwareType();
    $well->setName($labwaretype->getName() . '_well');
    $well->setIdParent($labwaretype->getId());
    $well->setType(LabwareType::TYPE_WELL);
    $well->save();
  }

  $form_state['redirect'] = LABHUB_EHM_URL_LABWARE;
}