<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * @param int $experiment_id Parent experiment ID
 * @param int $labware_well_id ID of the well/labware containing the EHM sample
 *
 * @return string EHM sample display page contents
 */
function labhub_ehm_sample_view($experiment_id, $labware_well_id) {

  $experiment = ExperimentRepository::getById($experiment_id);
  $measurement_ids = $experiment->getMeasurementEventIDs();

  $m_ids = implode(",", $measurement_ids);
  $conditions = [
    'idParent' => $m_ids,
    'idLabware' => $labware_well_id,
  ];
  $sample_measuring_events = EventRepository::getAllByConditions($conditions);

  $header = ['Measurement', 'Date', 'Person', 'FoC %', 'FoC % STDDEV',];
  $rows = [];
  foreach ($sample_measuring_events as $event) {
    /** @var \Event $event */
    $measurement_id = $event->getIdParent();
    $measurement = MeasurementRepository::getById($measurement_id);
    $measurement_link = l($measurement->getId(), $measurement->url());

    if (is_array($statistics = $measurement->getStatistics())
      && array_key_exists($labware_well_id, $statistics)) {
      $stats = $statistics[$labware_well_id];
      $rows[] = [
        $measurement_link,
        $measurement->getEvent()->getDate(),
        $measurement->getPerson()->displayName(TRUE),
        $stats['foc_mean'],
        $stats['foc_stddev'],
      ];
    }
  }
  try {
    $display_measurements = theme('table',
      ['header' => $header, 'rows' => $rows]);
  } catch (Exception $e) {
    watchdog_exception(__CLASS__, $e);
  }

  _labhub_ehm_include_css();

  $display = '';

  $experiment_link = l("Back to Experiment",
    $experiment->url(),
    ['attributes' => ['class' => 'btn btn-default btn-labhub-back-exp']]);

  $display .= $experiment_link;
  $display .= $experiment->displayPageHeader();

  if (isset($display_measurements)) {
    $display .= $display_measurements;
  }

  $display .= _draw_chart($rows);

  return $display;
}

function _draw_chart($rows = []) {
  drupal_add_js('https://code.highcharts.com/highcharts.js');
  drupal_add_js('https://code.highcharts.com/highcharts-more.js');
  //  drupal_add_js(drupal_get_path('module',
  //      'labhub_ehm') . '/lib/highcharts/themes/sand-signika.js');

  $categories = [];
  $values = [];
  $errors = [];
  foreach ($rows as $row) {
    $categories[] = $row[1];
    $values[] = (double) $row[3];
    $errors[] = [$row[3] - $row[4], $row[3] + $row[4]];
  }
  $categories = json_encode($categories);
  $values = json_encode($values);
  $errors = json_encode($errors);

  $chart = "<div id=\"highchartcontainer\" style=\"width:100%; height: 450px;\"></div>";
  $chart .= "<script>
              (function () { 
                  var myChart = Highcharts.chart('highchartcontainer', {
                      chart: {
                          type: 'line',
                      },
                      title: {
                          text: 'EHM force development'
                      },
                      xAxis: {
                          categories: " . $categories . "
                      },
                      yAxis: {
                          title: {
                              text: 'Force'
                          }
                      },
                      series: [{
                          name: 'Contraction force',
                          type: 'line',
                          data: " . $values . "
                      },
                      {
                          name: 'Force error',
                          type: 'errorbar',
                          data: " . $errors . "
                      }],
                      credits: {
                          enabled: false
                      }
                  });
              }(jQuery));
            </script>";

  return $chart;
}