<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubSeekApiClient {

  use LabhubApiClientTrait;

  const TYPE_LABHUB_SEEK_API_CLIENT = 'LabHub SEEK API Client';

  public function testAuthentication() {
    if (!$this->getCredentials()) {
      drupal_set_message('No authentication credentials provided.');
    }
    else {

      $this->init();

      $this->addHeader('Accept: application/json');

      $result = $this->execute();

      $this->setResponseCode(curl_getinfo($this->handle)['http_code']);
      $this->setData($result);
      drupal_set_message('Result : ' . json_encode(json_decode($result)->data));
    }
  }

  public function selectPeople() {
    $this->setRoute('people');
    $this->init();
    $this->addHeader('Accept: application/json');
    $result = $this->execute();

    $this->setResponseCode(curl_getinfo($this->handle)['http_code']);
    $this->setData($result);

    return $this->getData();
  }

}