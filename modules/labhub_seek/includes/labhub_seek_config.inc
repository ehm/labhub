<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_seek_config() {

  $services = LabhubServiceRepository::getAllByType(LabhubSeekService::TYPE_SERVICE_SEEK_CONNECTOR);

  if (count($services) > 0) {

    $header = ['ID', 'Service Name', 'Host', 'Service Type'];
    $rows = [];
    foreach ($services as $service) {
      $rows[] = [
        $service->getId(),
        $service->getServiceName(),
        $service->getHost(),
        $service->getServiceType(),
      ];
    }

    try {
      return theme('table', ['header' => $header, 'rows' => $rows]);
    } catch (Exception $e) {
      watchdog_exception('Labhub SEEK', $e);
    }
  }

  return 'No SEEK services configured.';
}