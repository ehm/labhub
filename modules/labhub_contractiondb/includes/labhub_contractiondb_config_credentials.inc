<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_contractiondb_config_credentials() {

  $output = '';

  // Check if JWT is enabled
  if (!variable_get('labhub_contractiondb_config_auth_jwt', 0)) {
    $output .= '<blockquote class="bg-warning">' . t('The JSON Web Token 
      authentication method has not been enabled. Please remember to !linktext 
      before trying to access the API with a JWT token.',
        [
          '!linktext' => l('enable JWT authentication',
            LABHUB_CDB_API_URL_CONFIG_DEFAULT),
        ]) . '</blockquote>';
  }

  $credentials = LabhubCredentialsRepository::getAllByContext(LabhubContractiondbApiService::CREDENTIALS_CONTEXT_CONTRACTIONDB);

  $header = ['Credentials', 'Type'];
  $data = [];

  foreach ($credentials as $credential) {
    /**
     * @var \LabhubCredentials $credential
     */
    $data[] = [$credential->getId(), $credential->getType()];
  }

  try {
    $output .= theme('table', ['header' => $header, 'rows' => $data]);
  } catch (Exception $e) {
    watchdog_exception('Labhub ContractionDB Server', $e);
  }

  return $output;
}

function labhub_contractiondb_config_credentials_create($form, &$form_state) {

  $step = !isset($form_state['step']) ? 1 : $form_state['step'];

  switch ($step) {
    case 2:
      $credentials = new LabhubCredentials();
      $credentials->setType($form_state['values']['credentials_type']);
      $form = $credentials->getForm();

      $form['btn-prev'] = [
        '#type' => 'submit',
        '#name' => 'previous',
        '#submit' => ['_form_previous_step'],
        '#limit_validation_errors' => [],
        '#value' => t('Back'),
      ];

      break;
    case 1:
    default:
      $form = [];
      $form_state['step'] = 1;
      $options = [
        LabhubCredentials::TYPE_USERPASS => 'Username & Password',
        LabhubCredentials::TYPE_JWT => 'JSON Web Token',
      ];

      $default = isset($form_state['values']['credentials_type']) ?
        $form_state['values']['credentials_type'] :
        LabhubCredentials::TYPE_USERPASS;

      $form['credentials_type'] = [
        '#type' => 'select',
        '#title' => 'Choose which type of credentials you want to create',
        '#options' => $options,
        '#default_value' => $default,
        '#required' => TRUE,
      ];

      $form['btn-next'] = [
        '#type' => 'submit',
        '#name' => 'next',
        '#submit' => ['_form_next_step'],
        '#value' => t('Next'),
      ];
  }

  return $form;
}

function _form_next_step($form, &$form_state) {
  $step = $form_state['step'];
  $form_state['stored_values'][$step]['values'] = $form_state['values'];
  $form_state['step']++;
  drupal_set_message("next.. ");
  $form_state['rebuild'] = TRUE;
}

function _form_previous_step($form, &$form_state) {
  $form_state['step']--;
  $step = $form_state['step'];
  $form_state['values'] = $form_state['stored_values'][$step]['values'];
  drupal_set_message("previous.. ");
  $form_state['rebuild'] = TRUE;
}

function labhub_contractiondb_config_credentials_create_submit(
  $form,
  &$form_state
) {
  $step = $form_state['step']++;
  $form_state['stored_values'][$step]['values'] = $form_state['values'];

  $credentials = new LabhubCredentials();
  $credentials->setContext(LabhubContractiondbApiService::CREDENTIALS_CONTEXT_CONTRACTIONDB);
  $credentials->setType($form_state['stored_values'][1]['values']['credentials_type']);

  if ($credentials->getType() == LabhubCredentials::TYPE_USERPASS) {
    $userpass = $form_state['stored_values'][2]['values']['username'] . ':' .
      $form_state['stored_values'][2]['values']['password'];
    $credentials->setSecret($userpass);
    drupal_set_message("userpass : $userpass");
  }
  elseif ($credentials->getType() == LabhubCredentials::TYPE_JWT) {
    $credentials->setAlgorithm($form_state['stored_values'][2]['values']['algorithm']);
    $credentials->setSecret($form_state['stored_values'][2]['values']['key']);
    $credentials->setIssuer($form_state['stored_values'][2]['values']['iss']);
    $credentials->setSubject($form_state['stored_values'][2]['values']['sub']);
  }

  $credentials->save();
  drupal_set_message("Saved: " . $credentials->getId());

  $form_state['redirect'] = LABHUB_CDB_API_URL_CONFIG_CREDENTIALS;
}

// ToDo: implement
function clabhub_contractiondb_config_credentials_edit(
  $form,
  &$form_state,
  $id
) {
}

// ToDo: implement
function labhub_contractiondb_config_credentials_edit_submit(
  $form,
  &$form_state,
  $id
) {
}

// ToDo: implement
function labhub_contractiondb_config_credentials_delete(
  $form,
  &$form_state,
  $id
) {
}

// ToDo: implement
function labhub_contractiondb_config_credentials_delete_validate(
  $form,
  &$form_state,
  $id
) {
}

// ToDo: implement
function labhub_contractiondb_config_credentials_delete_submit(
  $form,
  &$form_state,
  $id
) {
}