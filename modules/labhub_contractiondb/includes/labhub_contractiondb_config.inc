<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_contractiondb_config($form, &$form_state) {
  $form = [];

  $services = LabhubDatabaseRepository::getAll(LabhubMysqlDatabase::TYPE_DATABASE_MYSQL);
  $options = [];
  $options[0] = 'None';
  foreach ($services as $service) {
    $options[$service->getId()] = $service->getServiceName();
  }

  $default = variable_get(LABHUB_CDB_VAR_DEFAULT_DATABASE);

  $form['database'] = [
    '#type' => 'fieldset',
    '#title' => 'Database Connection Settings',
  ];

  $form['database'][LABHUB_CDB_VAR_DEFAULT_DATABASE] = [
    '#type' => 'select',
    '#title' => t('Choose default MySQL Database for the API'),
    '#options' => $options,
    '#default_value' => $default,
  ];

  $form['authentication'] = [
    '#type' => 'fieldset',
    '#title' => 'Authentication Settings',
  ];


  // ToDo: insert custom text, this is "borrowed" from https://jwt.io
  $form['authentication']['intro'] = [
    '#markup' => '<blockquote><p>' . t('JSON Web Token (JWT) is an open standard 
    (RFC 7519) that defines a compact and self-contained way for securely 
    transmitting information between parties as a JSON object. This information 
    can be verified and trusted because it is digitally signed. 
    JWTs can be signed using a secret (with the HMAC algorithm)') . '</p></blockquote>',
  ];

  $form['authentication']['labhub_contractiondb_config_auth_jwt'] = [
    '#type' => 'checkbox',
    '#title' => "Enable Json Web Token Authentication",
    '#default_value' => variable_get('labhub_contractiondb_config_auth_jwt', 0),
  ];

  return system_settings_form($form);
}
