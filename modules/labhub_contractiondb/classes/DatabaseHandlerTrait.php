<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

trait DatabaseHandlerTrait {

  /**
   * @var \LabhubDatabaseInterface
   */
  private $database = NULL;

  /**
   * @var bool
   */
  private $debug = FALSE;

  /**
   * Constructor.
   *
   * @param \LabhubDatabaseInterface|NULL $database
   * @param bool $debug
   */
  public function __construct(
    LabhubDatabaseInterface $database = NULL,
    $debug = FALSE
  ) {
    if ($database) {
      $this->database = $database;
    }
    $this->debug = $debug;
  }


  /**
   * @return bool
   */
  public function isDebug() {
    return $this->debug;
  }

  /**
   * @param bool $debug
   */
  public function setDebug($debug) {
    $this->debug = $debug;
  }

  /**
   * @return \LabhubDatabaseInterface
   */
  public function getDatabase() {
    return $this->database;
  }

}