<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class GenericDatabaseJsonHandler implements DatabaseHandlerInterface {

  use DatabaseHandlerTrait;

  public static $primary_keys = [
    'analysis' => 'idArea',
    'analysistype' => 'idAnalysisType',
    'area' => ['idArea', 'idEvent'],
    'cells' => 'idCells',
    'celltype' => 'idCellType',
    'compound' => 'idCompound',
    'ehm' => 'idEvent',
    'event' => 'idEvent',
    'eventtype' => 'idEventType',
    'experiment' => 'idEvent',
    'forces' => 'idEvent',
    'labware' => 'idLabware',
    'labwaretype' => 'idLabwareType',
    'measurement' => 'idEvent',
    'peak' => 'idPeak',
    'peakanalysis' => 'idPeakAnalysis',
    'person' => 'idPerson',
    'platetype' => 'idLabwareType',
    'protocol' => ['idProtocol', 'idEventType'],
    'protocolstep' => 'idProtocol',
    'solution' => 'idSolution',
    'solution_cells' => ['idSolution', 'idCells'],
    'solution_compound' => ['idSolution', 'idCompound'],
    'solution_solution' => ['idSolution', 'idSolute'],
  ];

  /**
   * @param string $table_name
   * @param \stdClass $result
   *
   * @return array
   */
  public static function resultToJsonApiObject($table_name, stdClass $result) {
    $object = [];
    $object['type'] = $table_name;
    foreach (get_object_vars($result) as $key => $value) {
      if ($key == self::$primary_keys[$table_name]) {
        $object['id'] = $value;
      }
      elseif (in_array($key, self::$primary_keys)) {
        $object['relationships'][$key] = [
          'data' => [
            'type' => array_keys(self::$primary_keys, $key)[0],
            'id' => $value,
          ],
        ];
      }
      else {
        $object['attributes'][$key] = $value;
      }
    }
    return $object;
  }

  /**
   * Check if input is safe, i.e. only contains alphanumerical characters.
   *
   * @param $table_name
   *
   * @return bool FALSE if input contains special characters
   */
  private static function safe_name($table_name) {
    $safe = preg_match('/^[A-Za-z0-9_]+$/', $table_name) ? TRUE : FALSE;
    if (!$safe) {
      watchdog(self::DRUPAL_WATCHDOG_TYPE, "Unsafe table_name %t",
        ['%t' => $table_name], WATCHDOG_NOTICE);
    }
    return $safe;
  }

  public function selectAll($table_name, $limit = 10, $parameters = []) {
    $data = [];

    // Debug output:
    if ($this->debug) {
      $data["tableName"] = $table_name;
      $data["limit"] = $limit;
      $data['parameters'] = $parameters;
    }

    if (self::safe_name($table_name)) {

      /**
       * Switch to contractionDB database connection.
       */
      $this->database->activate();

      // Only proceed if table name exists
      if (db_table_exists($table_name)) {

        // If parameters match table fields add search condition
        $db_and_trigger = FALSE;
        if (count($parameters)) {
          $db_and = db_and();
          foreach ($parameters as $key => $value) {
            if (db_field_exists($table_name, $key)) {
              $operator = '=';
              // Translate string to NULL if "null" was passed as a search condition
              if ($value == "null") {
                $value = NULL;
              }
              // Translate simple comma separated list to array and SQL "IN" condition
              elseif (strchr($value, ",")) {
                $value = explode(",", $value);
                $operator = 'IN';
              }
              $db_and->condition($key, $value, $operator);
              $db_and_trigger = TRUE;
            }
          }
        }

        // Build and execute query
        try {

          $query = db_select($table_name, 't')
            ->fields('t');

          // Conditions from parameters
          if ($db_and_trigger) {
            $query->condition($db_and);
          }

          // Sort data from table event by date
          if ($table_name == 'event') {
            $query->orderBy('date');
          }

          // Row limit
          if ($limit > 0) {
            $query->range(0, $limit);
          }

          $result = $query->execute()
            ->fetchAll();

          // Return data
          if ($this->debug) {
            $data['data'] = $result;
          }
          else {
            $data = $result;
          }
        } catch (Exception $e) {
          watchdog_exception(self::DRUPAL_WATCHDOG_TYPE, $e);
        }
      }

      /**
       * Switch back to default database connection.
       */
      $this->database->deactivate();
    }

    return $data;
  }

  /**
   * Generic database update
   *
   * @param $table_name
   * @param $json_data
   *
   * @return int|bool The updated row ID on success, FALSE otherwise
   */
  public function update($table_name, $json_data) {
    // Escape if unsafe table request
    if (!self::safe_name($table_name)) {
      return FALSE;
    }

    $object = json_decode($json_data, TRUE)['data'];

    // If ID was not set, pass NULL to db_merge
    $id = array_key_exists('id', $object) ? $object['id'] : NULL;

    // get attributes from JSON
    $attributes = $object['attributes'];

    // parse foreign keys from JSON
    if ($relationships = array_key_exists('relationships',
      $object) ? $object['relationships'] : FALSE) {
      foreach ($relationships as $key => $value) {
        $attributes[$key] = $value['data']['id'];
      }
    }

    /**
     * Switch to contractionDB database connection.
     */
    $this->database->activate();

    // Try db_merge for sanitized data
    if (db_table_exists($table_name)) {
      if ($primary_key = self::$primary_keys[$table_name]) {
        $validated_attributes = [];
        foreach ($attributes as $key => $value) {
          if (db_field_exists($table_name, $key) && isset($value)) {
            $validated_attributes[$key] = $value;
          }
        }

        try {
          db_merge($table_name)
            ->key([$primary_key => $id])
            ->fields($validated_attributes)
            ->execute();

          // Get the new ID if it was not an update
          if (!$id) {
            $id = Database::getConnection()->lastInsertId();
          }

          /**
           * Switch back to default database connection.
           */
          $this->database->deactivate();
          return $id;

        } catch (Exception $e) {
          watchdog_exception(self::DRUPAL_WATCHDOG_TYPE, $e);
        }

      }
    }

    /**
     * Switch back to default database connection.
     */
    $this->database->deactivate();

    return FALSE;
  }

  /**
   * @param $table_name
   * @param $id
   *
   * @return bool TRUE if successfully deleted.
   */
  public function delete($table_name, $id) {
    // Escape if unsafe table request
    if (!self::safe_name($table_name)) {
      return FALSE;
    }

    /**
     * Switch to contractionDB database connection.
     */
    $this->database->activate();

    if (db_table_exists($table_name)) {
      try {
        db_delete($table_name)
          ->condition(self::$primary_keys[$table_name], $id)
          ->execute();

        /**
         * Switch back to default database connection.
         */
        $this->database->deactivate();
        return TRUE;
      } catch (Exception $e) {
        watchdog_exception(self::DRUPAL_WATCHDOG_TYPE, $e);
      }
    }

    /**
     * Switch back to default database connection.
     */
    $this->database->deactivate();
    return FALSE;
  }

}