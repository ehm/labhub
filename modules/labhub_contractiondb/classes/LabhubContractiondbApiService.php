<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class LabhubContractiondbApiService
 *
 * Controller class for API service. Dispatches requests, parses and filters
 * HTTP POST input and URI parameters.
 *
 * Control flow is as follows:
 * - Construct class instance (parses request)
 * - Authenticate and authorize request
 * - Dispatch according to URI
 * - Preprocess arguments and parameters according to route/endpoint
 * - Pass query to @see \DatabaseHandlerInterface instance to process
 * communication with the database backbone
 */
class LabhubContractiondbApiService {

  const TYPE_SERVICE_JSON_API = 'LabHub JSON API Service';

  const CREDENTIALS_CONTEXT_CONTRACTIONDB = 'ContractionDB API Server';

  private
    $headers,
    $method,
    $parameters,
    $post_data,
    $response_code = 200;

  /**
   * @var array JSON-API object, see:
   *   http://jsonapi.org/format/#document-structure
   *  Allowed keys are:
   *  - data
   *  - error
   *  - meta
   *  - jsonapi
   *  - links
   *  - included
   */
  private $response_data = [
    'data' => [],
    'error' => [],
    'meta' => [
      'service' => 'ContractionDB API',
    ],
    'links' => [],
    'jsonapi' => [
      'version' => '1.0',
    ],
  ];

  /**
   * LabhubContractiondbApiService constructor.
   */
  public function __construct() {
    /**
     * Parse HTTP request information from server.
     */
    $this->headers = apache_request_headers();
    $this->method = LabhubHttpResources::$methods[$_SERVER['REQUEST_METHOD']];
    $this->post_data = file_get_contents('php://input');

    /**
     * Parse HTTP GET parameters
     */
    $this->parameters = [];
    $params = array_keys($_GET);

    /**
     * Enforce safe string input for HTTP GET parameters
     */
    foreach ($params as $key) {
      if ($param = filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRING)) {
        $this->parameters[$key] = $param;
      }
    }

    /**
     * Enforce integer input for "limit" parameter
     */
    if (array_key_exists('limit', $_GET)) {
      $this->parameters['limit'] = filter_input(INPUT_GET, 'limit',
        FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Include external library for JSON Web Token processing
     */
    labhub_include_lib_jwt();
  }

  /**
   * Dispatch request among existing API endpoints.
   *
   * Control flow:
   * - Autheticate and authorize request
   * - Dispatch request to appropriate handler
   * - Trigger HTTP response, if not interrupted before
   */
  public function dispatch() {

    $this->authorize();

    // Attach request URL to response data
    $this->response_data['links']['self'] = url(request_uri(),
      ['absolute' => TRUE, 'https' => TRUE]);

    // parse route from request URI
    $route = str_replace('/' . LABHUB_CDB_API_URL_DEFAULT_ENDPOINT . '/', '',
      request_uri());

    // remove parameters from route
    if (strchr($route, "?")) {
      $route = explode("?", $route, 2)[0];
    }
    $args = explode('/', $route);

    /**
     * Dispatch based on first URI section
     */
    switch ($args[0]) {
      /**
       * Endpoint {base_url}/tasks
       */
      case 'tasks':
        $this->routeTasks($args);
        break;
      /**
       * Endpoint {base_url}/experiment/{id]
       */
      case 'experiment':
        $this->routeExperiment($args);
        break;
      /**
       * Endpoint {base_url}/measurement/{id}
       */
      case 'measurement':
        $this->routeMeasurement($args);
        break;
      /**
       * Endpoint {base_url}/generic/{table}
       */
      case 'generic':
        $this->routeGeneric($args);
        break;
      /**
       * Default endpoint (i.e. no endpoint matched): send HTTP code "400 Bad Request"
       */
      default:
        $this->deny(400);
    }

    /**
     * After handling the request, send response.
     */
    $this->respond();
  }

  /**
   * ToDo: doc
   */
  private function authorize() {

    /**
     * At the moment, only authentication mechanisms based on "Authorization"
     * HTTP header are support. If header is missing, access can be denied.
     */
    if (!array_key_exists('Authorization', $this->headers)) {
      $this->deny();
    }

    /**
     * Parse "Authorization" header information.
     */
    $auth = $this->headers['Authorization'];

    /**
     * Option 1: HTTP Basic Authorization
     */
    if (strchr($auth, "Basic ")) {
      $userpass = str_replace("Basic ", "", $auth);
      $userpass = base64_decode($userpass);

      if (!self::validateBasic($userpass)) {
        $this->deny();
      }
    }

    /**
     * Option 2: Json Web Token (JWT) Authorization
     */
    elseif (strchr($auth, "Bearer ")) {
      $token = str_replace("Bearer ", "", $auth);

      if (!self::validateJWT($token)) {
        $this->deny();
      }
    }

    /**
     * Anything else, deny access.
     */
    else {
      $this->deny();
    }
  }

  /**
   * Validate HTTP Basic Auth.
   *
   * Build a @see \LabhubCredentials instance based on given credentials and
   * validate against whitelist.
   *
   * @see \LabhubCredentialsRepository handles validation.
   *
   * @param $userpass
   *
   * @return bool TRUE if given credentials matched in whitelist.
   */
  private static function validateBasic($userpass) {

    $credentials = new LabhubCredentials();
    $credentials->setType(LabhubCredentials::TYPE_USERPASS);
    $credentials->setSecret($userpass);
    $context = self::CREDENTIALS_CONTEXT_CONTRACTIONDB;
    $credentials->setContext($context);

    return LabhubCredentialsRepository::validate($credentials, $context);
  }


  /**
   * Validate JSON Web Token.
   *
   * @param $token
   *
   * @return bool TRUE if token is valid.
   */
  private static function validateJWT($token) {

    /**
     * Fetch whitelist of authorized JWT-type credentials.
     */
    $credentials = LabhubCredentialsRepository::getAllTypeByContext(
      LabhubCredentials::TYPE_JWT,
      self::CREDENTIALS_CONTEXT_CONTRACTIONDB
    );

    /**
     * Test given token against whitelist
     */
    foreach ($credentials as $credential) {
      /**
       * @var \LabhubCredentials $credential
       */

      try {
        /**
         * Try to decode the given token with this instance of whitelisted JWT
         */
        $algorithm = $credential->getAlgorithm();
        $secret = labhub_ssl_decrypt($credential->getSecret());
        $payload = Firebase\JWT\JWT::decode($token, $secret, [$algorithm]);
      } catch (\Firebase\JWT\SignatureInvalidException $e) {
        // ignore this exception, signals invalid token
        // if not ignored, a single failed validation breaks validation loop
      } catch (Exception $e) {
        // handle any other Exception
        watchdog_exception('Labhub ContractionDB API Server', $e);
      }

      /**
       * If the token was successfully decoded, check if parameters "Issuer"
       * and "Subject" match whitelist entry
       */
      if (isset($payload)) {

        $test = clone $credential;
        $test->setIssuer($payload->iss);
        $test->setSubject($payload->sub);

        if ($credential->equals($test)) {
          // validate token, if parameters match
          return TRUE;
        }
      }
    }

    // All tests failed, deny token.
    return FALSE;
  }

  /**
   * Send HTTP response with the instance's collected variable values as return
   * transfer data.
   */
  private function respond() {
    /**
     * Set HTTP response code.
     */
    http_response_code($this->response_code);

    /**
     * Return JSON-API object, see:
     * - https://www.iana.org/assignments/media-types/application/vnd.api+json
     * - https://tools.ietf.org/html/rfc7159
     */
    header('Content-Type: application/vnd.api+json');

    /**
     * Only one of "data" or "error" objects may be present in JSON response
     */
    if (!empty($this->response_data['error'])) {
      unset($this->response_data['data']);
    }
    else {
      unset($this->response_data['error']);
    }

    /**
     * Send raw JSON output instead of Drupal rendered webpage.
     */
    drupal_json_output($this->response_data);
    exit();
  }

  /**
   * Send HTTP response header with error code. No response body is sent.
   *
   * @param int $code
   */
  private function deny($code = 401) {
    http_response_code($code);
    drupal_send_headers();
    exit();
  }

  /**
   * ToDo: doc
   *
   * @param array $args Request URI sections
   */
  private function routeGeneric($args) {

    $db_id = variable_get(LABHUB_CDB_VAR_DEFAULT_DATABASE);
    $db = LabhubDatabaseRepository::getById($db_id);
    $handler = new GenericDatabaseJsonHandler($db);

    switch ($this->method) {
      case LabhubHttpResources::METHOD_HTTP_POST:
      case LabhubHttpResources::METHOD_HTTP_PUT:
        if (!isset($args[1])) {
          // no table specified, return "400 Bad Request"
          $this->deny(400);
        }
        else {
          if (json_decode($this->post_data)) {
            // Continue if the are no JSON decode errors, i.e.

            $table_name = $args[1];
            $object = $this->post_data;
            $id = $handler->update($table_name, $object);

            if ($id) {
              if (array_key_exists('id', json_decode($object, TRUE)['data'])) {
                // If object was update, send "200 OK"
                $this->response_code = 200;
              }
              else {
                // If object was created, send "201 Created"
                $this->response_code = 201;
              }
              // Return object
              $object = json_decode($object, TRUE);
              $object['data']['id'] = $id;
              $this->response_data['data'] = json_encode($object['data']);
            }
            else {
              // Anything fails, send "400 Bad Request"
              $this->deny(400);
            }
          }
          else {
            // Anything fails, send "400 Bad Request"
            $this->deny(400);
          }
        }
        break;
      case LabhubHttpResources::METHOD_HTTP_DELETE:
        if (isset($args[1]) && isset($args[2])) {
          if ($handler->delete($args[1], $args[2])) {
            $this->response_code = 204; // If deleted, send "204 No Content"
          }
          else {
            $this->response_code = 400; // Bad request
          }
        }
        else {
          // Anything fails, send "400 Bad Request"
          $this->deny(400);
        }
        break;
      case LabhubHttpResources::METHOD_HTTP_GET:
        // is default
      default:
        if (!isset($args[1])) {
          // no table specified
          $this->response_data['meta']['message'] = "Generic Endpoint";
        }
        else {
          $handler->setDebug(FALSE);
          $limit = array_key_exists('limit',
            $this->parameters) ? $this->parameters['limit'] : 10;
          $parameters = $this->parameters;
          $this->response_data['meta']['parameters'] = $this->parameters;
          $this->response_data['data'] = $handler->selectAll($args[1], $limit,
            $parameters);
        }
    }
  }

  /**
   * ToDo: doc
   *
   * @param array $args Request URI sections
   */
  private function routeExperiment($args) {

    $db_id = variable_get(LABHUB_CDB_VAR_DEFAULT_DATABASE);
    $db = LabhubDatabaseRepository::getById($db_id);
    $handler = new EhmDatabaseHandler($db);

    switch ($this->method) {
      case LabhubHttpResources::METHOD_HTTP_GET:
      default:
        if (isset($args[1])) {
          $data = $handler->selectExperiment($args[1]);
          $this->response_data = array_merge($this->response_data, $data);
        }
        break;
    }
  }

  /**
   * ToDo: doc
   *
   * @param array $args Request URI sections
   */
  private function routeMeasurement($args) {
    $db_id = variable_get(LABHUB_CDB_VAR_DEFAULT_DATABASE);
    $db = LabhubDatabaseRepository::getById($db_id);
    $handler = new EhmDatabaseHandler($db);

    switch ($this->method) {
      case LabhubHttpResources::METHOD_HTTP_GET:
      default:
        if (isset($args[1])) {
          $data = $handler->selectMeasurement($args[1]);
          $this->response_data = array_merge($this->response_data, $data);
        }
        break;
    }

  }

  /**
   * ToDo: doc
   *
   * @param array $args Request URI sections
   */
  private function routeTasks($args) {
    $db_id = variable_get(LABHUB_CDB_VAR_DEFAULT_DATABASE);
    $db = LabhubDatabaseRepository::getById($db_id);
    $handler = new EhmDatabaseHandler($db);

    switch ($this->method) {
      case LabhubHttpResources::METHOD_HTTP_GET:
      default:
        /**
         * Route is {base_route}/tasks?{parameters}
         * If request URI contains any more sections, send "400 Bad Request"
         */
        if (count($args) > 1) {
          $this->deny(400);
        }

        /**
         * Acceptable parameters: person [id], (optional: begin [date], end [date])
         */
        if (!array_key_exists('person', $this->parameters)) {
          $this->deny(400);
        }
        /**
         * If "begin" or "end" parameters exist, test if they are well formatted
         * date strings, i.e. matching "YYYY-MM-DD" format. Malformatted parameters
         * trigger an error response.
         */
        if (array_key_exists('begin', $this->parameters)) {
          if (!$this->is_date($this->parameters['begin'])) {
            $this->deny(400);
          }
        }
        if (array_key_exists('end', $this->parameters)) {
          if (!$this->is_date($this->parameters['end'])) {
            $this->deny(400);
          }
        }
        LabhubHttpResources::$http_status_codes;
        $data = $handler->selectTasks($this->parameters);
      if (!count($data)) {
        $this->response_code = 204;
      }
        $this->response_data = array_merge($this->response_data, $data);
        break;
    }
  }

  private function is_date($string) {
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",
      $string)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
