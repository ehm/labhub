<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


/**
 * Class EhmDatabaseHandler
 *
 * Implements specialised routes for contractionDB API server, i.e. API calls
 * that surpass generic access to MySQL database tables.
 */
class EhmDatabaseHandler implements DatabaseHandlerInterface {

  use DatabaseHandlerTrait;


  /**
   * EhmDatabaseHandler constructor.
   *
   * Internal database service connector is set to a default value which is
   * configured in the LabHub ContractionDB module configuration back end.
   *
   * @param \LabhubDatabaseInterface|NULL $database
   * @param bool $debug
   */
  public function __construct(
    LabhubDatabaseInterface $database = NULL,
    $debug = FALSE
  ) {
    /**
     * Set default database connection for the instance.
     */
    $db_id = variable_get(LABHUB_CDB_VAR_DEFAULT_DATABASE);
    $this->database = LabhubDatabaseRepository::getById($db_id);

    /**
     * Enable or disable debug output.
     */
    $this->debug = $debug;
  }

  /**
   * Handle requests for {base_url}/experiment endpoint.
   *
   * @param string $id The identifier of the requested experiment.
   *
   * @return mixed
   */
  public function selectExperiment($id) {

    /**
     * Switch to contractionDB database connection.
     */
    $this->database->activate();

    try {

      /**
       * Select the experiment data from DB.
       */
      $exp_event = db_select('event', 'ev')
        ->fields('ev')
        ->condition('ev.idEvent', $id)
        ->range(0, 1)
        ->execute()
        ->fetch();

      if (!empty($exp_event)) {
        /**
         * Append data to result set.
         */
        $return['data'] = GenericDatabaseJsonHandler::resultToJsonApiObject("event",
          $exp_event);

        /**
         * Add attributes from experiment table
         */
        $experiment = db_select('experiment', 't')
          ->fields('t')
          ->condition('idEvent', $exp_event->idEvent)
          ->execute()
          ->fetch();
        if (!empty($experiment)) {
          /**
           * Person in charge of experiment
           */
          $return['data']['relationships']['idPerson'] = [
            'data' => [
              'type' => 'person',
              'id' => $experiment->idPerson,
            ],
          ];
          /**
           * Experiment name
           */
          $return['data']['attributes']['name'] = $experiment->name;
        }
      }

      /**
       * Fetch related labware.
       */
      $labware = db_select('labware', 't')
        ->fields('t')
        ->condition('t.idLabware', $exp_event->idLabware)
        ->range(0, 1)
        ->execute()
        ->fetch();

      if (!empty($labware)) {
        /**
         * Append data to result set.
         */
        $return['included'][] = GenericDatabaseJsonHandler::resultToJsonApiObject("labware",
          $labware);
      }

      /**
       * Fetch related labwaretype.
       */
      $labwaretype = db_select('labwaretype', 't')
        ->fields('t')
        ->condition('t.idLabwareType', $labware->idLabwareType)
        ->range(0, 1)
        ->execute()
        ->fetch();

      if (!empty($labwaretype)) {
        /**
         * Append data to result set.
         */
        $return['included'][] = GenericDatabaseJsonHandler::resultToJsonApiObject("labwaretype",
          $labwaretype);
      }

      /**
       * Prepare condition for wells related to the experiment.
       */
      $cond = db_and();
      $cond->condition('t.idParent', $labware->idLabware);
      $cond->condition('t.idLabware', $labware->idLabware, "!=");

      /**
       * Fetch wells related to experiment.
       */
      $wells = db_select('labware', 't')
        ->fields('t')
        ->condition($cond)
        ->execute()
        ->fetchAll();

      if (!empty($wells)) {
        /**
         * Append data to result set.
         */
        foreach ($wells as $row) {
          $well = GenericDatabaseJsonHandler::resultToJsonApiObject("labware",
            $row);
          // override type "labware"
          $well['type'] = 'well';
          $return['included'][] = $well;
        }
      }

      /**
       * Prepare condition for events related to experiment.
       */
      $cond = db_and();
      $cond->condition('t.idLabware', $labware->idLabware);
      $cond->condition('t.idParent', $exp_event->idEvent);

      /**
       * Fetch events related to experiment.
       */
      $events = db_select('event', 't')
        ->fields('t')
        ->condition($cond)
        ->execute()
        ->fetchAll();

      if (!empty($events)) {
        /**
         * Append data to result set.
         */
        foreach ($events as $event) {
          $event = GenericDatabaseJsonHandler::resultToJsonApiObject("event",
            $event);
          $return['included'][] = $event;
        }
      }

      /**
       * Fetch cell lines
       */
      $query = "SELECT * FROM celltype WHERE idCellType IN (
                  SELECT idCellType FROM cells WHERE idCells IN (
                    SELECT idCells FROM solution_cells WHERE idSolution = (
                      SELECT idSolution FROM eventtype WHERE idEventType = (
                        SELECT idEventType FROM event WHERE idEvent = (
                          SELECT MIN(idEvent) FROM event WHERE idParent = " . $id . "
                          AND idEventType IN (
                            SELECT idEventType FROM eventtype WHERE idSolution is not null
                            )
                          )
                        )
                      )
                    )
                  )";
      $stemcells = db_query($query)->fetchAll();

      if (count($stemcells)) {
        foreach ($stemcells as $stemcell) {
          $cells = GenericDatabaseJsonHandler::resultToJsonApiObject('celltype',
            $stemcell);
          $return['included'][] = $cells;
        }
      }

    } catch (Exception $e) {
      watchdog_exception(self::DRUPAL_WATCHDOG_TYPE, $e);
      $return['data'] = [];
    }

    /**
     * Switch back to default database connection.
     */
    $this->database->deactivate();

    return $return;
  }


  /**
   * Handle requests for {base_url}/measurement/{id} endpoint.
   *
   * @param string $id The identifier of the requested experiment.
   *
   * @return mixed
   */
  public function selectMeasurement($id) {

    /**
     * Switch to contractionDB database connection.
     */
    $this->database->activate();

    try {
      $measurement = db_select('measurement', 't')
        ->fields('t')
        ->condition('t.idEvent', $id)
        ->range(0, 1)
        ->execute()
        ->fetch();

      $return = [];
      if (!empty($measurement)) {
        $return['data'] = GenericDatabaseJsonHandler::resultToJsonApiObject("measurement",
          $measurement);
      }


      $event = db_select('event', 't')
        ->fields('t')
        ->condition('t.idEvent', $id)
        ->range(0, 1)
        ->execute()
        ->fetch();
      if (!empty($event)) {
        $return['included'][] = GenericDatabaseJsonHandler::resultToJsonApiObject('event',
          $event);
      }

      $person = db_select('person', 't')
        ->fields('t')
        ->condition('t.idPerson', $measurement->idPerson)
        ->range(0, 1)
        ->execute()
        ->fetch();
      if (!empty($person)) {
        $return['included'][] = GenericDatabaseJsonHandler::resultToJsonApiObject('person',
          $person);
      }

      $children_condition = db_and()
        ->condition('t.idParent', $measurement->idEvent)
        ->condition('t.idEvent', $measurement->idEvent, "!=")
        ->condition('t.idLabware', $event->idLabware, "!=");

      $children_query = db_select('event', 't')
        ->fields('t', ['idEvent', 'idLabware'])
        ->distinct()
        ->condition($children_condition);

      $children_events = $children_query->execute()->fetchAll();
      $wells = [];
      if (!empty($children_events)) {
        foreach ($children_events as $children_event) {
          $wells[$children_event->idEvent] = $children_event->idLabware;
        }
      }
      $return['data']['attributes']['wells'] = $wells;
      $children_event_ids = array_keys($wells);

      $children_count = $children_query->countQuery()->execute()->fetchField();
      $return['data']['attributes']['wellsMeasured'] = $children_count;

      foreach ($wells as $idEvent => $idLabware) {
        $stat_query = 'SELECT COUNT(idPeak) as count_peaks, 
                        AVG((100*height)/base) as foc_mean, 
                        STD((100*height)/base) as foc_stddev 
                        FROM peak WHERE idEvent = ' . $idEvent;
        $statistics = db_query($stat_query)->fetchAssoc();

        if (!is_null($statistics['foc_mean'])) {
          $return['data']['attributes']['statistics'][$idLabware] = $statistics;
        }
      }
    } catch (Exception $e) {
      watchdog_exception(self::DRUPAL_WATCHDOG_TYPE, $e);
      $return['data'] = [];
    }

    /**
     * Switch back to default database connection.
     */
    $this->database->deactivate();

    return $return;
  }

  public function selectTasks($parameters) {

    $person_id = $parameters['person'];

    if (array_key_exists('begin', $parameters)) {
      $begin = $parameters['begin'];
    }
    if (array_key_exists('end', $parameters)) {
      $end = $parameters['end'];
    }

    $return = [];

    /**
     * Switch to contractionDB database connection.
     */
    $this->database->activate();

    try {
      $experiments = db_select('experiment', 't')
        ->fields('t', ['idEvent', 'name'])
        ->condition('t.idPerson', $person_id)
        ->execute()
        ->fetchAllAssoc('idEvent');

      $measurements = db_select('measurement', 't')
        ->fields('t', ['idEvent', 'name'])
        ->condition('t.idPerson', $person_id)
        ->execute()
        ->fetchAllAssoc('idEvent');

      $events = array_merge(array_keys($experiments),
        array_keys($measurements));

      /**
       * ToDo: peakanalysis table?
       */

      $condition = db_and();
      if (count($events)) {
        $condition->condition('t.idEvent', $events, 'IN');

        /**
         * Build condition for event dates based on passed parameters
         */
        if (isset($end)) {
          $condition->condition('t.date', $begin, '>=');
          $condition->condition('t.date', $end, '<=');
        }
        elseif (isset($begin)) {
          // If only begin is set, define end as the next day:
          $end = strtotime("+1 days", strtotime($begin));
          $end = date("Y-m-d", $end);
          $condition->condition('t.date', $begin, '>=');
          $condition->condition('t.date', $end, '<');
        }

        $events = db_select('event', 't')
          ->fields('t')
          ->condition($condition)
          ->orderBy('t.date')
          ->execute()
          ->fetchAll();

        foreach ($events as $event) {
          if (array_key_exists($event->idEvent, $experiments)) {
            $type = 'experiment';
            $name = $experiments[$event->idEvent]->name;
          }
          elseif (array_key_exists($event->idEvent, $measurements)) {
            $type = 'measurement';
            $name = $measurements[$event->idEvent]->name;
          }
          else {
            continue;
          }
          $return['data'][] = [
            /**
             * ToDo: which attributes to collect and return!?
             */
            'type' => 'task',
            'id' => $event->idEvent,
            'attributes' => [
              'type' => $type,
              'name' => $name,
              'date' => $event->date,
            ],
            'relationships' => [
              'idPerson' => [
                'data' => [
                  'type' => 'person',
                  'id' => $person_id,
                ],
              ],
            ],
          ];
        }
      }
    } catch (Exception $exception) {
      watchdog_exception(self::DRUPAL_WATCHDOG_TYPE, $exception);
    }

    $this->database->deactivate();

    return $return;
  }

}