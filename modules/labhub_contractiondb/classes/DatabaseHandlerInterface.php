<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

interface DatabaseHandlerInterface {

  const DRUPAL_WATCHDOG_TYPE = "ContractionDB Database Handler";

  /**
   * @return bool
   */
  public function isDebug();

  /**
   * @param bool $debug
   */
  public function setDebug($debug);

  /**
   * @return \LabhubDatabaseInterface
   */
  public function getDatabase();
}