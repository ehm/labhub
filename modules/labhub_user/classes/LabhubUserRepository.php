<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class LabhubUserRepository {

  private static $table_name = 'labhub_user';

  public static function save(LabhubUser $user) {
    try {
      db_merge(self::$table_name)
        ->key(['uid' => $user->getUid()])
        ->fields([
          'seek_id' => $user->getSeekId(),
          'ehm_id' => $user->getEhmId(),
        ])
        ->execute();
      return TRUE;
    } catch (Exception $e) {
      watchdog_exception("LabHub User Repository", $e);
    }
    return FALSE;
  }

  public static function load($uid) {
    $user = new LabhubUser();
    $user->setUid($uid);
    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t')
        ->condition('t.uid', $uid)
        ->range(0, 1)
        ->execute()
        ->fetch();
      if ($result) {
        $user->setSeekId($result->seek_id);
        $user->setEhmId($result->ehm_id);
      }
    } catch (Exception $e) {
      watchdog_exception("LabHub User Repository", $e);
    }
    return $user;
  }

  public static function loadByEhmId($ehm_id) {
    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t')
        ->condition('t.ehm_id', $ehm_id)
        ->range(0, 1)
        ->execute()
        ->fetch();
      if ($result) {
        return LabhubUser::load($result->uid);
      }
    } catch (Exception $e) {
      watchdog_exception("LabHub User Repository", $e);
    }
    return new LabhubUser();
  }
}