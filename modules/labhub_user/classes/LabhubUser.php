<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubUser {

  private static $seek_id_options = [];

  private static $ehm_id_options = [];

  private $uid;

  private $seek_id;

  private $ehm_id;

  private $drupal_username;

  private $firstname;

  private $lastname;

  private $orcid;

  private $rspace_api_key;

  /**
   * @return mixed
   */
  public function getOrcid() {
    return $this->orcid;
  }

  /**
   * @param mixed $orcid
   */
  public function setOrcid($orcid) {
    $this->orcid = $orcid;
  }

  /**
   * @return mixed
   */
  public function getRspaceApiKey() {
    return $this->rspace_api_key;
  }

  /**
   * @param mixed $rspace_api_key
   */
  public function setRspaceApiKey($rspace_api_key) {
    $this->rspace_api_key = $rspace_api_key;
  }

  public static function configFormTable() {
    $users = db_select('users', 't')
      ->fields('t', ['uid'])
      ->condition('t.uid', 0, '>')
      ->execute()
      ->fetchAll();

    $output = self::configFormTableHeader();

    foreach ($users as $user) {
      $form = drupal_get_form('labhub_user_inline_form', $user->uid);
      $output .= drupal_render($form);
    }

    $output .= '</table>';

    _labhub_user_include_css();

    return $output;
  }

  /**
   * @return mixed
   */
  public function getFirstname() {
    return $this->firstname;
  }

  /**
   * @param mixed $firstname
   */
  public function setFirstname($firstname) {
    $this->firstname = $firstname;
  }

  /**
   * @return mixed
   */
  public function getLastname() {
    return $this->lastname;
  }

  /**
   * @param mixed $lastname
   */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  private static function configFormTableHeader() {
    return '<table class="labhub_user_config"><tr><th>Username</th><th>EHM Database ID</th><th>SEEK ID</th><th>RSpace Key</th></tr>';
  }

  public static function load($uid) {
    // uid, seek_id and ehm_id from Repository class
    $labhubuser = LabhubUserRepository::load($uid);

    // Drupal username
    $user = user_load($uid);
    $labhubuser->setDrupalUsername($user->name);

    // Lastname
    $field = field_get_items('user', $user, LABHUB_FIELD_USER_LASTNAME);
    $labhubuser->setLastname($field[0]['value']);

    // Firstname
    $field = field_get_items('user', $user, LABHUB_FIELD_USER_FIRSTNAME);
    $labhubuser->setFirstname($field[0]['value']);

    // ORCID field
    $field = field_get_items('user', $user, LABHUB_FIELD_USER_ORCID);
    $labhubuser->setOrcid($field[0]['value']);

    // RSpace API Key field
    $field = field_get_items('user', $user,
      LABHUB_FIELD_USER_RSPACE_API_KEY);
    $labhubuser->setRspaceApiKey($field[0]['value']);

    return $labhubuser;
  }

  public static function loadByEhmId($ehm_id) {
    return LabhubUserRepository::loadByEhmId($ehm_id);
  }

  /**
   * @return mixed
   */
  public function getDrupalUsername() {
    return $this->drupal_username;
  }

  /**
   * @param mixed $drupal_username
   */
  public function setDrupalUsername($drupal_username) {
    $this->drupal_username = $drupal_username;
  }

  public function save() {
    LabhubUserRepository::save($this);
  }

  /**
   * @return mixed
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * @param mixed $uid
   */
  public function setUid($uid) {
    $this->uid = $uid;
  }

  /**
   * @return mixed
   */
  public function getSeekId() {
    return $this->seek_id;
  }

  /**
   * @param mixed $seek_id
   */
  public function setSeekId($seek_id) {
    $this->seek_id = $seek_id;
  }

  /**
   * @return mixed
   */
  public function getEhmId() {
    return $this->ehm_id;
  }

  /**
   * @param mixed $ehm_id
   */
  public function setEhmId($ehm_id) {
    $this->ehm_id = $ehm_id;
  }

  public function configFormTableRow() {

    if (!count(self::$ehm_id_options)) {
      self::$ehm_id_options[NULL] = "--Select--";

      // clear Person cache to avoid missings
      PersonRepository::flush();
      $persons = PersonRepository::getAll();
      /** @var \Person $person */
      foreach ($persons as $person) {
        self::$ehm_id_options[$person->getId()] = $person->displayName();
      }
    }

    if (!count(self::$seek_id_options)) {

      self::$seek_id_options[NULL] = '--Select--';

      $seek_id = variable_get('labhub_user_seek_service_id');
      $service = LabhubServiceRepository::getById($seek_id);
      $api = new LabhubSeekApiClient($service);
      $creds = new LabhubCredentials();
      $api->setCredentials($creds);
      $result = $api->selectPeople();
      $persons = json_decode($result, TRUE)['data'];
      if (is_array($persons)) {
        foreach ($persons as $person) {
          self::$seek_id_options[$person['id']] = $person['attributes']['title'];
        }
      }

    }

    $form = [];

    $form['uid'] = [
      '#type' => 'hidden',
      '#value' => $this->uid,
    ];

    $form['drupal_username'] = [
      '#markup' => '<tr><td>' . $this->drupal_username . '</td>',
    ];

    // ToDo: use ajax callback to store value in DB
    $ajax_wrapper = 'labhub_user_ehm_select_' . $this->uid;
    $form['ehm_id'] = [
      '#type' => 'select',
      '#options' => self::$ehm_id_options,
      '#prefix' => '<td><div class="' . $ajax_wrapper . '">',
      '#suffix' => '</div></td>',
      '#default_value' => $this->getEhmId(),
      '#ajax' => [
        'callback' => 'labhub_user_list_select_ajax_callback',
        'wrapper' => $ajax_wrapper,
      ],
    ];
    // ToDo: use ajax callback to store value in DB
    $ajax_wrapper = 'labhub_user_seek_select_' . $this->uid;
    $form['seek_id'] = [
      '#type' => 'select',
      '#options' => self::$seek_id_options,
      '#prefix' => '<td><div class="' . $ajax_wrapper . '">',
      '#suffix' => '</div></td>',
      '#default_value' => $this->getSeekId(),
      '#ajax' => [
        'callback' => 'labhub_user_list_select_ajax_callback',
        'wrapper' => $ajax_wrapper,
      ],
    ];

    if (!empty($this->rspace_api_key)) {
      $icon = '<span class="glyphicon glyphicon-ok-sign" data-toggle="tooltip" title="RSpace API key has been set for this user"></span>';
    }
    else {
      $icon = '<span class="glyphicon glyphicon-remove-sign" data-toggle="tooltip" title="RSpace API key has not been set for this user"></span>';
    }
    $form['rspace_key'] = [
      '#markup' => '<td>' . $icon . '</td>',
    ];

    $form['row_end'] = [
      '#markup' => '</tr>',
    ];

    return $form;
  }
}