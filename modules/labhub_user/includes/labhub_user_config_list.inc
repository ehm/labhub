<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_user_config_list() {

  $output = LabhubUser::configFormTable();

  return $output;
}

function labhub_user_inline_form($form, $form_state, $uid) {
  $labhub_user = LabhubUser::load($uid);
  $form = $labhub_user->configFormTableRow();

  return $form;
}

function labhub_user_list_select_ajax_callback($form, &$form_state) {
  $uid = $form_state['input']['uid'];
  $user = new LabhubUser();
  $user->setUid($uid);
  $user->setEhmId($form_state['input']['ehm_id']);
  $user->setSeekId($form_state['input']['seek_id']);
  $user->save();
}