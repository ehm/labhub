<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_user_config($form, &$form_state) {

  $form = [];

  $options_seek = [];
  $seek_services = LabhubServiceRepository::getAllByType(LabhubSeekService::TYPE_SERVICE_SEEK_CONNECTOR);
  foreach ($seek_services as $service) {
    $options_seek[$service->getId()] = $service->getServiceName();
  }

  $options_ehm = [];
  $ehm_services = LabhubServiceRepository::getAllByType(LabhubEhmService::TYPE_LABHUB_EHM_SERVICE);
  foreach ($ehm_services as $service) {
    $options_ehm[$service->getId()] = $service->getServiceName();
  }

  $form['fieldset_general'] = [
    '#type' => 'fieldset',
    '#title' => t('LabHub User Management Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['fieldset_general']['labhub_user_seek_service_id'] = [
    '#type' => 'select',
    '#title' => t("Select SEEK Service"),
    '#options' => $options_seek,
    '#default_value' => $var_seek = variable_get('labhub_user_seek_service_id',
      NULL),
    '#required' => TRUE,
  ];

  $form['fieldset_general']['labhub_user_ehm_service_id'] = [
    '#type' => 'select',
    '#title' => t("Select EHM Service"),
    '#options' => $options_ehm,
    '#default_value' => $var_ehm = variable_get('labhub_user_ehm_service_id',
      NULL),
    '#required' => TRUE,
  ];

  $form_state['redirect'] = LABHUB_USER_URL_CONFIG_DEFAULT;

  return system_settings_form($form);
}