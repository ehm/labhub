<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class LabhubMysqlDatabase
 */
class LabhubMysqlDatabase implements LabhubDatabaseInterface {

  use LabhubServiceTrait;
  use LabhubObjectTrait;

  const TYPE_DATABASE_MYSQL = 'MySQL Database Service';

  /**
   * @var \LabhubAuthentication
   */
  private $authentication;

  private static $driver = 'mysql';

  public function __construct() {
    $this->service_type = self::TYPE_DATABASE_MYSQL;
    $this->authentication = new LabhubAuthentication();
    $this->authentication->setType(LabhubAuthentication::AUTH_TYPE_BASIC);
  }

  public function getDatabaseName() {
    return $this->authentication->getResource();
  }

  public function setDatabaseName($database_name) {
    $this->authentication->setResource($database_name);
  }

  public function getUsername() {
    return $this->authentication->getUsername();
  }

  public function setUsername($username) {
    $this->authentication->setUsername($username);
  }

  public function getPassword() {
    return $this->authentication->getPassword();
  }

  public function setPassword($password) {
    $this->authentication->setPassword($password);
  }

  public function getServiceType() {
    return $this->service_type;
  }

  /**
   * @param \LabhubAuthentication $authentication
   */
  public function setAuthentication(LabhubAuthentication $authentication) {
    $this->authentication = $authentication;
  }

  public function activate() {
    $database = [
      'database' => $this->getDatabaseName(),
      'username' => $this->getUsername(),
      'password' => labhub_ssl_decrypt($this->getPassword()),
      'host' => $this->getHost(),
      'driver' => self::$driver,
    ];
    Database::addConnectionInfo($this->getServiceName(), 'default', $database);
    return db_set_active($this->getServiceName());
  }

  public function deactivate() {
    return db_set_active(); // without the paramater means set back to the default for the site
  }

  public function testConnection() {
    try {
      $this->activate();
      db_query('SELECT 1+1 FROM DUAL')
        ->fetchAssoc();
      $this->deactivate();
      return TRUE;
    } catch (Exception $e) {
      $this->deactivate();
      watchdog_exception('LabHub MySQL', $e);
    }
    $this->deactivate();
    return FALSE;
  }

  /**
   * @return array
   */
  public function getFormCreate() {
    $form = LabhubDatabaseForm::create($this);

    $form = LabhubServiceForm::addSubmitButton($form);

    return $form;
  }

  /**
   * @return array
   */
  public function getFormUpdate() {
    $form = LabhubDatabaseForm::update($this);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'save',
    ];

    return $form;
  }

  /**
   * ToDo: move to superclass?
   */
  public function save() {
    $id = LabhubDatabaseRepository::save($this);
    if ($this->isEmpty() && $id != self::EMPTY_ID) {
      $this->setId($id);
    }
    drupal_set_message('New ID: ' . $id);
  }

  public function getAuthentication() {
    return $this->authentication;
  }

  /**
   * @param \LabhubServiceInterface $service
   * @param \LabhubAuthentication $authentication
   * @param int $id
   *
   * @return \LabhubDatabaseInterface
   */
  public static function cast(
    LabhubServiceInterface $service,
    LabhubAuthentication $authentication,
    $id = self::EMPTY_ID
  ) {
    $database = new self();
    $database->setId($id);
    $database->setServiceName($service->getServiceName());
    $database->setHost($service->getHost());
    $database->setAuthentication($authentication);

    return $database;
  }
}