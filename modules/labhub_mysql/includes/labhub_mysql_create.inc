<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 24.04.18
 * Time: 18:57
 */

function labhub_mysql_create($form, &$form_state) {

  $service = new LabhubMysqlDatabase();
  return $service->getFormCreate();
}

/**
 * @param $form
 * @param $form_state
 */
function labhub_mysql_create_validate($form, &$form_state) {

  $values = $form_state['values'];

  $service = new LabhubMysqlDatabase();
  $service->setServiceName($values['service_name']);
  $service->setHost($values['host']);
  $service->setDatabaseName($values['database_name']);
  $service->setUsername($values['username']);
  $service->setPassword($values['password']);
  if (!$service->testConnection()) {
    form_set_error('',
      'Database connection could not be verified, please check');
  }

}

function labhub_mysql_create_submit($form, &$form_state) {

  $values = $form_state['values'];

  $service = new LabhubMysqlDatabase();
  $service->setServiceName($values['service_name']);
  $service->setHost($values['host']);
  $service->setDatabaseName($values['database_name']);
  $service->setUsername($values['username']);
  $service->setPassword($values['password']);
  $service->save();

  $form_state['redirect'] = LABHUB_MYSQL_URL_CONFIG_DEFAULT;
}