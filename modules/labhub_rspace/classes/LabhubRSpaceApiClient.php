<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class LabhubRSpaceApiClient
 */
class LabhubRSpaceApiClient {

  use LabhubApiClientTrait;

  /**
   * LabhubRSpaceApiClient constructor.
   *
   * @param \LabhubServiceInterface|NULL $service
   */
  public function __construct(\LabhubServiceInterface $service = NULL) {
    $default_service_id = variable_get(LABHUB_RSPACE_VAR_DEFAULT_SERVICE, 0);
    if ($default_service_id) {
      $default_service = LabhubServiceRepository::getById($default_service_id);
      $this->service = $default_service;
    }

    global $user;
    $lh_user = LabhubUser::load($user->uid);
    if ($key = $lh_user->getRspaceApiKey()) {
      $credentials = new LabhubCredentials();
      $credentials->setSecret($key);
      $credentials->setType(LabhubCredentials::TYPE_API_KEY);
      $this->setCredentials($credentials);
    }
  }

  /**
   * Test if combination of user's credentials and API service endpoint produce
   * a positive HTTP response.
   *
   * @return bool TRUE on successful connection to API endpoint.
   */
  public function testAuthentication() {
    if (!$this->getCredentials()) {
      drupal_set_message('No authentication credentials provided.');
    }
    else {

      /**
       * Query documents as a test.
       */
      $this->setRoute('documents?pageSize=1&orderBy=lastModified%20desc');
      $this->init();

      $this->addHeader('Accept: application/json');

      $this->execute();

      /**
       * Return TRUE if HTTP response code was 200.
       */
      if ($this->response_code == 200) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Push content of a @see \LabhubRSpaceExport instance to RSpace API endpoint.
   *
   * @param \LabhubRSpaceExport $tasks_export
   *
   * @return string|bool RSpace ID of created document success, FALSE otherwise.
   */
  public function createTasksDocument(LabhubRSpaceExport $tasks_export) {
    $this->setRoute('documents');
    $this->init();

    /**
     * Build RSpace API compatible HTTP POST body
     * https://community.researchspace.com/public/apiDocs
     */
    $post_body = [];
    $post_body['name'] = $tasks_export->displayName();
    $post_body['tags'] = 'LabHub Export';
    $post_body['fields'] = [
      [
        'content' => $tasks_export->getExportContent(),
      ],
    ];
    curl_setopt($this->handle, CURLOPT_POSTFIELDS, json_encode($post_body));

    /**
     * Set content type to JSON
     */
    $this->addHeader('Content-Type: application/json');

    $this->setMethod('POST');
    $result = $this->execute();
    $this->setData($result);

    if ($this->response_code == 201) {
      return json_decode($result, TRUE)['id'];
    }
    else {
      return FALSE;
    }
  }
}