<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class LabhubRSpaceService implements LabhubServiceInterface {

  use LabhubObjectTrait;
  use LabhubServiceTrait;

  const TYPE_SERVICE_RSPACE_CONNECTOR = 'LabHub RSpace Service Connector';

  public function __construct() {
    $this->service_type = self::TYPE_SERVICE_RSPACE_CONNECTOR;
  }

}