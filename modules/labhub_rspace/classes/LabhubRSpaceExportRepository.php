<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class LabhubRSpaceExportRepository
 */
abstract class LabhubRSpaceExportRepository implements LabhubObjectRepositoryInterface {

  use LabhubObjectRepositoryTrait;

  public static $table_name = 'labhub_rspace_ehm_user_export';

  private static $database_fields = [
    'id',
    'uid',
    'rspace_doc_id',
    'export_uuid',
    'export_timestamp',
  ];

  private static $class_name = LabhubRSpaceExport::class;

  public static function getAllByUid($uid) {

    try {
      $result = db_select(self::$table_name, 't')
        ->fields('t')
        ->condition('t.uid', $uid)
        ->execute()
        ->fetchAll();
    } catch (Exception $e) {
      watchdog_exception('LabHub', $e);
      drupal_set_message($e->getMessage(), 'error');

      return [];
    }

    return LabhubRSpaceExport::dbResultsToObjects($result);
  }
}