<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class LabhubRSpaceExport
 */
class LabhubRSpaceExport implements LabhubObjectInterface {

  use LabhubObjectTrait;

  /**
   * @var int Drupal user ID
   */
  private $uid = 0;

  /**
   * @var string RSpace document identifier
   */
  private $rspace_doc_id;

  /**
   * @var null IETF RFC4122 universally unique identifier for the export.
   */
  private $export_uuid;

  /**
   * @var int Timestamp of the export process.
   */
  private $export_timestamp = 0;

  /**
   * @var string $payload HTML content that will be exported to RSpace document.
   */
  private $payload;

  /**
   * @var string URL of RSpace document.
   */
  private $link;

  /**
   * LabhubRSpaceExport constructor.
   *
   * @param int $uid Drupal user ID
   * @param string $export_uuid Hexadecimal string of a RFC4122 UUID
   */
  public function __construct($uid = 0, $export_uuid = NULL) {
    if ($uid == 0) {
      global $user;
      $uid = $user->uid;
    }
    $this->uid = $uid;

    if (isset($export_uuid)) {
      $this->export_uuid = $export_uuid;
    }
    else {
      /**
       * Generate random UUID (IETF RFC4122 v4)
       */
      $this->export_uuid = UUID::generate(UUID::UUID_RANDOM, UUID::FMT_STRING);
    }
  }

  /**
   * @return string
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * @param string $payload HTML content that will be exported to RSpace
   *   document.
   */
  public function setPayload($payload) {
    $this->payload = $payload;
  }

  /**
   * Retrieve the (HTML) content that will be written to RSpace document.
   */
  public function getExportContent() {
    $content = '<div class="LabHubExport"><h3>' . $this->displayName() . '</h3>';
    $content .= $this->payload;
    $content .= '</div>';

    return $content;
  }

  /**
   * @return string Human readable display name for the export.
   */
  public function displayName() {
    return 'LabHub Export ' . $this->export_uuid;
  }

  /**
   * @return array Table column headings for Drupal rendered table.
   */
  private static function tableHeader() {
    return [
      'Export UUID',
      'Export Date',
      'RSpace Document ID',
    ];
  }

  /**
   * @return array Attributes to display in Drupal rendered table.
   */
  private function tableRow() {
    $uuid_html = '<span style="font-family: monospace;">' .
      strtoupper($this->getExportUuid()) . '</span>';
    return [
      $uuid_html,
      date("Y-m-d, H:i", $this->getExportTimestamp()),
      $this->getRspaceDocId(),
    ];
  }

  /**
   * @param self[] $exports Array of LabhubRSpaceExport objects.
   *
   * @return string Drupal rendered HTML table.
   */
  public static function table($exports) {
    try {
      $rows = [];
      foreach ($exports as $export) {
        $rows[] = $export->tableRow();
      }
      return theme('table', ['header' => self::tableHeader(), 'rows' => $rows]);
    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
    }
    return "";
  }

  /**
   * Send export payload to RSpace API. If RSpace document is created
   * successfully, store export instance in Drupal database.
   */
  public function save() {
    $api = new LabhubRSpaceApiClient();
    $api->createTasksDocument($this);
    if ($api->getResponseCode() == 201) {
      $rspace_result = json_decode($api->getData(), TRUE);
      $this->rspace_doc_id = $rspace_result['id'];
      $this->export_timestamp = strtotime($rspace_result['created']);
      $this->link = $rspace_result['_links'][0]['link'];
      $this->id = LabhubRSpaceExportRepository::save($this);
      drupal_set_message("Exported to RSpace, Document ID: " . $this->rspace_doc_id,
        'info');
    }
  }

  /**
   * @return array
   */
  public function dbInsertFields() {
    return [
      'uid' => $this->getUid(),
      'rspace_doc_id' => $this->getRspaceDocId(),
      'export_uuid' => $this->getExportUuid(),
      'export_timestamp' => $this->getExportTimestamp(),
    ];
  }

  /**
   * @return int
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * @return string
   */
  public function getRspaceDocId() {
    return $this->rspace_doc_id;
  }

  /**
   * @param string $rspace_doc_id
   */
  public function setRspaceDocId($rspace_doc_id) {
    $this->rspace_doc_id = $rspace_doc_id;
  }

  /**
   * @return null
   */
  public function getExportUuid() {
    return $this->export_uuid;
  }

  /**
   * @return int
   */
  public function getExportTimestamp() {
    return $this->export_timestamp;
  }
}