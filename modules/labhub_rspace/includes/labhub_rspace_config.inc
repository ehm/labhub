<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_rspace_config() {

  $output = '';

  $services = LabhubServiceRepository::getAllByType(LabhubRSpaceService::TYPE_SERVICE_RSPACE_CONNECTOR);

  if (count($services) > 0) {

    $header = ['ID', 'Service Name', 'Host', 'Service Type'];
    $rows = [];

    foreach ($services as $service) {
      $rows[] = [
        $service->getId(),
        $service->getServiceName(),
        $service->getHost(),
        $service->getServiceType(),
      ];
    }

    try {
      $output .= theme('table', ['header' => $header, 'rows' => $rows]);
      $default_service_form = drupal_get_form('labhub_rspace_config_default_service');
      $output .= drupal_render($default_service_form);
    } catch (Exception $e) {
      watchdog_exception('Labhub RSpace', $e);
    }
  }
  else {
    $output .= 'No RSpace services configured.';
  }

  return $output;
}

function labhub_rspace_config_default_service($form, $form_state) {
  $form = [];

  $default_service = variable_get(LABHUB_RSPACE_VAR_DEFAULT_SERVICE);
  $services = LabhubServiceRepository::getAllByType(LabhubRSpaceService::TYPE_SERVICE_RSPACE_CONNECTOR);
  $options = [];
  foreach ($services as $service) {
    $options[$service->getId()] = $service->getServiceName();
  }

  $form[LABHUB_RSPACE_VAR_DEFAULT_SERVICE] = [
    '#type' => 'select',
    '#title' => t("Select default RSpace service"),
    '#options' => $options,
    '#default_value' => $default_service,
  ];

  return system_settings_form($form);
}