<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function labhub_rspace_service_create($form, &$form_state) {

  $service = new LabhubSeekService();
  $form = LabhubServiceForm::create($service);

  $form = LabhubServiceForm::addSubmitButton($form);

  return $form;
}

function labhub_rspace_service_create_validate($form, &$form_state) {

  $value = $form_state['values'];

  $service = new LabhubSeekService();
  $service->setHost($value['host']);
  if (!$service->testConnection()) {
    form_set_error('host', 'Could not connect to the host.');
  }

}

function labhub_rspace_service_create_submit($form, &$form_state) {

  $value = $form_state['values'];

  $service = new LabhubRSpaceService();
  $service->setHost($value['host']);
  $service->setServiceName($value['service_name']);
  $service->save();

  if ($service->isEmpty()) {
    drupal_set_message("Service could not be stored", "error");
  }
  $form_state['redirect'] = LABHUB_RSPACE_URL_CONFIG;

}