# LabHub Project

Drupal modules to connect data management resources in the 
biomedical research laboratory.

Master's thesis project by Markus Suhr, 
[markus.suhr@stud.uni-goettingen.de](mailto:markus.suhr@stud.uni-goettingen.de)

## Overview

LabHub is a integrated data management system for 
biomedical research laboratories. Facing the challenges of data 
integration and management in the highly dynamic context of biomedical 
research laboratories, LabHub offers an extensible framework for 
comprehensive information infrastructure.

## Installation

Copy project files into modules path of Drupal 7.x installation. 
Enable modules through Drupal administrative back end.

## Copyright and license

#### Third party libraries

##### [Firebase JSON Web Token](https://github.com/firebase/php-jwt/) 
BSD3 license.  
Copyright (c) 2011, Neuman Vong.

##### [Pure PHP UUID generator](https://github.com/fredriklindberg/class.uuid.php)
Appache license.  
Copyright (c) 2011, Fredrik Lindberg.

##### [Highcharts](https://www.highcharts.com/)
CC-BY-NC 3.0.  
Copyright (c), Highsoft.

#### LabHub

GPLv3.

    Copyright (C) 2018  Markus Suhr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.