<?php
/**
 * Copyright (c) 2018 Markus Suhr
 *
 * @author  Markus Suhr <markus.suhr@stud.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Custom Drupal Hook definition
 * hook_labhub_service_connector()
 *
 * @return \stdClass
 */
function hook_labhub_service_connector() {
  $obj = new stdClass();
  $obj->name = 'Service Name';
  $obj->type = 'Service Type';
  $obj->consumes = 'Service Type';
  return $obj;
}

function hook_labhub_implements_type() {
  return [
    'type1' => 'classname',
    'type2' => 'classname',
  ];
}